(** Formalization of Example 4.3. *)

Require Import Utf8.
Require Import List.
Require Import IxFree.Lib.
Require Import Lang.Syntax Lang.Typing Lang.Reduction.
Require Import Lang.SyntaxProperties.
Require Import Lang.CtxApprox.
Require Import BinaryRelation.Core.
Require Import BinaryRelation.ContextRel BinaryRelation.HandlerRel.
Require Import BinaryRelation.Properties BinaryRelation.DefUnroll.
Require Import BinaryRelation.Soundness.

Section ReturnClause.

(** Assuming a set of effects. *)

Variable Eff V EV : Set.

(** Equality of effects should be decidable. *)

Context {EffDec : DecEffectEq Eff}.

(** Assuming the necessary building blocks for the expressions in the
    example. *)

Variable l : Eff.
Variable e : expr Eff V.
Variable h : list (expr Eff (inc (inc V))).
Variable r : expr Eff (inc V).

Variable σ τ : typ Eff EV.
Variable ε : effect Eff EV.

Variable Σ : eff_sig Eff.
Variable Γ : V → typ Eff EV.

Variable e_typing : T[ Σ ; Γ ⊢ e ∷ σ // ef_cons l ε ].
Variable h_typing : H[ Σ ; Γ ; Σ l ⊢ h ∷ τ // ε ].
Variable r_typing : T[ Σ ; Γ ,+ σ ⊢ r ∷ τ // ε ].

Section RelationalReasoning.

(** A step index. *)

Variable n : nat.

(** Assuming two related substitutions. *)

Variable γ₁ γ₂ : V → value0 Eff.
Variable η : EV → IRel (rel_row_sig (Eff := Eff)).

Variable Hγ : n ⊨ rel_g Σ Γ η γ₁ γ₂.

(** An auxiliary property describing an interaction of the lift
    operator with the directly enclosing handler. *)

Lemma lift_neutralizes_handle_l :
  n ⊨ C〚Σ ⊢ τ // ε ↝ τ // ε〛 η r_hole
    (r_handle l (r_lift l r_hole)
      (map (ebind (lift (lift γ₂))) h)
      (v_var VZ)).
Proof.
apply (rel_c_intro Σ (ModNone ε)).
+ intros v₁ v₂; iintro Hv; simpl.
  eapply rel_e_red_r; [ apply red_handle, red_lift_val | ].
  eapply rel_e_red_r; [ apply red_handle_val | ]; simpl.
  apply rel_v_in_e; assumption.
+ intro l'; simpl.
  iintro; split; [ constructor | ].
  destruct (dec_effect_eq l l'); subst; repeat constructor; trivial.
Qed.

(** Using Löb induction to prove the "obvious" contexts related --
    property (4) in the paper. *)

Lemma context_related_l :
  n ⊨ C〚Σ ⊢ σ // ef_cons l ε ↝ τ // ε〛 η
    (r_handle l r_hole
      (map (ebind (lift (lift γ₁))) h)
      (ebind (lift γ₁) r))
    (r_handle l (r_app2 (v_lam (e_lift l (ebind (lift γ₂) r))) r_hole)
      (map (ebind (lift (lift γ₂))) h)
      (v_var VZ)).
Proof.
assert (H_sh := lift_neutralizes_handle_l).
rename n into k.
loeb_induction.
apply (rel_c_intro Σ (ModHandle l (ModNone ε))).
+ intros v₁ v₂; iintro Hv; simpl.
  eapply rel_e_red_r; [ apply red_handle, red_beta | ].
  eapply rel_e_red_l; [ apply red_handle_val | ].
  iintro; simpl.
  eapply rel_c_compat_e in H_sh; [ exact H_sh | ]; clear H_sh.
  rewrite esubst_bind_lift, esubst_bind_lift.
  eapply rel_e_open_elim.
  - apply fundamental_property, r_typing.
  - iintro x; destruct x as [ | x ]; simpl.
    * assumption.
    * iespecialize Hγ; exact Hγ.
+ clear H_sh; intro l'; simpl.
  destruct (dec_effect_eq l' l); subst; isplit.
  - destruct (dec_effect_eq l l); [ | exfalso; auto ].
    iintro; split; repeat constructor.
  - iintro op; iintro Θ; iintro v₁; iintro v₂; iintro E₁; iintro E₂.
    iintro HE.
    idestruct HE as Hfree HE; idestruct HE as Hv HE.
    apply I_Prop_elim in Hfree; destruct Hfree as [ Hop [ Hfree₁ Hfree₂ ] ].
    assert (Hh : k ⊨ rel_h Σ τ ε η (Σ l)
      (map (ebind (lift (lift γ₁))) h) (map (ebind (lift (lift γ₂))) h)).
    { eapply rel_h_close; [ exact Hγ | ].
      apply fundamental_property_h, h_typing.
    }
    eapply rel_h_handle in Hh; [ | exact Hop ].
    idestruct Hh as he₁ Hh; idestruct Hh as he₂ Hh; idestruct Hh as Hhe Hh.
    apply I_Prop_elim in Hh; destruct Hh as [ Hh₁ Hh₂ ].
    eapply rel_e_red_r.
    { apply (red_handle_op _ (r_app2 _ E₂)); [ | eassumption ].
      constructor; assumption.
    }
    eapply rel_e_red_l.
      { apply red_handle_op; eassumption. }
    later_shift.
    iespecialize Hhe; iapply Hhe; clear Hhe.
    isplit; [ exact Hv | ].
    apply rel_v_arrow_roll; intros u₁ u₂; iintro Hu.
    eapply rel_e_red_r; [ apply red_beta | ].
    eapply rel_e_red_l; [ apply red_beta | ]; iintro; simpl.
    rewrite esubst_shift1, esubst_shift1.
    rewrite (List_map2_id _ (emap _) (ebind _));
      [ | intro; apply esubst_shift2 ].
    rewrite (List_map2_id _ (emap _) (ebind _));
      [ | intro; apply esubst_shift2 ].
    eapply rel_c_compat_e in IH; [ exact IH | ]; clear IH.
    rewrite bind_rplug, bind_rplug.
    rewrite rsubst_shift, rsubst_shift.
    simpl; iespecialize HE; iapply HE.
    exact Hu.
  - destruct (dec_effect_eq l l'); [ subst; exfalso; auto | ].
    iintro; split; repeat constructor; assumption.
  - iintro; constructor.
Qed.

(** Proving the two expressions under consideration related by using
    the fundamental property and lemma context_related_l. *)

Lemma progs_related_cl_l :
  n ⊨ E〚Σ ⊢ τ // ε〛 η
    (e_handle l (ebind γ₁ e)
      (map (ebind (lift (lift γ₁))) h)
      (ebind (lift γ₁) r))
    (e_handle l (e_app (v_lam (e_lift l (ebind (lift γ₂) r))) (ebind γ₂ e))
      (map (ebind (lift (lift γ₂))) h)
      (v_var VZ)).
Proof.
assert (HC := context_related_l).
eapply rel_c_compat_e in HC; [ exact HC | ]; clear HC.
eapply rel_e_open_elim; [ | eassumption ].
apply fundamental_property.
assumption.
Qed.

End RelationalReasoning.

(** Approximation as a consequence of the soundness of the logical
    relation. *)

Theorem example_4_3 :
  CTX[Σ; Γ ⊨ e_handle l e h r ≾
    e_handle l (e_app (v_lam (e_lift l r)) e) h (v_var VZ)
    ∷ τ // ε].
Proof.
apply soundness; intro n; unfold rel_e_open.
iintro η; iintro γ₁; iintro γ₂; iintro Hγ.
apply progs_related_cl_l.
assumption.
Qed.

End ReturnClause.