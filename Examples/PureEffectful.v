(** Formalization of Example 4.1. *)

Require Import Utf8.
Require Import IxFree.Lib.
Require Import Lang.Syntax Lang.Typing Lang.Reduction.
Require Import Lang.SyntaxProperties Lang.ReductionProperties.
Require Import Lang.CtxApprox.
Require Import BinaryRelation.Core.
Require Import BinaryRelation.Properties BinaryRelation.DefUnroll.
Require Import BinaryRelation.ContextRel.
Require Import BinaryRelation.EffectSubst.
Require Import BinaryRelation.Compatibility.
Require Import BinaryRelation.Soundness.

Section Pure_vs_Effectful.

(** Assuming a set of effects with at least one effect reader. This
    effect has one operation: ask. *)

Variable Eff : Set.
Variable reader : Eff.
Definition ask : Op := 0.

Variable Σ : eff_sig Eff.

(** Equality of effects should be decidable. *)

Context {EffDec : DecEffectEq Eff}.

(** Assuming some type (called int) with at least one value (5). *)

Variable int : typ0 Eff.
Variable _5 : value0 Eff.

Variable T_5 : T[ Σ ; of_empty ⊢ _5 ∷ int // ef_nil ].

(** Assuming a set of variables with f as its member. *)

Variable Var : Set.
Variable f : Var.

(** Assuming the right typing of f. *)

Variable Γ : Var → typ0 Eff.

Variable τ   : typ0 Eff.

Definition type_of_f : typ0 Eff :=
  t_forallE (t_arrow
    (t_arrow t_unit (ef_var VZ) (tshift int))
    (ef_var VZ)
    (tshift τ)).
Variable type_of_f_in_Γ : Γ f = type_of_f.

(** The two programs under consideration: *)

Definition prog1 : expr Eff Var :=
  e_app (e_eapp (v_var f)) (v_lam (vmap of_empty _5)).

Definition prog2 : expr Eff Var :=
  e_handle reader
    (e_app (e_eapp (v_var f)) (v_op reader ask))
    (cons (e_app (v_var VZ) (vmap of_empty _5)) nil)
    (v_var VZ).

Section Relational_Reasoning.

(** A step index. *)

Variable n : nat.

(** Assuming two related values. *)

Variable f₁ f₂ : value0 Eff.
Variable η : Empty_set → IRel (rel_row_sig (Eff := Eff)).
Variable f_in_rel : n ⊨ 〚Σ ⊢ type_of_f〛 η f₁ f₂.

(** Defining the relation R. *)

Inductive R_core
  (v₁ v₂ : expr0 Eff) (ρ₁ ρ₂ : Eff → option nat) (ν : IRel rel_e2_sig) :=
| mk_R :
  v₁ = e_app (v_lam (vmap of_empty _5)) v_unit →
  v₂ = e_app (v_op reader ask) v_unit →
  (∀ l, ρ₁ l = None) →
  ρ₂ reader = Some 0 →
  (∀ l, reader ≠ l → ρ₂ l = None) →
  (⊨ ν _5 _5) →
  (∀ u₁ u₂ n, n ⊨ ν u₁ u₂ → u₁ = _5 ∧ u₂ = _5) →
  R_core v₁ v₂ ρ₁ ρ₂ ν
.

Definition R : IRel rel_row_sig :=
  λ v₁ v₂ ρ₁ ρ₂ ν, (R_core v₁ v₂ ρ₁ ρ₂ ν)ᵢ.

(** Defining an extension of the empty environment η with a mapping of
    the free type variable to R. *)

Definition ηR (α : inc Empty_set) :=
  match α with
  | VZ   => R
  | VS β => η β
  end.

(** Fact A from the paper. *)

Lemma Fact_A :
  n ⊨ E〚Σ ⊢
    t_arrow (t_arrow t_unit (ef_var VZ) (tshift int)) (ef_var VZ) (tshift τ) //
    ef_nil〛 ηR (e_eapp f₁) (e_eapp f₂).
Proof.
simpl in f_in_rel.
ispecialize f_in_rel R.
exact f_in_rel.
Qed.

(** Fact B from the paper. *)

Lemma Fact_B :
  n ⊨ E〚Σ ⊢ t_arrow t_unit (ef_var VZ) (tshift int) // ef_nil〛 ηR
    (v_lam (vmap of_empty _5)) (v_op reader ask).
Proof.
apply rel_v_in_e, rel_v_arrow_roll.
intros u₁ u₂; iintro Hu.
apply I_Prop_elim in Hu; destruct Hu.
apply rel_s_in_e with (E₁ := r_hole) (E₂ := r_hole).
apply rel_s_roll with
  (ρ₁ := λ _, None)
  (ρ₂ := λ l, if dec_effect_eq l reader then Some 0 else None)
  (ν := λ v₁ v₂ : expr _ _, (v₁ = _5 ∧ v₂ = _5)ᵢ).
+ simpl; iintro; constructor.
  - reflexivity.
  - reflexivity.
  - reflexivity.
  - destruct (dec_effect_eq reader reader); [ reflexivity | ].
    exfalso; auto.
  - intros l Hl; destruct (dec_effect_eq l reader); [ | reflexivity ].
    exfalso; auto.
  - intro; iintro; split; reflexivity.
  - intros u₁ u₂ k Hu; apply I_Prop_elim in Hu; assumption.
+ constructor.
+ intro l; destruct (dec_effect_eq l reader); constructor.
+ intros e₁ e₂; iintro He.
  apply I_Prop_elim in He; destruct He; subst.
  iintro.
  apply (fundamental_property_cl n
    (λ (α : Empty_set), match α with end)) in T_5.
  apply rel_e_weaken_l with (Φ := @VS _) (η₁ := ηR) in T_5.
  - apply rel_e_coerce_pure; exact T_5.
  - iintro μ; destruct μ.
Qed.

(** Fact C from the paper. *)

Lemma Fact_C :
  n ⊨ C〚Σ ⊢ tshift τ // ef_var VZ ↝ tshift τ // ef_nil〛 ηR
    r_hole
    (r_handle reader r_hole
      (cons (e_app (v_var VZ) (vmap of_empty _5)) nil)
      (v_var VZ)).
Proof.
loeb_induction.
unfold rel_c; isplit.
+ iintro v₁; iintro v₂; iintro Hv.
  eapply rel_e_red_r; [ apply red_handle_val | ].
  apply rel_v_in_e; exact Hv.
+ iintro E₁; iintro E₂; iintro e₁; iintro e₂; iintro HEe.
  unfold Frel_simpl_cl in HEe.
  idestruct HEe as ρ₁ HEe; idestruct HEe as ρ₂ HEe; idestruct HEe as ν HEe.
  idestruct HEe as HR HEe; idestruct HEe as Hfree HE.
  apply I_Prop_elim in Hfree; destruct Hfree as [ Hfree₁ Hfree₂ ].
  apply I_Prop_elim in HR.
  destruct HR as [ ? ?  Hρ₁ Hρ₂_e Hρ₂_n Hν Hν' ]; subst.
  ispecialize HE _5.
  ispecialize HE _5.
  assert (HE' := I_arrow_elim HE (Hν n)); clear HE.
  eapply rel_e_red_l.
    { simpl; apply red_in_rctx, red_beta. }
  rename n into k.
  later_shift.
  apply rel_expr_cl_fix_unroll in HE'.
  eapply rel_e_red_r.
    { simpl; apply red_handle_op.
      + specialize (Hfree₂ reader); rewrite Hρ₂_e in Hfree₂; assumption.
      + constructor.
    }
  eapply rel_e_red_r.
    { simpl; apply red_beta. }
  erewrite vmonad_bind_map; [ | intros [] ].
  rewrite vmonad_bind_return', vmonad_map_id'.
  match goal with
  [ |- _ ⊨ _ _ ?e2 ] => assert (He₂ : e2 =
    e_handle reader (rplug E₂ _5)
      (cons (e_app (v_var VZ) (vmap of_empty _5)) nil)
      (v_var VZ))
  end.
  { simpl.
    rewrite bind_rplug, rsubst_shift; simpl.
    rewrite vmonad_map_map'.
    erewrite vmonad_bind_map; [ | intros [] ].
    rewrite vmonad_bind_return'; reflexivity.
  }
  rewrite He₂; clear He₂.
  eapply rel_c_compat_e in IH; [ exact IH | ].
  simpl; erewrite vmonad_bind_map; [ | intros [] ].
  rewrite vmonad_bind_return', vmonad_map_id'.
  exact HE'.
Qed.

(** Using the facts A, B, C and the compatibility lemma for
    application. *)

Lemma combining_ABC :
  n ⊨ E〚Σ ⊢ tshift τ // ef_nil〛 ηR
    (e_app (e_eapp f₁) (v_lam (vmap of_empty _5)))
    (e_handle reader
      (e_app (e_eapp f₂) (v_op reader ask))
      (cons (e_app (v_var VZ) (vmap of_empty _5)) nil)
      (v_var VZ)).
Proof.
assert (HC := Fact_C).
eapply rel_c_compat_e in HC; [ exact HC | ].
eapply app_compat_cl.
+ apply rel_e_coerce_pure, Fact_A.
+ apply rel_e_coerce_pure, Fact_B.
Qed.

(** Weakening the interpretation environment ηR to η. *)

Lemma progs_in_rel_e :
  n ⊨ E〚Σ ⊢ τ // ef_nil〛 η
    (e_app (e_eapp f₁) (v_lam (vmap of_empty _5)))
    (e_handle reader
      (e_app (e_eapp f₂) (v_op reader ask))
      (cons (e_app (v_var VZ) (vmap of_empty _5)) nil)
      (v_var VZ)).
Proof.
eapply rel_e_weaken_r.
+ iintro μ; destruct μ.
+ apply combining_ABC.
Qed.

End Relational_Reasoning.

(** prog1 approximates prog2. *)

Example example_4_1 :
  CTX[Σ; Γ ⊨ prog1 ≾ prog2 ∷ τ // ef_nil].
Proof.
apply soundness; intro n; unfold rel_e_open.
iintro η; iintro γ₁; iintro γ₂; iintro Hγ.
unfold prog1; unfold prog2; simpl.
erewrite vmonad_bind_map, vmonad_bind_return'; [ | intros [] ].
erewrite vmonad_bind_map, vmonad_bind_return'; [ | intros [] ].
apply progs_in_rel_e.
unfold rel_g in Hγ.
ispecialize Hγ f.
rewrite type_of_f_in_Γ in Hγ; assumption.
Qed.

End Pure_vs_Effectful.