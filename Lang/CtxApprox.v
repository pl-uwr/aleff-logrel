Require Import Utf8.
Require Import Lang.Syntax Lang.Typing Lang.Reduction.

Section CtxApprox.
Context {Eff : Set}.
Variable (Σ : eff_sig Eff).

Inductive context : Set → Type :=
| ctx_hole    : context Empty_set
| ctx_lam     : ∀ V : Set, context V → context (inc V)
| ctx_elam    : ∀ V : Set, context V → context V
| ctx_app1    : ∀ V : Set, context V → expr Eff V → context V
| ctx_app2    : ∀ V : Set, expr Eff V → context V → context V
| ctx_eapp    : ∀ V : Set, context V → context V
| ctx_lift    : ∀ V : Set, Eff → context V → context V
| ctx_handle1 : ∀ V : Set, Eff →
    context V → list (expr Eff (inc (inc V))) → expr Eff (inc V) → context V
| ctx_handle3 : ∀ V : Set, Eff →
    expr Eff V → list (expr Eff (inc (inc V))) → context V → context (inc V)
| ctx_handler : ∀ V : Set,
    hcontext V → list (expr Eff (inc (inc V))) → context (inc (inc V))
with hcontext : Set → Type :=
| hctx_cons   : ∀ V : Set, hcontext V → expr Eff (inc (inc V)) → hcontext V
| hctx_handle : ∀ V : Set, Eff → expr Eff V → context V → expr Eff (inc V) →
    hcontext V
.

Arguments ctx_lam     [V].
Arguments ctx_elam    [V].
Arguments ctx_app1    [V].
Arguments ctx_app2    [V].
Arguments ctx_eapp    [V].
Arguments ctx_lift    [V].
Arguments ctx_handle1 [V].
Arguments ctx_handle3 [V].
Arguments ctx_handler [V].

Arguments hctx_cons   [V].
Arguments hctx_handle [V].

(** Contexts are represented inside-out. *)

Fixpoint cplug {V : Set} (C : context V) : expr Eff V → expr0 Eff :=
  match C in context V return expr Eff V → expr0 Eff with
  | ctx_hole            => λ e, e
  | ctx_lam  C          => λ e, cplug C (v_lam e)
  | ctx_elam C          => λ e, cplug C (v_elam e)
  | ctx_app1 C e'       => λ e, cplug C (e_app e e')
  | ctx_app2 e' C       => λ e, cplug C (e_app e' e)
  | ctx_eapp C          => λ e, cplug C (e_eapp e)
  | ctx_lift    l C     => λ e, cplug C (e_lift l e)
  | ctx_handle1 l C h r => λ e, cplug C (e_handle l e h r)
  | ctx_handle3 l f h C => λ e, cplug C (e_handle l f h e)
  | ctx_handler C h     => λ e, hplug C (e :: h)
  end
with hplug {V : Set} (C : hcontext V) :
    list (expr Eff (inc (inc V))) → expr0 Eff :=
  match C in hcontext V return list (expr Eff (inc (inc V))) → expr0 Eff with
  | hctx_cons   C he    => λ h, hplug C (he :: h)
  | hctx_handle l e C r => λ h, cplug C (e_handle l e h r)
  end.

Inductive ctx_typing : ∀ (V EV : Set),
    (V → typ Eff EV) → context V → typ Eff EV → effect Eff EV → Prop :=
| CT_hole : ctx_typing Empty_set Empty_set of_empty ctx_hole t_unit ef_nil
| CT_lam  : ∀ (V EV : Set) (Γ : V → typ Eff EV) C σ ε τ,
    ctx_typing _ _ Γ C (t_arrow σ ε τ) ef_nil →
    ctx_typing _ _ (Γ ,+ σ) (ctx_lam C) τ ε
| CT_elam : ∀ (V EV : Set) (Γ : V → typ Eff EV) C τ,
    ctx_typing _ _ Γ C (t_forallE τ) ef_nil →
    ctx_typing _ _ (Γ ↑+) (ctx_elam C) τ ef_nil
| CT_app1 : ∀ (V EV : Set) (Γ : V → typ Eff EV) C e σ ε τ,
    ctx_typing _ _ Γ C τ ε →
    T[ Σ ; Γ ⊢ e ∷ σ // ε ] →
    ctx_typing _ _ Γ (ctx_app1 C e) (t_arrow σ ε τ) ε
| CT_app2 : ∀ (V EV : Set) (Γ : V → typ Eff EV) e C σ ε τ,
    T[ Σ ; Γ ⊢ e ∷ t_arrow σ ε τ // ε ] →
    ctx_typing _ _ Γ C τ ε →
    ctx_typing _ _ Γ (ctx_app2 e C) σ ε
| CT_eapp : ∀ (V EV : Set) (Γ : V → typ Eff EV) C τ ε ε',
    ctx_typing _ _ Γ C (tsubst τ ε') ε →
    ctx_typing _ _ Γ (ctx_eapp C) (t_forallE τ) ε
| CT_elift : ∀ (V EV : Set) (Γ : V → typ Eff EV) l C τ ε,
    ctx_typing _ _ Γ C τ (ef_cons l ε) →
    ctx_typing _ _ Γ (ctx_lift l C) τ ε
| CT_handle1 : ∀ (V EV : Set) (Γ : V → typ Eff EV) l C h r σ τ ε,
    ctx_typing _ _ Γ C τ ε →
    H[ Σ ; Γ ; Σ l ⊢ h ∷ τ // ε ] →
    T[ Σ ; Γ ,+ σ ⊢ r ∷ τ // ε ] →
    ctx_typing _ _ Γ (ctx_handle1 l C h r) σ (ef_cons l ε)
| CT_handle3 : ∀ (V EV : Set) (Γ : V → typ Eff EV) l f h C σ τ ε,
    T[ Σ ; Γ ⊢ f ∷ σ // ef_cons l ε ] →
    H[ Σ ; Γ ; Σ l ⊢ h ∷ τ // ε ] →
    ctx_typing _ _ Γ C τ ε →
    ctx_typing _ _ (Γ ,+ σ) (ctx_handle3 l f h C) τ ε
| CT_handle : ∀ (V EV : Set) (Γ : V → typ Eff EV) Θ FS C h τ ε,
    hctx_typing _ _ Γ (Θ :: FS) C τ ε →
    H[ Σ ; Γ ; FS ⊢ h ∷ τ // ε ] →
    ctx_typing _ _
      (Γ ,+ topen (op_input Θ) ,+ (t_arrow (topen (op_output Θ)) ε τ))
      (ctx_handler C h) τ ε
| CT_sub : ∀ (V EV : Set) (Γ : V → typ Eff EV) C τ₁ τ₂ ε₁ ε₂,
    ctx_typing _ _ Γ C τ₂ ε₂ →
    ST[ τ₁ ≾ τ₂ ] →
    SF[ ε₁ ≾ ε₂ ] →
    ctx_typing _ _ Γ C τ₁ ε₁
with hctx_typing : ∀ (V EV : Set), (V → typ Eff EV) → list (op_sig Eff) →
    hcontext V → typ Eff EV → effect Eff EV → Prop :=
| HT_cons : ∀ (V EV : Set) (Γ : V → typ Eff EV) Θ FS C he τ ε,
    hctx_typing _ _ Γ (Θ :: FS) C τ ε →
    T[ Σ ; Γ ,+ topen (op_input Θ) ,+ t_arrow (topen (op_output Θ)) ε τ
     ⊢ he ∷ τ // ε ] →
    hctx_typing _ _ Γ FS (hctx_cons C he) τ ε
| HT_handle : ∀ (V EV : Set) (Γ : V → typ Eff EV) l e C r σ τ ε,
    T[ Σ ; Γ ⊢ e ∷ σ // ef_cons l ε ] →
    ctx_typing _ _ Γ C τ ε →
    T[ Σ ; Γ ,+ σ ⊢ r ∷ τ // ε ] →
    hctx_typing _ _ Γ (Σ l) (hctx_handle l e C r) τ ε
.

Arguments ctx_typing [V] [EV].

Definition base_obs (e₁ e₂ : expr0 Eff) : Prop :=
  red_rtc e₁ v_unit → red_rtc e₂ v_unit.

Definition ctx_approx {V EV : Set} (Γ : V → typ Eff EV)
    (e₁ e₂ : expr Eff V) (τ : typ Eff EV) (ε : effect Eff EV) : Prop :=
  ∀ C, ctx_typing Γ C τ ε → base_obs (cplug C e₁) (cplug C e₂).

End CtxApprox.

Notation "'CTX[' Σ ';' Γ '⊨' e₁ '≾' e₂ '∷' τ '//' ε ']'" :=
  (ctx_approx Σ Γ e₁ e₂ τ ε) (Γ at level 97).

Notation "'C[' Σ ';' Γ '⊢' C '∷' τ '//' ε ']'" :=
  (@ctx_typing _ Σ _ _ Γ C τ ε).
Notation "'CH[' Σ ';' Γ ';' FS '⊢' C '∷' τ '//' ε ']'" :=
  (@hctx_typing _ Σ _ _ Γ FS C τ ε).