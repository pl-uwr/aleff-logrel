Require Import Utf8.
Require Import Lang.Syntax.

Lemma aux_lift_map_id {A : Set}
  {f : A → A} :
  (∀ x, f x = x) →
  (∀ x, inc_map f x = x).
Proof.
intros Hf [ | x]; simpl; [ reflexivity | ].
rewrite Hf; reflexivity.
Qed.

Lemma List_map_id {A : Set} (xs : list A) (f : A → A) :
  (∀ x, f x = x) → List.map f xs = xs.
Proof.
intro Hf; induction xs; simpl; congruence.
Qed.

Lemma List_map2_id {A B : Set} (xs : list A) (f : A → B) (g : B → A) :
  (∀ x, g (f x) = x) → List.map g (List.map f xs) = xs.
Proof.
intro Hf; induction xs; simpl; congruence.
Qed.

Lemma List_map2_map2 {A B B' C : Set} (xs : list A)
  (f₁ : A → B) (f₂ : B → C) (g₁ : A → B') (g₂ : B' → C) :
  (∀ x, f₂ (f₁ x) = g₂ (g₁ x)) →
  List.map f₂ (List.map f₁ xs) = List.map g₂ (List.map g₁ xs).
Proof.
intro Hfg; induction xs as [ | x xs IHxs ]; simpl; [ reflexivity | ].
rewrite Hfg; rewrite IHxs; reflexivity.
Qed.

Fixpoint fmonad_map_id {Eff A : Set} (ε : effect Eff A)
  (f : A → A) {struct ε} :
  (∀ x, f x = x) → fmap f ε = ε.
Proof.
intro Hf; destruct ε as [ μ | | l ε ]; simpl.
+ rewrite Hf; reflexivity.
+ reflexivity.
+ rewrite fmonad_map_id; [ | assumption ]; reflexivity.
Qed.

Fixpoint emonad_map_id {Eff A : Set} (e : expr Eff A)
  (f : A → A){struct e} :
  (∀ x, f x = x) → emap f e = e
with vmonad_map_id {Eff A : Set} (v : value Eff A)
  (f : A → A) {struct v} :
  (∀ x, f x = x) → vmap f v = v.
Proof.
+ intro Hf; destruct e as [ v | e₁ e₂ | e | l e | l e h re ]; simpl.
  - erewrite vmonad_map_id; [ reflexivity | eassumption ].
  - erewrite emonad_map_id; [ | eassumption ].
    erewrite emonad_map_id; [ | eassumption ].
    reflexivity.
  - erewrite emonad_map_id; [ reflexivity | eassumption ].
  - erewrite emonad_map_id; [ reflexivity | eassumption ].
  - erewrite emonad_map_id; [ | eassumption ].
    erewrite emonad_map_id; [ | apply (aux_lift_map_id Hf) ].
    assert (Hh : List.map (emap (inc_map (inc_map f))) h = h).
    { induction h; simpl; [ reflexivity | ].
      rewrite IHh.
      erewrite emonad_map_id; [ reflexivity | ].
      intros [ | [ | x ] ]; simpl; try reflexivity.
      rewrite Hf; reflexivity.
    }
    rewrite Hh; reflexivity.
+ intro Hf; destruct v as [ x | | e | e | l op ]; simpl.
  - rewrite Hf; reflexivity.
  - reflexivity.
  - erewrite emonad_map_id; [ reflexivity | apply (aux_lift_map_id Hf) ].
  - erewrite emonad_map_id; [ reflexivity | eassumption ].
  - reflexivity.
Qed.

Lemma vmonad_map_id' {Eff A : Set} (v : value Eff A) :
  vmap (λ x, x) v = v.
Proof.
apply vmonad_map_id; reflexivity.
Qed.

Lemma rmonad_map_id {Eff A : Set} (E : rctx Eff A)
  (f : A →  A) :
  (∀ x, f x = x) → rmap f E = E.
Proof.
intro Hf; induction E as [ | | | | | l E IHE h re ]; simpl.
+ reflexivity.
+ rewrite IHE; erewrite emonad_map_id; [ | eassumption ]; reflexivity.
+ rewrite IHE; erewrite vmonad_map_id; [ | eassumption ]; reflexivity.
+ rewrite IHE; reflexivity.
+ rewrite IHE; reflexivity.
+ rewrite IHE.
  erewrite (emonad_map_id re); [ | apply (aux_lift_map_id Hf) ].
  erewrite List_map_id; [ reflexivity | ].
  intro e; apply emonad_map_id.
  intros [ | [ | x ]]; simpl; try reflexivity.
  rewrite Hf; reflexivity.
Qed.

Lemma aux_lift_map_map {A B C : Set}
  {f₁ : A → B} {f₂ : B → C} {g : A → C} :
  (∀ x, f₂ (f₁ x) = g x) →
  (∀ x, inc_map f₂ (inc_map f₁ x) = inc_map g x).
Proof.
intros Hf [ | x]; simpl; [ reflexivity | ].
rewrite Hf; reflexivity.
Qed.

Lemma aux_lift2_map_map {A B C : Set}
  {f₁ : A → B} {f₂ : B → C} {g : A → C} :
  (∀ x, f₂ (f₁ x) = g x) →
  (∀ x, inc_map (inc_map f₂) (inc_map (inc_map f₁) x) = inc_map (inc_map g) x).
Proof.
intros Hf [ | [ | x ] ]; simpl; [ reflexivity | reflexivity | ].
rewrite Hf; reflexivity.
Qed.

Fixpoint emonad_map_map {Eff A B C : Set} (e : expr Eff A)
  (f₁ : A → B) (f₂ : B → C) (g : A → C) {struct e} :
  (∀ x, f₂ (f₁ x) = g x) →
  emap f₂ (emap f₁ e) = emap g e
with vmonad_map_map {Eff A B C : Set} (v : value Eff A)
  (f₁ : A → B) (f₂ : B → C) (g : A → C) {struct v} :
  (∀ x, f₂ (f₁ x) = g x) →
  vmap f₂ (vmap f₁ v) = vmap g v.
Proof.
+ intro Hf; destruct e as [ v | e₁ e₂ | e | l e | l e h re ]; simpl.
  - erewrite vmonad_map_map; [ reflexivity | eassumption ].
  - erewrite emonad_map_map; [ | eassumption ].
    erewrite emonad_map_map; [ | eassumption ].
    reflexivity.
  - erewrite emonad_map_map; [ reflexivity | eassumption ].
  - erewrite emonad_map_map; [ reflexivity | eassumption ].
  - erewrite emonad_map_map; [ | eassumption ].
    erewrite emonad_map_map; [ | apply (aux_lift_map_map Hf) ].
    assert (Hh : List.map (emap (inc_map (inc_map f₂)))
              (List.map (emap (inc_map (inc_map f₁))) h) =
            List.map (emap (inc_map (inc_map g))) h).
    { induction h; simpl; [ reflexivity | ].
      rewrite IHh.
      erewrite emonad_map_map; [ reflexivity | ].
      apply aux_lift2_map_map; assumption.
    }
    rewrite Hh; reflexivity.
+ intro Hf; destruct v as [ x | | e | e | l op ]; simpl.
  - rewrite Hf; reflexivity.
  - reflexivity.
  - erewrite emonad_map_map; [ reflexivity | apply (aux_lift_map_map Hf) ].
  - erewrite emonad_map_map; [ reflexivity | eassumption ].
  - reflexivity.
Qed.

Lemma emonad_map_map' {Eff A B C : Set} (e : expr Eff A)
  (f : A → B) (g : B → C) :
  emap g (emap f e) = emap (λ x, g (f x)) e.
Proof.
apply emonad_map_map; reflexivity.
Qed.

Lemma vmonad_map_map' {Eff A B C : Set} (v : value Eff A)
  (f : A → B) (g : B → C) :
  vmap g (vmap f v) = vmap (λ x, g (f x)) v.
Proof.
apply vmonad_map_map; reflexivity.
Qed.

Lemma aux_lift_bind_map {Eff A B B' C : Set}
  {f₁ : A → B} {f₂ : B → value Eff C} {g₁ : A → value Eff B'} {g₂ : B' → C} :
  (∀ x, f₂ (f₁ x) = vmap g₂ (g₁ x)) →
  (∀ x, lift f₂ (inc_map f₁ x) = vmap (inc_map g₂) (lift g₁ x)).
Proof.
intros Hf [ | x]; simpl; [ reflexivity | ].
rewrite Hf; rewrite vmonad_map_map'.
symmetry; apply vmonad_map_map; reflexivity.
Qed.

Lemma aux_lift2_bind_map {Eff A B B' C : Set}
  {f₁ : A → B} {f₂ : B → value Eff C} {g₁ : A → value Eff B'} {g₂ : B' → C} :
  (∀ x, f₂ (f₁ x) = vmap g₂ (g₁ x)) →
  (∀ x, lift (lift f₂) (inc_map (inc_map f₁) x) =
        vmap (inc_map (inc_map g₂)) (lift (lift g₁) x)).
Proof.
intros Hf [ | [ | x ]]; simpl; [ reflexivity | reflexivity | ].
rewrite Hf.
rewrite vmonad_map_map', vmonad_map_map', vmonad_map_map'.
symmetry; apply vmonad_map_map; reflexivity.
Qed.

Fixpoint fmonad_bind_map {Eff A B B' C : Set} (ε : effect Eff A)
  (f₁ : A → B) (f₂ : B → effect Eff C) (g₁ : A → effect Eff B') (g₂ : B' → C)
  {struct ε} :
  (∀ x, f₂ (f₁ x) = fmap g₂ (g₁ x)) →
  fbind f₂ (fmap f₁ ε) = fmap g₂ (fbind g₁ ε).
Proof.
intro Hf; destruct ε as [ μ | | l ε ]; simpl.
- rewrite Hf; reflexivity.
- reflexivity.
- erewrite fmonad_bind_map; [ | eassumption ]; reflexivity.
Qed.

Fixpoint emonad_bind_map {Eff A B B' C : Set} (e : expr Eff A)
  (f₁ : A → B ) (f₂ : B → value Eff C) (g₁ : A → value Eff B') (g₂ : B' → C)
  {struct e} :
  (∀ x, f₂ (f₁ x) = vmap g₂ (g₁ x)) →
  ebind f₂ (emap f₁ e) = emap g₂ (ebind g₁ e)
with vmonad_bind_map {Eff A B B' C : Set} (v : value Eff A)
  (f₁ : A → B ) (f₂ : B → value Eff C) (g₁ : A → value Eff B') (g₂ : B' → C)
  {struct v} :
  (∀ x, f₂ (f₁ x) = vmap g₂ (g₁ x)) →
  vbind f₂ (vmap f₁ v) = vmap g₂ (vbind g₁ v).
Proof.
+ intro Hf; destruct e as [ v | e₁ e₂ | e| l e | l e h re ]; simpl.
  - erewrite vmonad_bind_map; [ reflexivity | eassumption ].
  - erewrite emonad_bind_map; [ | eassumption ].
    erewrite emonad_bind_map; [ | eassumption ].
    reflexivity.
  - erewrite emonad_bind_map; [ reflexivity | eassumption ].
  - erewrite emonad_bind_map; [ reflexivity | eassumption ].
  - erewrite emonad_bind_map; [ | eassumption ].
    erewrite emonad_bind_map; [ | apply (aux_lift_bind_map Hf) ].
    assert (Hh : List.map (ebind (lift (lift f₂)))
                   (List.map (emap (inc_map (inc_map f₁))) h) =
                 List.map (emap (inc_map (inc_map g₂)))
                   (List.map (ebind (lift (lift g₁))) h)).
    { induction h; simpl; [ reflexivity | ].
      rewrite IHh.
      erewrite emonad_bind_map; [ reflexivity | ].
      apply aux_lift2_bind_map; assumption.
    }
    rewrite Hh; reflexivity.
+ intro Hf; destruct v as [ x | | e | e | l op ]; simpl.
  - rewrite Hf; reflexivity.
  - reflexivity.
  - erewrite emonad_bind_map; [ reflexivity | apply (aux_lift_bind_map Hf) ].
  - erewrite emonad_bind_map; [ reflexivity | eassumption ].
  - reflexivity.
Qed.

Lemma rmonad_bind_map {Eff A B B' C : Set} (E : rctx Eff A)
  (f₁ : A → B ) (f₂ : B → value Eff C) (g₁ : A → value Eff B') (g₂ : B' → C) :
  (∀ x, f₂ (f₁ x) = vmap g₂ (g₁ x)) →
  rbind f₂ (rmap f₁ E) = rmap g₂ (rbind g₁ E).
Proof.
intro Hf; induction E as [ | | | | | l E IHE h re ]; simpl.
+ reflexivity.
+ rewrite IHE; erewrite emonad_bind_map; [ | eassumption ]; reflexivity.
+ rewrite IHE; erewrite vmonad_bind_map; [ | eassumption ]; reflexivity.
+ rewrite IHE; reflexivity.
+ rewrite IHE; reflexivity.
+ rewrite IHE.
  erewrite (emonad_bind_map re); [ | apply (aux_lift_bind_map Hf) ].
  erewrite List_map2_map2; [ reflexivity | ].
  intro e; apply emonad_bind_map, aux_lift2_bind_map; assumption.
Qed.

Lemma aux_lift_bind_bind {Eff A B C : Set}
  {f₁ : A → value Eff B} {f₂ : B → value Eff C} {g : A → value Eff C} :
  (∀ x, vbind f₂ (f₁ x) = g x) → 
  (∀ x, vbind (lift f₂) (lift f₁ x) = lift g x).
Proof.
intros Hf [ | x]; simpl; [ reflexivity | ].
rewrite <- Hf; apply vmonad_bind_map; reflexivity.
Qed.

Lemma aux_lift2_bind_bind {Eff A B C : Set}
  {f₁ : A → value Eff B} {f₂ : B → value Eff C} {g : A → value Eff C} :
  (∀ x, vbind f₂ (f₁ x) = g x) → 
  (∀ x, vbind (lift (lift f₂)) (lift (lift f₁) x) = lift (lift g) x).
Proof.
intros Hf [ | [ | x ]]; simpl; [ reflexivity | reflexivity | ].
rewrite <- Hf, vmonad_map_map', vmonad_map_map'.
apply vmonad_bind_map; simpl.
intro; apply vmonad_map_map; reflexivity.
Qed.

Fixpoint emonad_bind_bind {Eff A B C : Set} (e : expr Eff A)
  (f₁ : A → value Eff B) (f₂ : B → value Eff C) (g : A → value Eff C)
  {struct e} :
  (∀ x, vbind f₂ (f₁ x) = g x) → 
  ebind f₂ (ebind f₁ e) = ebind g e
with vmonad_bind_bind {Eff A B C : Set} (v : value Eff A)
  (f₁ : A → value Eff B) (f₂ : B → value Eff C) (g : A → value Eff C)
  {struct v} :
  (∀ x, vbind f₂ (f₁ x) = g x) → 
  vbind f₂ (vbind f₁ v) = vbind g v.
Proof.
+ intro Hf; destruct e as [ v | e₁ e₂ | e | l e | l e h re ]; simpl.
  - erewrite vmonad_bind_bind; [ reflexivity | eassumption ].
  - erewrite emonad_bind_bind; [ | eassumption ].
    erewrite emonad_bind_bind; [ | eassumption ].
    reflexivity.
  - erewrite emonad_bind_bind; [ reflexivity | eassumption ].
  - erewrite emonad_bind_bind; [ reflexivity | eassumption ].
  - erewrite emonad_bind_bind; [ | eassumption ].
    erewrite emonad_bind_bind; [ | apply (aux_lift_bind_bind Hf) ].
    assert (Hh : List.map (ebind (lift (lift f₂)))
                   (List.map (ebind (lift (lift f₁))) h) =
                 List.map (ebind (lift (lift g))) h).
    { induction h; simpl; [ reflexivity | ].
      rewrite IHh.
      erewrite emonad_bind_bind; [ reflexivity | ].
      apply aux_lift2_bind_bind; assumption.
    }
    rewrite Hh; reflexivity.
+ intro Hf; destruct v as [ x | | e | e | l op ]; simpl.
  - rewrite Hf; reflexivity.
  - reflexivity.
  - erewrite emonad_bind_bind; [ reflexivity | apply (aux_lift_bind_bind Hf) ].
  - erewrite emonad_bind_bind; [ reflexivity | eassumption ].
  - reflexivity.
Qed.

Lemma aux_lift_bind_return {Eff A : Set}
  {f : A → value Eff A} :
  (∀ x, f x = v_var x) → 
  (∀ x, lift f x = v_var x).
Proof.
intros Hf [ | x]; simpl; [ reflexivity | ].
rewrite Hf; reflexivity.
Qed.

Fixpoint fmonad_bind_return {Eff A : Set} (ε : effect Eff A)
  (f : A → effect Eff A) {struct ε} :
  (∀ x, f x = ef_var x) →
  fbind f ε = ε.
Proof.
intro Hf; destruct ε as [ μ | | l ε ]; simpl.
+ apply Hf.
+ reflexivity.
+ rewrite fmonad_bind_return; [ | assumption ]; reflexivity.
Qed.

Fixpoint emonad_bind_return {Eff A : Set} (e : expr Eff A)
  (f : A → value Eff A) {struct e} :
  (∀ x, f x = v_var x) →
  ebind f e = e
with vmonad_bind_return {Eff A : Set} (v : value Eff A)
  (f : A → value Eff A) {struct v} :
  (∀ x, f x = v_var x) →
  vbind f v = v.
Proof.
+ intro Hf; destruct e as [ v | e₁ e₂ | e | l e | l e h re ]; simpl.
  - erewrite vmonad_bind_return; [ reflexivity | eassumption ].
  - erewrite emonad_bind_return; [ | eassumption ].
    erewrite emonad_bind_return; [ | eassumption ].
    reflexivity.
  - erewrite emonad_bind_return; [ reflexivity | eassumption ].
  - erewrite emonad_bind_return; [ reflexivity | eassumption ].
  - erewrite emonad_bind_return; [ | eassumption ].
    erewrite emonad_bind_return; [ | apply (aux_lift_bind_return Hf) ].
    assert (Hh : List.map (ebind (lift (lift f))) h = h).
    { induction h; simpl; [ reflexivity | ].
      rewrite IHh.
      erewrite emonad_bind_return; [ reflexivity | ].
      intros [ | [ | x ]]; simpl; try reflexivity.
      rewrite Hf; reflexivity.
    }
    rewrite Hh; reflexivity.
+ intro Hf; destruct v as [ x | | e | e | l op ]; simpl.
  - rewrite Hf; reflexivity.
  - reflexivity.
  - erewrite emonad_bind_return;
      [ reflexivity | apply (aux_lift_bind_return Hf) ].
  - erewrite emonad_bind_return; [ reflexivity | eassumption ].
  - reflexivity.
Qed.

Lemma rmonad_bind_return {Eff A : Set} (E : rctx Eff A)
  (f : A → value Eff A) :
  (∀ x, f x = v_var x) → rbind f E = E.
Proof.
intro Hf; induction E as [ | | | | | l E IHE h re ]; simpl.
+ reflexivity.
+ rewrite IHE; erewrite emonad_bind_return; [ | eassumption ]; reflexivity.
+ rewrite IHE; erewrite vmonad_bind_return; [ | eassumption ]; reflexivity.
+ rewrite IHE; reflexivity.
+ rewrite IHE; reflexivity.
+ rewrite IHE.
  erewrite (emonad_bind_return re); [ | apply (aux_lift_bind_return Hf) ].
  erewrite List_map_id; [ reflexivity | ].
  intro e; apply emonad_bind_return.
  intros [ | [ | x ]]; simpl; try reflexivity.
  rewrite Hf; reflexivity.
Qed.

Lemma emonad_bind_return' {Eff A : Set} (e : expr Eff A) :
  ebind (@v_var _ _) e = e.
Proof.
apply emonad_bind_return; reflexivity.
Qed.

Lemma vmonad_bind_return' {Eff A : Set} (v : value Eff A) :
  vbind (@v_var _ _) v = v.
Proof.
apply vmonad_bind_return; reflexivity.
Qed.

Lemma fsubst_shift {Eff EV : Set} (ε : effect Eff EV) ε' :
  fsubst (fshift ε') ε = ε'.
Proof.
rewrite fmonad_bind_map with (g₁ := @ef_var _ _) (g₂ := λ x, x).
+ rewrite fmonad_bind_return; [ | reflexivity ].
  apply fmonad_map_id; reflexivity.
+ reflexivity.
Qed.

Lemma vsubst_shift {Eff V : Set} (v₀ : value Eff V) v :
  vsubst (vshift v₀) v = v₀.
Proof.
rewrite vmonad_bind_map with (g₁ := @v_var _ _) (g₂ := λ x, x).
+ rewrite vmonad_bind_return; [ | reflexivity ].
  apply vmonad_map_id; reflexivity.
+ reflexivity.
Qed.

Lemma rsubst_shift {Eff V : Set} (E : rctx Eff V) v :
  rsubst (rshift E) v = E.
Proof.
rewrite rmonad_bind_map with (g₁ := @v_var _ _) (g₂ := λ x, x).
+ rewrite rmonad_bind_return; [ | reflexivity ].
  apply rmonad_map_id; reflexivity.
+ reflexivity.
Qed.

Lemma esubst_shift1 {Eff V : Set} (e : expr Eff (inc V)) v :
  esubst1 (eshift1 e) v = e.
Proof.
rewrite emonad_bind_map with (g₁ := @v_var _ _) (g₂ := λ x, x).
+ rewrite emonad_bind_return; [ | reflexivity ].
  apply emonad_map_id; reflexivity.
+ intros [ | x ]; simpl; reflexivity.
Qed.

Lemma esubst_shift2 {Eff V : Set} (e : expr Eff (inc (inc V))) v :
  esubst2 (eshift2 e) v = e.
Proof.
rewrite emonad_bind_map with (g₁ := @v_var _ _) (g₂ := λ x, x).
+ rewrite emonad_bind_return; [ | reflexivity ].
  apply emonad_map_id; reflexivity.
+ intros [ | [ | x ] ]; simpl; reflexivity.
Qed.

Lemma vbisubst_shift_shift {Eff V : Set} (v : value Eff V) v₁ v₂ :
  vbisubst (vshift (vshift v)) v₁ v₂ = v.
Proof.
rewrite vmonad_map_map'.
rewrite vmonad_bind_map with (g₁ := @v_var _ _) (g₂ := λ x, x).
+ rewrite vmonad_bind_return; [ | reflexivity ].
  apply vmonad_map_id; reflexivity.
+ reflexivity.
Qed.

Definition extend {Eff A B : Set} (γ : A → value Eff B) (v : value Eff B) :
    inc A → value Eff B :=
  λ x, match x with
       | VZ   => v
       | VS y => γ y
       end.

Lemma esubst_bind_lift {Eff A B : Set} (γ : A → value Eff B) e v :
  esubst (ebind (lift γ) e) v = ebind (extend γ v) e.
Proof.
apply emonad_bind_bind.
intros [ | x ]; simpl; [ reflexivity | ].
apply vsubst_shift.
Qed.

Lemma ebisubst_bind_lift_lift {Eff A B : Set} (γ : A → value Eff B) e v₁ v₂ :
  ebisubst (ebind (lift (lift γ)) e) v₁ v₂ = ebind (extend (extend γ v₁) v₂) e.
Proof.
apply emonad_bind_bind.
intros [ | [ | x ] ]; simpl; [ reflexivity | reflexivity | ].
apply vbisubst_shift_shift.
Qed.

Lemma bind_rplug {Eff A B : Set} (f : A → value Eff B) E e :
  ebind f (rplug E e) = rplug (rbind f E) (ebind f e).
Proof.
induction E; simpl; try rewrite IHE; reflexivity.
Qed.

Lemma plug_xcompr {Eff V : Set} (E₀ : ectx Eff V) (E : rctx Eff V) e :
  plug (xcompr E₀ E) e = plug E₀ (rplug E e).
Proof.
generalize E₀; clear E₀; induction E; simpl; intro E₀;
    try rewrite IHE; reflexivity.
Qed.

Lemma rplug_rcomp {Eff V : Set} (E₀ E : rctx Eff V) e :
  rplug (rcomp E₀ E) e = rplug E₀ (rplug E e).
Proof.
induction E₀; simpl; try reflexivity; rewrite IHE₀; reflexivity.
Qed.

Lemma rfree_g_rcomp {Eff V : Set} l (E₀ E : rctx Eff V) n₁ n₂ n₃ :
  rfree_g n₂ l n₃ E₀ →
  rfree_g n₁ l n₂ E →
  rfree_g n₁ l n₃ (rcomp E₀ E).
Proof.
intros HE₀ HE; induction HE₀; simpl; try constructor; assumption.
Qed.

Lemma rfree_g_shift {Eff V : Set} l (E : rctx Eff V) n₁ n₂ n :
  rfree_g n₁ l n₂ E →
  rfree_g (n₁ + n) l (n₂ + n) E.
Proof.
induction 1; simpl; try constructor; assumption.
Qed.