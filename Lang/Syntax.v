(** * Formalization of Section 2.1 *)

Require Import Utf8.
Require Import List.

(**
Our calculus is parametrized by a set of effect names (denoted by Eff in
the formalization). We assume decidable equality for this set.
*)

Class DecEffectEq (Eff : Set) : Set :=
  { dec_effect_eq : ∀ l₁ l₂ : Eff, {l₁ = l₂} + {l₁ ≠ l₂}
  }.

(** ** Variable binding and open terms *)
(**
Each part of syntax which involves variables (expression vairables or effect
variables) is parametrized by a set of "potentially free" variables. This
approach allows us to represent variable binding in a simple way: A binder
just extends this set by one element. In order to extend any set by one
element we define the following inductive type:
*)

Inductive inc (V : Set) : Set :=
| VZ : inc V
| VS : V → inc V
.

Arguments VZ [V].
Arguments VS [V].

(**
Note that this type is isomorphic with option type, but we decided to
define own type in order to have close connection with deBruijn indices:
newly defined variable is represented by a constructor VZ (zero), and
constructor VS (successor) is added to all already existing variables.
*)

(**
A typing environment (f) is defined as a function from set A of type variables
(potentially free in given context) to types (B). Extended environment
is as function from inc A to B and can be defined using the following
function:
*)

Definition extend {A : Set} {B : Type} (f : A → B) (x : B) : inc A → B :=
  λ y, match y with
       | VZ   => x
       | VS z => f z
       end.
Notation "f ',+' x" := (@extend _ _ f x) (at level 45, left associativity).

(** ** Syntax of the calculus (Figure 1) *)

(**
Effect rows are parametrized by set of effect names and effect variables.
*)
Inductive effect (Eff EV : Set) : Set :=
| ef_var  : EV → effect Eff EV
| ef_nil  : effect Eff EV
| ef_cons : Eff → effect Eff EV → effect Eff EV
.

Arguments ef_var  [Eff] [EV].
Arguments ef_nil  [Eff] [EV].
Arguments ef_cons [Eff] [EV].

(**
Types. The constructor t_forallE binds the effect variable: type under
the constructor is parametrized by a set (inc EV), which is EV with
one additional element. Note that types and effects are well-formed by
construction; we don't need rules of Figure 4.
*)
Inductive typ (Eff EV : Set) : Set :=
| t_unit    : typ Eff EV
| t_arrow   : typ Eff EV → effect Eff EV → typ Eff EV → typ Eff EV
| t_forallE : typ Eff (inc EV) → typ Eff EV
.

Arguments t_unit    [Eff] [EV].
Arguments t_arrow   [Eff] [EV].
Arguments t_forallE [Eff] [EV].

(** Closed types are parametrized by empty set: they have no free variables *)
Notation typ0 Eff := (typ Eff Empty_set).

(** Assuming the infinite set of operation names. In fact set operation names
for given effect with n operations must have the form {0,1,2,...,n-1}. This
restriction is not forced by the grammar, but by the typing rules (see Typing
module). *)
Definition Op : Set := nat.

(** Grammar of extressions and values. Types expr and value are parametrized
by a set of effect names and set of potentially free effect variables

Representation of each part of syntax is strightforward, except handle
construct. The term
  e_handle l e [e₀, e₁, …, e_n] e_r
corresponds to the term
  handle_l e 
  { op₀ x r. e₀
  ; op₁ x r. e₁
  …
  ; op_n x r. e_n
  ; return x. e_r
  }
Since we are using nameless representation (variable binding is represented
by extending set of potentially free variables), variable names (x,r) are
not visible in the syntax (e.g., term e₀ is used in the context with two
bound variables x and r, so it has a type expr Eff (inc (inc V))).
We also do not need to store operation names in handler: since operation
names for effect l forms a set of consecutive numbers, the handler for
operation op_i is a i-th element of the list.
*)
Inductive expr (Eff V : Set) : Set :=
| e_value  : value Eff V → expr Eff V
| e_app    : expr Eff V → expr Eff V → expr Eff V
| e_eapp   : expr Eff V → expr Eff V
| e_lift   : Eff → expr Eff V → expr Eff V
| e_handle : 
    Eff → expr Eff V → list (expr Eff (inc (inc V))) → expr Eff (inc V) →
    expr Eff V
with value (Eff V : Set) : Set :=
| v_var  : V → value Eff V
| v_unit : value Eff V
| v_lam  : expr Eff (inc V) → value Eff V
| v_elam : expr Eff V → value Eff V
| v_op   : Eff → Op → value Eff V
.

Arguments e_value  [Eff] [V].
Arguments e_app    [Eff] [V].
Arguments e_eapp   [Eff] [V].
Arguments e_lift   [Eff] [V].
Arguments e_handle [Eff] [V].

Arguments v_var  [Eff] [V].
Arguments v_unit [Eff] [V].
Arguments v_lam  [Eff] [V].
Arguments v_elam [Eff] [V].
Arguments v_op   [Eff] [V].

Coercion e_value : value >-> expr.

(** Expresions with 0 1 and 2 potentially free variables and closed values *)
Notation expr0  Eff := (expr  Eff Empty_set).
Notation expr1  Eff := (expr  Eff (inc Empty_set)).
Notation expr2  Eff := (expr  Eff (inc (inc Empty_set))).
Notation value0 Eff := (value Eff Empty_set).

(** ** Evaluation context *)
(** We have two types for representing evaluation context: ectx and
rctx which are evaluation contexts represented inside-out and
outside-in respectively (see the definitions of plug and rplug
operations). This distinction is not crucial, but very convenient:
contexts represented inside-out (ectx) are good for representing the
rest of the program, since the outermost constructor represents a
construct near the place where reduction holds. Such contexts with an
expression can be seen as a zipper over expressions: (E v)[λ x. e] =
E[(λ x. e) v] → E[e{x↦ v}].

On the other hand, the outside-in contexts (rctx) are useful for
building control-stuck-terms (E[op_l v]): in order to build bigger
control-stuck-term (e.g., by applying it to another expression (E[op_l
v] e)) we can just put another constructor around the context (E
e)[op_l v].  *)

Inductive ectx (Eff V : Set) : Set :=
| x_hole   : ectx Eff V
| x_app1   : ectx Eff V → expr Eff V → ectx Eff V
| x_app2   : value Eff V → ectx Eff V → ectx Eff V
| x_eapp   : ectx Eff V → ectx Eff V
| x_lift   : Eff → ectx Eff V → ectx Eff V
| x_handle :
    Eff → ectx Eff V → list (expr Eff (inc (inc V))) → expr Eff (inc V) →
    ectx Eff V
.

Arguments x_hole   [Eff] [V].
Arguments x_app1   [Eff] [V].
Arguments x_app2   [Eff] [V].
Arguments x_eapp   [Eff] [V].
Arguments x_lift   [Eff] [V].
Arguments x_handle [Eff] [V].

Notation ectx0 Eff := (ectx Eff Empty_set).

Fixpoint plug {Eff V : Set} (E : ectx Eff V) (e : expr Eff V) : expr Eff V :=
  match E with
  | x_hole            => e
  | x_app1 E e'       => plug E (e_app e e')
  | x_app2 v E        => plug E (e_app v e)
  | x_eapp E          => plug E (e_eapp e)
  | x_lift   l E      => plug E (e_lift   l e)
  | x_handle l E h re => plug E (e_handle l e h re)
  end.

Inductive rctx (Eff V : Set) : Set :=
| r_hole   : rctx Eff V
| r_app1   : rctx Eff V → expr Eff V → rctx Eff V
| r_app2   : value Eff V → rctx Eff V → rctx Eff V
| r_eapp   : rctx Eff V → rctx Eff V
| r_lift   : Eff → rctx Eff V → rctx Eff V
| r_handle :
    Eff → rctx Eff V → list (expr Eff (inc (inc V))) → expr Eff (inc V) →
    rctx Eff V
.

Arguments r_hole   [Eff] [V].
Arguments r_app1   [Eff] [V].
Arguments r_app2   [Eff] [V].
Arguments r_eapp   [Eff] [V].
Arguments r_lift   [Eff] [V].
Arguments r_handle [Eff] [V].

Notation rctx0 Eff := (rctx Eff Empty_set).

Fixpoint rplug {Eff V : Set} (E : rctx Eff V) (e : expr Eff V) : expr Eff V :=
  match E with
  | r_hole            => e
  | r_app1 E e'       => e_app (rplug E e) e'
  | r_app2 v E        => e_app v (rplug E e)
  | r_eapp E          => e_eapp (rplug E e)
  | r_lift   l E      => e_lift   l (rplug E e)
  | r_handle l E h re => e_handle l (rplug E e) h re
  end.

(** Composition of contexts *)

Fixpoint rcomp {Eff V : Set} (E E' : rctx Eff V) : rctx Eff V :=
  match E with
  | r_hole            => E'
  | r_app1 E e        => r_app1 (rcomp E E') e
  | r_app2 v E        => r_app2 v (rcomp E E')
  | r_eapp E          => r_eapp (rcomp E E')
  | r_lift   l E      => r_lift   l (rcomp E E')
  | r_handle l E h re => r_handle l (rcomp E E') h re
  end.

Fixpoint xcompr {Eff V : Set} (E₀ : ectx Eff V) (E : rctx Eff V) : ectx Eff V :=
  match E with
  | r_hole            => E₀
  | r_app1 E e        => xcompr (x_app1 E₀ e) E
  | r_app2 v E        => xcompr (x_app2 v E₀) E
  | r_eapp E          => xcompr (x_eapp E₀) E
  | r_lift   l E      => xcompr (x_lift   l E₀) E
  | r_handle l E h re => xcompr (x_handle l E₀ h re) E
  end.

(** ** Freedom *)
(** The definition of freedom is slightly more general then the one defined
in the paper (Figure 2). In the paper, context E is n-free for effect l
if E[op_l v] can be handled by (n+1)-th handler outside E. Freedom defined
here has the following intuition: we have rfree_g m l n E if E[e_m] can be
handled by the (n+1)-th handler outside E, where e_m is op_l v lifted m times.
Such generalized notion of freedom is needed in the characterization of
logical relation C (for partial contexts) (see BinaryRelation/ContextRel.v).
*)

Inductive rfree_g {Eff V : Set} (m : nat) (l : Eff) : nat → rctx Eff V → Prop :=
| RF_hole : rfree_g m l m r_hole
| RF_app1 : ∀ n E e,
    rfree_g m l n E →
    rfree_g m l n (r_app1 E e)
| RF_app2 : ∀ n v E,
    rfree_g m l n E →
    rfree_g m l n (r_app2 v E)
| RF_eapp : ∀ n E,
    rfree_g m l n E →
    rfree_g m l n (r_eapp E)
| RF_lift_e : ∀ n E,
    rfree_g m l n E →
    rfree_g m l (S n) (r_lift l E)
| RF_lift_n : ∀ n l' E,
    l' ≠ l →
    rfree_g m l n E →
    rfree_g m l n (r_lift l' E)
| RF_handle_e : ∀ n E re h,
    rfree_g m l (S n) E →
    rfree_g m l n (r_handle l E re h)
| RF_handle_n : ∀ n l' E h re,
    l' ≠ l →
    rfree_g m l n E →
    rfree_g m l n (r_handle l' E h re)
.

(** Notion of freedom from the paper can be regained by taking m = 0 *)
Notation rfree := (rfree_g 0).

(** Notion of freedom generalized for partial maps. Partial maps are
represented as a functions from effects to option nat *)
Definition rfree_m {Eff V : Set} ρ (E : rctx Eff V) : Prop :=
  ∀ l, match ρ l with
       | None   => True
       | Some n => rfree l n E
       end.

(** ** Variable renaming *)
(** Types parametrized by a set of potentially free variables form a functor,
where the mapping operation is a simultaneous variable renaming:
having function f : A → B, we can rename all the variables represented by
type A to variables represented by type B.
*)

(** First we define auxiliary mapping function for inc type *)
Definition inc_map {A B : Set} (f : A → B) (m : inc A) : inc B :=
  match m with
  | VZ   => VZ
  | VS x => VS (f x)
  end.

(** mapping for effects *)
Fixpoint fmap {Eff A B : Set} (f : A → B) (ε : effect Eff A) : effect Eff B :=
  match ε with
  | ef_var μ    => ef_var (f μ)
  | ef_nil      => ef_nil
  | ef_cons l ε => ef_cons l (fmap f ε)
  end.

(** mapping for types *)
Fixpoint tmap {Eff A B : Set} (f : A → B) (τ : typ Eff A) : typ Eff B :=
  match τ with
  | t_unit          => t_unit
  | t_arrow τ₁ ε τ₂ => t_arrow (tmap f τ₁) (fmap f ε) (tmap f τ₂)
  | t_forallE τ     => t_forallE (tmap (inc_map f) τ)
  end.

(** mapping for expressions and values *)
Fixpoint emap {Eff A B : Set} (f : A → B) (e : expr Eff A) : expr Eff B :=
  match e with
  | e_value v     => vmap f v
  | e_app e₁ e₂   => e_app (emap f e₁) (emap f e₂)
  | e_eapp e      => e_eapp (emap f e)
  | e_lift   l e  => e_lift l (emap f e)
  | e_handle l e h re =>
    e_handle l (emap f e)
      (map (emap (inc_map (inc_map f))) h) (emap (inc_map f) re)
  end
with vmap {Eff A B : Set} (f : A → B) (v : value Eff A) : value Eff B :=
  match v with
  | v_var x   => v_var (f x)
  | v_unit    => v_unit
  | v_lam e   => v_lam (emap (inc_map f) e)
  | v_elam e  => v_elam (emap f e)
  | v_op l op => v_op l op
  end.

(** mapping for evaluation contexts *)
Fixpoint rmap {Eff A B : Set} (f : A → B) (E : rctx Eff A) : rctx Eff B :=
  match E with
  | r_hole       => r_hole
  | r_app1 E e   => r_app1 (rmap f E) (emap f e)
  | r_app2 v E   => r_app2 (vmap f v) (rmap f E)
  | r_eapp E     => r_eapp (rmap f E)
  | r_lift   l E => r_lift l (rmap f E)
  | r_handle l E h re =>
    r_handle l (rmap f E)
      (map (emap (inc_map (inc_map f))) h) (emap (inc_map f) re)
  end.

(** Each closed type can be used in any context. In order to do so we have
map empty set to appropriate type
*)
Definition of_empty {V : Set} (x : Empty_set) : V :=
  match x with
  end.

Notation topen := (tmap of_empty).

(**
Each term can be used in a context with one more potentially free variable.
Shifting add one dummy variable to set of potentially free variables of a term
*)
Notation fshift  := (fmap (@VS _)).
Notation tshift  := (tmap (@VS _)).

Notation vshift  := (vmap (@VS _)).
Notation eshift1 := (emap (inc_map (@VS _))).
Notation eshift2 := (emap (inc_map (inc_map (@VS _)))).
Notation rshift  := (rmap (@VS _)).

(** ** Substitution *)
(** Types parametrized by a set of potentially free variables can also form
a monad. The return (unit) operation is a constructor for variable, and
bind operation is a simultaneous substitution:
having function f: A → F B, we can substitute for all variables represented
by type A corresponding terms given by function A.
Note that in general, such simultaneous substitution has type
(A → F B) → G A → G B, where F and G are not necessarily the same.
E.g., substituting values in expressions (ebind) has type:
(A → value Eff B) → expr Eff A → expr Eff B
*)

(** First we define auxiliary lift function that allows to "go with
substitution under a binder". In order to use function f: A → F B,
in the context with one more potentially free variable we need a function
lift f: inc A → F (inc B), which for zero (VZ) return variable zero itself,
but for VS x, returns f x shifted by one (because it is used in the context
with one more potentially free variable).
*)
Definition flift {Eff A B : Set} (f : A → effect Eff B) :
    inc A → effect Eff (inc B) :=
  λ x, match x with
       | VZ   => ef_var VZ
       | VS y => fshift (f y)
       end.

Definition lift {Eff A B : Set} (f : A → value Eff B) :
    inc A → value Eff (inc B) :=
  λ x, match x with
       | VZ   => v_var VZ
       | VS y => vshift (f y)
       end.

(** mapping for effects *)
Fixpoint fbind {Eff A B : Set} (f : A → effect Eff B) (ε : effect Eff A) :
    effect Eff B :=
  match ε with
  | ef_var μ    => f μ
  | ef_nil      => ef_nil
  | ef_cons l ε => ef_cons l (fbind f ε)
  end.

(** mapping for types *)
Fixpoint tbind {Eff A B : Set} (f : A → effect Eff B) (τ : typ Eff A) :
    typ Eff B :=
  match τ with
  | t_unit          => t_unit
  | t_arrow τ₁ ε τ₂ => t_arrow (tbind f τ₁) (fbind f ε) (tbind f τ₂)
  | t_forallE τ     => t_forallE (tbind (flift f) τ)
  end.

(** mapping for expressions and values *)
Fixpoint ebind {Eff A B : Set} (f : A → value Eff B) (e : expr Eff A) :
    expr Eff B :=
  match e with
  | e_value v         => vbind f v
  | e_app e₁ e₂       => e_app (ebind f e₁) (ebind f e₂)
  | e_eapp e          => e_eapp (ebind f e)
  | e_lift   l e      => e_lift l (ebind f e)
  | e_handle l e h re =>
    e_handle l (ebind f e) (map (ebind (lift (lift f))) h) (ebind (lift f) re)
  end
with vbind {Eff A B : Set} (f : A → value Eff B) (v : value Eff A) :
    value Eff B :=
  match v with
  | v_var x   => f x
  | v_unit    => v_unit
  | v_lam e   => v_lam (ebind (lift f) e)
  | v_elam e  => v_elam (ebind f e)
  | v_op l op => v_op l op
  end.

(** mapping for evaluation contexts *)
Fixpoint rbind {Eff A B : Set} (f : A → value Eff B) (E : rctx Eff A)
    : rctx Eff B :=
  match E with
  | r_hole            => r_hole
  | r_app1 E e        => r_app1 (rbind f E) (ebind f e)
  | r_app2 v E        => r_app2 (vbind f v) (rbind f E)
  | r_eapp E          => r_eapp (rbind f E)
  | r_lift   l E      => r_lift l (rbind f E)
  | r_handle l E h re =>
    r_handle l (rbind f E) (map (ebind (lift (lift f))) h) (ebind (lift f) re)
  end.

(** Usual substitution (of term for one variable) can be defined as a special
case of simultaneous substitution, which for first variable (VZ) returns
given term, and for other variables (VS y) returns variable y

Analogously we can define substitution for two variables (needed in the
operational semantics, case for handling an effect).
*)

Definition fsubst_func {Eff EV : Set} (ε : effect Eff EV) (x : inc EV) :
    effect Eff EV :=
  match x with
  | VZ   => ε
  | VS y => ef_var y
  end.

Definition subst_func {Eff V : Set} (v : value Eff V) (x : inc V) :
    value Eff V :=
  match x with
  | VZ   => v
  | VS y => v_var y
  end.

Definition bisubst_func {Eff V : Set} (v1 v2 : value Eff V) (x : inc (inc V)) :
    value Eff V :=
  match x with
  | VZ        => v2
  | VS VZ     => v1
  | VS (VS y) => v_var y
  end.

(** substitution in effects and types *)
Notation fsubst ε' ε := (fbind (fsubst_func ε) ε').
Notation tsubst τ  ε := (tbind (fsubst_func ε) τ).

(** substitution in expressions *)
Notation esubst  e v := (ebind (subst_func v) e).
Notation esubst1 e v := (ebind (lift (subst_func v)) e).
Notation esubst2 e v := (ebind (lift (lift (subst_func v))) e).
Notation ebisubst e v1 v2 := (ebind (bisubst_func v1 v2) e).

(** substitution in values *)
Notation vsubst v₀ v := (vbind (subst_func v) v₀).
Notation vbisubst v v1 v2 := (vbind (bisubst_func v1 v2) v).

(** substitution in evaluation contexts *)
Notation rsubst E v := (rbind (subst_func v) E).

(** mapping and binding defined here satisfies the usual monadic laws
(see SyntaxProperties module)
*)