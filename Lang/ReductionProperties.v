(** Standard properties of the reduction relation defined in
    Reduction.v. Not shown in the paper. *)

Require Import Utf8.
Require Import Lang.Reduction Lang.Syntax.

(** Reduction is compatible with respect to evaluation contexts. *)

Lemma red_in_ectx {Eff : Set} {EffDec : DecEffectEq Eff}
    (E : ectx0 Eff) {e₁ e₂ : expr0 Eff} :
  red e₁ e₂ →
  red (plug E e₁) (plug E e₂).
Proof.
generalize e₁ e₂; clear e₁ e₂; induction E; simpl; intros e₁ e₂ Hred.
+ assumption.
+ apply IHE, red_app1; assumption.
+ apply IHE, red_app2; assumption.
+ apply IHE, red_eapp; assumption.
+ apply IHE, red_lift; assumption.
+ apply IHE, red_handle; assumption.
Qed.

Lemma red_in_rctx {Eff : Set} {EffDec : DecEffectEq Eff}
    (E : rctx0 Eff) {e₁ e₂ : expr0 Eff} :
  red e₁ e₂ →
  red (rplug E e₁) (rplug E e₂).
Proof.
generalize e₁ e₂; clear e₁ e₂; induction E; simpl; intros e₁ e₂ Hred.
+ assumption.
+ apply red_app1, IHE; assumption.
+ apply red_app2, IHE; assumption.
+ apply red_eapp, IHE; assumption.
+ apply red_lift, IHE; assumption.
+ apply red_handle, IHE; assumption.
Qed.

(** The reflexive and transitive closure of one-step reduction
    describes finite computations. *)

Lemma red_rtc_to_red_n {Eff : Set} (e₁ e₂ : expr0 Eff) :
  red_rtc e₁ e₂ → ∃ n, red_n n e₁ e₂.
Proof.
induction 1 as [ | e₁ e₂ e₃ Hred Hrtc [ n Hredn ] ].
+ exists 0; constructor.
+ exists (S n); econstructor; eassumption.
Qed.

(** Control-stuck terms decomppose uniquely. *)

Ltac auto_determ :=
  match goal with
  | [ H: red (e_value _) _ |- _ ] => inversion H
  | [ H: rplug ?E (e_app _ _) = e_value _ |- _ ] =>
    destruct E; discriminate
  | [ H: e_value _ = rplug ?E (e_app _ _) |- _ ] =>
    destruct E; discriminate
  | [ H: e_app _ _ = e_app _ _ |- _ ] =>
    injection H; clear H; intros; auto_determ
  | [ H: e_eapp _ = e_eapp _ |- _ ] =>
    injection H; clear H; intros; auto_determ
  | [ H: e_lift _ _ = e_lift _ _ |- _ ] =>
    injection H; clear H; intros; auto_determ
  | [ H: e_handle _ _ _ _ = e_handle _ _ _ _ |- _ ] =>
    injection H; clear H; intros; auto_determ
  | _ => idtac
  end.

Lemma cstuck_unique {Eff : Set} E₁ E₂ l₁ l₂ op₁ op₂ (v₁ v₂ : value0 Eff) :
  rplug E₁ (e_app (v_op l₁ op₁) v₁) = rplug E₂ (e_app (v_op l₂ op₂) v₂) →
  (E₁ = E₂ ∧ v₁ = v₂) ∧ (l₁ = l₂ ∧ op₁ = op₂).
Proof.
generalize E₂; clear E₂; induction E₁; simpl; intros E₂ Heq.
+ destruct E₂; simpl in Heq; try discriminate; auto_determ.
  subst; auto.
+ destruct E₂; simpl in Heq; try discriminate; auto_determ.
  match goal with
  [ H: rplug _ _ = rplug _ _ |- _ ] => apply IHE₁ in H
  end; intuition; subst; auto.
+ destruct E₂; simpl in Heq; try discriminate; auto_determ.
  match goal with
  [ H: rplug _ _ = rplug _ _ |- _ ] => apply IHE₁ in H
  end; intuition; subst; auto.
+ destruct E₂; simpl in Heq; try discriminate; auto_determ.
  match goal with
  [ H: rplug _ _ = rplug _ _ |- _ ] => apply IHE₁ in H
  end; intuition; subst; auto.
+ destruct E₂; simpl in Heq; try discriminate; auto_determ.
  match goal with
  [ H: rplug _ _ = rplug _ _ |- _ ] => apply IHE₁ in H
  end; intuition; subst; auto.
+ destruct E₂; simpl in Heq; try discriminate; auto_determ.
  match goal with
  [ H: rplug _ _ = rplug _ _ |- _ ] => apply IHE₁ in H
  end; intuition; subst; auto.
Qed.

(** The n-freeness of a context for an effect is determined
    uniquely. *)

Lemma rfree_unique {Eff : Set} (E : rctx0 Eff) l n₁ n₂ :
  rfree l n₁ E → rfree l n₂ E → n₁ = n₂.
Proof.
intro Hfree; generalize n₂; clear n₂.
induction Hfree; simpl; intros n₂ Hfree2.
+ inversion Hfree2; auto.
+ inversion Hfree2; auto.
+ inversion Hfree2; auto.
+ inversion Hfree2; auto.
+ inversion Hfree2.
  - auto.
  - exfalso; auto.
+ inversion Hfree2.
  - exfalso; subst; auto.
  - auto.
+ inversion Hfree2.
  - apply eq_add_S; auto.
  - exfalso; auto.
+ inversion Hfree2.
  - exfalso; subst; auto.
  - auto.
Qed.

(** A control-stuck term on an effect l cannot get unstuck by an
    evaluation context that is free for l. *)

Lemma cstuck_not_reduce {Eff : Set} E l op (v : value0 Eff) e n :
  rfree l n E → red (rplug E (e_app (v_op l op) v)) e → False.
Proof.
intro Hfree; generalize e; clear e.
induction Hfree; simpl; intros e' Hred.
+ inversion Hred; auto_determ.
+ inversion Hred; auto_determ.
  subst; eapply IHHfree; eassumption.
+ inversion Hred; auto_determ.
  subst; eapply IHHfree; eassumption.
+ inversion Hred; auto_determ.
  subst; eapply IHHfree; eassumption.
+ inversion Hred; auto_determ.
  subst; eapply IHHfree; eassumption.
+ inversion Hred; auto_determ.
  subst; eapply IHHfree; eassumption.
+ inversion Hred; auto_determ.
  - match goal with
    [ H: rplug _ _ = rplug _ _ |- _ ] => apply cstuck_unique in H
    end; intuition; subst.
    exfalso; assert (HS_eq_0 : S n = 0).
      { eapply rfree_unique; eassumption. }
    discriminate.
  - subst; eapply IHHfree; eassumption.
+ inversion Hred; auto_determ.
  - match goal with
    [ H: rplug _ _ = rplug _ _ |- _ ] => apply cstuck_unique in H
    end; intuition; subst.
  - subst; eapply IHHfree; eassumption.
Qed.

(** Each operation is handled in a unique way by a given handler. *)

Lemma handler_unique {Eff V : Set} (he₁ he₂ : expr Eff (inc (inc V))) h op :
  handler he₁ h op → handler he₂ h op → he₁ = he₂.
Proof.
induction 1; inversion 1; auto.
Qed.

(** The reduction relation is deterministic. *)

Lemma red_determ {Eff : Set} (e e₁ e₂ : expr0 Eff) :
  red e e₁ → red e e₂ → e₁ = e₂.
Proof.
intro Hred; generalize e₂; clear e₂.
induction Hred; intros r₂ Hr₂.
+ inversion Hr₂; auto_determ; reflexivity.
+ inversion Hr₂; auto_determ; reflexivity.
+ inversion Hr₂; auto_determ; reflexivity.
+ inversion Hr₂; auto_determ; reflexivity.
+ inversion Hr₂; auto_determ.
  - match goal with
    [ H: rplug _ _ = rplug _ _ |- _ ] => apply cstuck_unique in H
    end; intuition; subst.
    match goal with
    [ H: handler _ _ _ |- _ ] =>
      eapply handler_unique in H; [ rewrite <- H; reflexivity | ]
    end.
    assumption.
  - exfalso; eapply cstuck_not_reduce; eassumption.
+ inversion Hr₂; subst; auto_determ.
  erewrite IHHred; [ reflexivity | ]; assumption.
+ inversion Hr₂; subst; auto_determ.
  erewrite IHHred; [ reflexivity | ]; assumption.
+ inversion Hr₂; subst; auto_determ.
  erewrite IHHred; [ reflexivity | ]; assumption.
+ inversion Hr₂; subst; auto_determ.
  erewrite IHHred; [ reflexivity | ]; assumption.
+ inversion Hr₂; subst; auto_determ.
  - exfalso; eapply cstuck_not_reduce; eassumption.
  - erewrite IHHred; [ reflexivity | ]; assumption.
Qed.