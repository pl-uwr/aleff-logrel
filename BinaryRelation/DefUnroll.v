(** This module provides convenience lemmas for rolling and unrolling
the definitions of the logical relation. This is useful, since going
through the fixed-point operation explicitly can be somewhat
cumbersome. *)
Require Import Utf8.
Require Import IxFree.Lib.
Require Import Lang.Syntax Lang.Typing.
Require Import BinaryRelation.Core.
Require Import BinaryRelation.EffectSubst.

Section DefUnroll.
Context {Eff : Set} {EffDec : DecEffectEq Eff}.
Context (Σ : eff_sig Eff).

Lemma rel_v_arrow_roll {EV : Set} σ ε τ
    (η : EV → IRel rel_row_sig)
    (v₁ v₂ : value0 Eff)
    {n : nat} :
  (∀ u₁ u₂ : value0 Eff,
    (n ⊨ 〚Σ ⊢ σ〛 η u₁ u₂ ⇒ E〚Σ ⊢ τ // ε〛 η (e_app v₁ u₁) (e_app v₂ u₂))) →
  n ⊨ 〚Σ ⊢ t_arrow σ ε τ〛 η v₁ v₂.
Proof.
intro H; simpl.
iintro u₁; iintro u₂; apply H.
Qed.

Lemma rel_s_roll {EV : Set}
    (ρ₁ ρ₂ : Eff → option nat)
    (ν : IRel rel_e2_sig)
    τ ε (η : EV → IRel rel_row_sig)
    E₁ E₂ e₁ e₂
    {n : nat} :
  n ⊨ (R〚Σ ⊢ ε〛 η) e₁ e₂ ρ₁ ρ₂ ν →
  rfree_m ρ₁ E₁ →
  rfree_m ρ₂ E₂ →
  (∀ e₁' e₂', n ⊨ ν e₁' e₂' ⇒
    ▷ E〚Σ ⊢ τ // ε〛 η (rplug E₁ e₁') (rplug E₂ e₂')) →
  n ⊨ S〚Σ ⊢ τ // ε〛 η E₁ E₂ e₁ e₂.
Proof.
intros Hr Hρ₁ Hρ₂ Hν.
unfold rel_simpl_cl.
iexists ρ₁; iexists ρ₂; iexists ν.
isplit; [ | isplit ].
+ exact Hr.
+ iintro; split.
  - exact Hρ₁.
  - exact Hρ₂.
+ iintro e₁'; iintro e₂'.
  specialize (Hν e₁' e₂').
  iintro He'; assert (Hν' := I_arrow_elim Hν).
  specialize (Hν' He').
  later_shift.
  apply rel_expr_cl_fix_roll; exact Hν'.
Qed.

Lemma rel_k_roll {EV : Set}
    τ ε (η : EV → IRel rel_row_sig)
    F₁ F₂
    {n : nat} :
  (∀ v₁ v₂, n ⊨ 〚Σ ⊢ τ〛 η v₁ v₂ ⇒
      ProgramObs.Obs (plug F₁ v₁) (plug F₂ v₂)) →
  (∀ E₁ E₂ e₁ e₂, n ⊨ S〚Σ ⊢ τ // ε〛 η E₁ E₂ e₁ e₂ ⇒
      ProgramObs.Obs (plug F₁ (rplug E₁ e₁)) (plug F₂ (rplug E₂ e₂))) →
  n ⊨ K〚Σ ⊢ τ // ε〛 η F₁ F₂.
Proof.
intros Hv Hs.
unfold rel_ectx_cl; isplit.
+ iintro v₁; iintro v₂; apply Hv.
+ iintro E₁; iintro E₂; iintro e₁; iintro e₂; apply Hs.
Qed.

Definition coerce_rel_v2e (R : IRel (@rel_v2_sig Eff)) : IRel rel_e2_sig :=
  λ e₁ e₂,
    match e₁, e₂ with
    | e_value v₁, e_value v₂ => ▷ R v₁ v₂
    | _, _ => (False)ᵢ
    end.

Lemma rel_l_intro l {EV : Set}
    (η : EV → IRel rel_row_sig)
    op Θ
    (v₁ v₂ : value0 Eff)
    {n : nat} :
  Ef[ Σ l ⊢ op ∷ Θ] →
  n ⊨ ▷ 〚Σ ⊢ topen (op_input Θ)〛 η v₁ v₂ →
  n ⊨ L〚Σ ⊢ l〛
    (e_app (v_op l op) v₁)
    (e_app (v_op l op) v₂)
    (λ l', if dec_effect_eq l l' then Some 0 else None)
    (λ l', if dec_effect_eq l l' then Some 0 else None)
    (coerce_rel_v2e (〚Σ ⊢ topen (op_output Θ)〛 η)).
Proof.
intros HΘ Hv.
unfold Frel_eff.
iexists op; iexists Θ; iexists v₁; iexists v₂.
isplit; [ | isplit ].
+ iintro; constructor.
  - exact HΘ.
  - destruct (dec_effect_eq l l); [ reflexivity | ].
    exfalso; auto.
  - destruct (dec_effect_eq l l); [ reflexivity | ].
    exfalso; auto.
  - intros l' Hl'.
    destruct (dec_effect_eq l l'); [ | reflexivity ].
    exfalso; auto.
  - intros l' Hl'.
    destruct (dec_effect_eq l l'); [ | reflexivity ].
    exfalso; auto.
+ later_shift; apply rel0_v_fix_roll.
  eapply rel_v_weaken_r; [ | exact Hv ].
    iintro μ; destruct μ.
+ iintro w₁; iintro w₂; isplit.
  - simpl; iintro Hw; later_shift.
    apply rel0_v_fix_roll.
    eapply rel_v_weaken_r; [ | exact Hw ].
    iintro μ; destruct μ.
  - simpl; iintro Hw; later_shift.
    apply rel0_v_fix_unroll in Hw.
    eapply rel_v_weaken_l; [ | exact Hw ].
    iintro μ; destruct μ.
Qed.

Lemma rel_e_open_elim {V EV : Set}
    (Γ : V → typ Eff EV) (e₁ e₂ : expr Eff V) τ ε γ₁ γ₂ η n :
  n ⊨ T〚Σ; Γ ⊨ e₁ ≾ e₂ ∷ τ // ε〛 →
  n ⊨ G〚Σ ⊢ Γ〛 η γ₁ γ₂ →
  n ⊨ E〚Σ ⊢ τ // ε〛 η (ebind γ₁ e₁) (ebind γ₂ e₂).
Proof.
intros He Hγ; unfold rel_e_open in He;
iespecialize He; iapply He; assumption.
Qed.

End DefUnroll.