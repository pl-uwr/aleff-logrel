(** This module proves the type soundness of our calculus as a
corollary of the soundness of the logical relation, and the
fundamental property.*)
(** Since both the required properties have been established, the
proof is straightforward, and proceeds in the standard manner.*)
Require Import Utf8.
Require Import IxFree.Lib.
Require Import Lang.Syntax Lang.Typing Lang.CtxApprox.
Require Import Lang.Reduction Lang.ReductionProperties.
Require Import BinaryRelation.Core BinaryRelation.Soundness.

Section TypeSoundness.
Context {Eff : Set} {EffDec : DecEffectEq Eff}.
Context {Σ : eff_sig Eff}.

Lemma obs_dont_go_wrong n (e e' f : expr0 Eff) :
  red_n n e e' →
  irred e' →
  n ⊨ ProgramObs.Obs e f →
  e' = v_unit.
Proof.
intros Hredn Hirr; induction Hredn as [ | ? ? ? ? Hred ]; intro HObs.
+ apply ProgramObs.Obs_unroll in HObs; unfold ProgramObs.F_Obs in HObs.
  idestruct HObs as HObs HObs.
  - apply I_Prop_elim in HObs; apply HObs.
  - idestruct HObs as e' HObs; idestruct HObs as HObs Hl.
    apply I_Prop_elim in HObs; exfalso; eapply Hirr; eassumption.
+ apply ProgramObs.Obs_unroll in HObs; unfold ProgramObs.F_Obs in HObs.
  idestruct HObs as HObs HObs.
  - apply I_Prop_elim in HObs; destruct HObs; subst.
    inversion Hred.
  - idestruct HObs as e' HObs; idestruct HObs as Hred' Hl.
    apply I_Prop_elim in Hred'.
    apply IHHredn; [ assumption | ].
    erewrite (red_determ _ _ _ Hred Hred').
    apply I_valid_elim in Hl; assumption.
Qed.

Theorem type_soundness (e e' : expr0 Eff) :
  T[ Σ; of_empty ⊢ e ∷ (t_unit : typ0 Eff) // ef_nil ] →
  red_rtc e e' →
  irred e' →
  e' = v_unit.
Proof.
intros Htp Hrtc Hirr.
apply red_rtc_to_red_n in Hrtc; destruct Hrtc as [ n Hred ].
eapply obs_dont_go_wrong; [ eassumption | assumption | ].
eapply adequacy_idx.
eapply precongruence with (C := ctx_hole).
+ apply CT_hole.
+ eapply fundamental_property; eassumption.
Qed.

End TypeSoundness.