(** This module contains proofs that the logical relation is
compatible with the rules of the type system. *)
(** Compatibility is shown by proving for each rule of the type system
a lemma of the same shape, but with a logical approximation instead of
typing judgment. The development at this point is mostly standard for
the logical relations technique, safe for the proof by Löb induction
of the handler case.
*)
Require Import Utf8.
Require Import IxFree.Lib.
Require Import Lang.Syntax Lang.SyntaxProperties.
Require Import Lang.Typing Lang.Reduction.
Require Import BinaryRelation.Core BinaryRelation.DefUnroll.
Require Import BinaryRelation.EffectSubst BinaryRelation.Properties.
Require Import BinaryRelation.ContextRel BinaryRelation.HandlerRel.

Section Compatibility.
Context {Eff : Set} {EffDec : DecEffectEq Eff}.
Context (Σ : eff_sig Eff).

(* ========================================================================= *)
(* variable *)

Lemma var_compat_v {EV V : Set} (Γ : V → typ Eff EV) (x : V) n :
  n ⊨ T〚Σ; Γ ⊨ v_var x ≾ v_var x ∷ Γ x〛.
Proof.
iintro η; iintro γ₁; iintro γ₂; iintro Hγ; simpl.
unfold rel_g in Hγ; iespecialize Hγ; exact Hγ.
Qed.

Lemma var_compat {EV V : Set} (Γ : V → typ Eff EV) ε (x : V) n :
  n ⊨ T〚Σ; Γ ⊨ v_var x ≾ v_var x ∷ Γ x // ε〛.
Proof.
apply rel_v_open_in_e, var_compat_v.
Qed.

(* ========================================================================= *)
(* unit *)

Lemma unit_compat_cl_v {EV : Set} (η : EV → IRel rel_row_sig) n :
  n ⊨ 〚Σ ⊢ t_unit〛 η v_unit v_unit.
Proof.
simpl; iintro; constructor.
Qed.

Lemma unit_compat_cl {EV : Set} (η : EV → IRel rel_row_sig) ε n :
  n ⊨ E〚Σ ⊢ t_unit // ε〛 η v_unit v_unit.
Proof.
apply rel_v_in_e, unit_compat_cl_v.
Qed.

Lemma unit_compat_v {EV V : Set} (Γ : V → typ Eff EV) n :
  n ⊨ T〚Σ; Γ ⊨ v_unit ≾ v_unit ∷ t_unit〛.
Proof.
iintro η; iintro γ₁; iintro γ₂; iintro Hγ; simpl.
simpl; iintro; constructor.
Qed.

Lemma unit_compat {EV V : Set} (Γ : V → typ Eff EV) ε n :
  n ⊨ T〚Σ; Γ ⊨ v_unit ≾ v_unit ∷ t_unit // ε〛.
Proof.
apply rel_v_open_in_e, unit_compat_v.
Qed.

(* ========================================================================= *)
(* lambda *)

Lemma lam_compat_v {EV V : Set} (Γ : V → typ Eff EV) τ₁ τ₂ ε
    (e₁ e₂ : expr Eff (inc V)) n :
  n ⊨ T〚Σ; Γ ,+ τ₁ ⊨ e₁ ≾ e₂ ∷ τ₂ // ε〛 →
  n ⊨ T〚Σ; Γ ⊨ v_lam e₁ ≾ v_lam e₂ ∷ t_arrow τ₁ ε τ₂〛.
Proof.
intro He.
iintro η; iintro γ₁; iintro γ₂; iintro Hγ; simpl.
iintro v₁; iintro v₂; iintro Hv.
eapply rel_e_red_r; [ apply red_beta | ].
eapply rel_e_red_l; [ apply red_beta | ].
rewrite esubst_bind_lift, esubst_bind_lift.
iintro.
iespecialize He; iapply He.
iintro x; destruct x as [ | x ]; simpl.
+ assumption.
+ iespecialize Hγ; exact Hγ.
Qed.

Lemma lam_compat {EV V : Set} (Γ : V → typ Eff EV) τ₁ τ₂ ε ε'
    (e₁ e₂ : expr Eff (inc V)) n :
  n ⊨ T〚Σ; Γ ,+ τ₁ ⊨ e₁ ≾ e₂ ∷ τ₂ // ε〛 →
  n ⊨ T〚Σ; Γ ⊨ v_lam e₁ ≾ v_lam e₂ ∷ t_arrow τ₁ ε τ₂ // ε'〛.
Proof.
intro He; apply rel_v_open_in_e, lam_compat_v; assumption.
Qed.

(* ========================================================================= *)
(* effect-lambda *)

Lemma elam_compat_v {EV V : Set} (Γ : V → typ Eff EV) τ
    (e₁ e₂ : expr Eff V) n :
  n ⊨ T〚Σ; Γ ↑+ ⊨ e₁ ≾ e₂ ∷ τ // ef_nil〛 →
  n ⊨ T〚Σ; Γ ⊨ v_elam e₁ ≾ v_elam e₂ ∷ t_forallE τ〛.
Proof.
intro He.
iintro η; iintro γ₁; iintro γ₂; iintro Hγ; simpl.
iintro R; change (n ⊨ E〚Σ ⊢ τ // ef_nil〛 (η ,+ R)
    (e_eapp (v_elam (ebind γ₁ e₁))) (e_eapp (v_elam (ebind γ₂ e₂)))).
eapply rel_e_red_r; [ apply red_ebeta | ].
eapply rel_e_red_l; [ apply red_ebeta | ].
iintro.
iespecialize He; iapply He.
iintro x; unfold shift_env.
apply rel_v_weaken_l with (η₂ := η).
+ iintro μ; iintro f₁; iintro f₂; iintro ρ₁; iintro ρ₂; iintro ν; simpl.
  auto_contr.
+ iespecialize Hγ; exact Hγ.
Qed.

Lemma elam_compat {EV V : Set} (Γ : V → typ Eff EV) τ ε
    (e₁ e₂ : expr Eff V) n :
  n ⊨ T〚Σ; Γ ↑+ ⊨ e₁ ≾ e₂ ∷ τ // ef_nil〛 →
  n ⊨ T〚Σ; Γ ⊨ v_elam e₁ ≾ v_elam e₂ ∷ t_forallE τ // ε〛.
Proof.
intro He; apply rel_v_open_in_e, elam_compat_v; assumption.
Qed.

(* ========================================================================= *)
(* operations *)

Lemma op_compat_cl_v {EV : Set} (η : EV → IRel rel_row_sig) l op Θ ε n :
  (Ef[ Σ l ⊢ op ∷ Θ ]) →
  n ⊨ 〚Σ ⊢ t_arrow (topen (op_input Θ)) (ef_cons l ε) (topen (op_output Θ))〛 η
    (v_op l op) (v_op l op).
Proof.
intro Hop; apply rel_v_arrow_roll.
intros v₁ v₂; iintro Hv.
apply rel_s_in_e with (E₁ := r_hole) (E₂ := r_hole).
eapply rel_s_roll.
+ ileft; apply rel_l_intro.
  - exact Hop.
  - iintro; exact Hv.
+ intro l'; destruct (dec_effect_eq l l'); constructor.
+ intro l'; destruct (dec_effect_eq l l'); constructor.
+ intros e₁' e₂'; iintro He'.
  destruct e₁' as [ u₁ | | | | ];
    try (apply I_Prop_elim in He'; destruct He'; fail).
  destruct e₂' as [ u₂ | | | | ];
    try (apply I_Prop_elim in He'; destruct He'; fail).
  simpl in He'.
  later_shift.
  apply rel_v_in_e; assumption.
Qed.

Lemma op_compat_cl {EV : Set} (η : EV → IRel rel_row_sig) l op Θ ε ε' n :
  (Ef[ Σ l ⊢ op ∷ Θ ]) →
  n ⊨ E〚Σ ⊢ 
        t_arrow (topen (op_input Θ)) (ef_cons l ε) (topen (op_output Θ)) 
        // ε'〛 η (v_op l op) (v_op l op).
Proof.
intro Hop; apply rel_v_in_e, op_compat_cl_v; assumption.
Qed.

Lemma op_compat_v {EV V : Set} (Γ : V → typ Eff EV) l op Θ ε n :
  (Ef[ Σ l ⊢ op ∷ Θ ]) →
  n ⊨ T〚Σ; Γ ⊨ v_op l op ≾ v_op l op ∷
    t_arrow (topen (op_input Θ)) (ef_cons l ε) (topen (op_output Θ))〛.
Proof.
intro Hop; iintro η; iintro γ₁; iintro γ₂; iintro Hγ.
apply op_compat_cl_v; assumption.
Qed.

Lemma op_compat {EV V : Set} (Γ : V → typ Eff EV) l op Θ ε ε' n :
  (Ef[ Σ l ⊢ op ∷ Θ ]) →
  n ⊨ T〚Σ; Γ ⊨ v_op l op ≾ v_op l op ∷
    t_arrow (topen (op_input Θ)) (ef_cons l ε) (topen (op_output Θ)) // ε'〛.
Proof.
intro Hop; iintro η; iintro γ₁; iintro γ₂; iintro Hγ.
apply op_compat_cl; assumption.
Qed.

(* ========================================================================= *)
(* application *)

Lemma r_app2_compat_cl {EV : Set} (η : EV → IRel rel_row_sig)
    v₁ v₂ σ τ ε n :
  n ⊨ 〚Σ ⊢ t_arrow σ ε τ〛 η v₁ v₂ →
  n ⊨ C〚Σ ⊢ σ // ε ↝ τ // ε〛 η (r_app2 v₁ r_hole) (r_app2 v₂ r_hole).
Proof.
intro Hv.
apply rel_c_intro with (M := ModNone _).
+ simpl; simpl in Hv;
  intros u₁ u₂; iespecialize Hv; exact Hv.
+ intro l; simpl.
  iintro; split; repeat constructor.
Qed.

Lemma r_app1_compat_cl {EV : Set} (η : EV → IRel rel_row_sig)
    e₁ e₂ σ τ ε n :
  n ⊨ E〚Σ ⊢ σ // ε〛 η e₁ e₂ →
  n ⊨ C〚Σ ⊢ t_arrow σ ε τ // ε ↝ τ // ε〛 η 
    (r_app1 r_hole e₁) (r_app1 r_hole e₂).
Proof.
intro He.
apply rel_c_intro with (M := ModNone _).
+ intros v₁ v₂; iintro Hv.
  change (n ⊨ E〚Σ ⊢ τ // ε〛 η
    (rplug (r_app2 v₁ r_hole) e₁) (rplug (r_app2 v₂ r_hole) e₂)).
  eapply rel_c_compat_e; [ | exact He ].
  apply r_app2_compat_cl; assumption.
+ intro l; simpl.
  iintro; split; repeat constructor.
Qed.

Lemma app_compat_cl {EV : Set} (η : EV → IRel rel_row_sig)
    f₁ f₂ e₁ e₂ σ τ ε n :
  n ⊨ E〚Σ ⊢ t_arrow σ ε τ // ε〛 η f₁ f₂ →
  n ⊨ E〚Σ ⊢ σ // ε〛 η e₁ e₂ →
  n ⊨ E〚Σ ⊢ τ // ε〛 η (e_app f₁ e₁) (e_app f₂ e₂).
Proof.
intros Hf He.
change (n ⊨ E〚Σ ⊢ τ // ε〛 η
     (rplug (r_app1 r_hole e₁) f₁) (rplug (r_app1 r_hole e₂) f₂)).
eapply rel_c_compat_e; [ | exact Hf ].
apply r_app1_compat_cl; assumption.
Qed.

Lemma app_compat {EV V : Set} (Γ : V → typ Eff EV) f₁ f₂ e₁ e₂ σ τ ε n :
  n ⊨ T〚Σ; Γ ⊨ f₁ ≾ f₂ ∷ t_arrow σ ε τ // ε〛 →
  n ⊨ T〚Σ; Γ ⊨ e₁ ≾ e₂ ∷ σ // ε〛 →
  n ⊨ T〚Σ; Γ ⊨ e_app f₁ e₁ ≾ e_app f₂ e₂ ∷ τ // ε〛.
Proof.
intros Hf He.
iintro η; iintro γ₁; iintro γ₂; iintro Hγ.
simpl; eapply app_compat_cl.
+ iespecialize Hf; iapply Hf; assumption.
+ iespecialize He; iapply He; assumption.
Qed.

(* ========================================================================= *)
(* effect application *)

Lemma r_eapp_compat_cl {EV : Set} (η : EV → IRel rel_row_sig)
    τ ε ε' n :
  n ⊨ C〚Σ ⊢ t_forallE τ // ε' ↝ tsubst τ ε // ε'〛 η
    (r_eapp r_hole) (r_eapp r_hole).
Proof.
apply rel_c_intro with (M := ModNone _).
+ intros v₁ v₂; iintro Hv.
  change (n ⊨
    ∀ᵢ R, E〚Σ ⊢ τ // ef_nil〛 (η ,+ R) (e_eapp v₁) (e_eapp v₂)) in Hv.
  ispecialize Hv (R〚Σ ⊢ ε〛 η).
  rewrite <- (fsubst_shift ε ε').
  eapply rel_e_subst_l; [ | apply rel_e_coerce_pure; exact Hv ].
  iintro μ; destruct μ as [ | μ ]; simpl;
    repeat iintro; auto_contr.
+ intro l; simpl.
  iintro; split; repeat constructor.
Qed.

Lemma eapp_compat_cl {EV : Set} (η : EV → IRel rel_row_sig)
    e₁ e₂ τ ε ε' n :
  n ⊨ E〚Σ ⊢ t_forallE τ // ε'〛 η e₁ e₂ →
  n ⊨ E〚Σ ⊢ tsubst τ ε // ε'〛 η (e_eapp e₁) (e_eapp e₂).
Proof.
intro He.
change (n ⊨ E〚Σ ⊢ tsubst τ ε // ε'〛 η
  (rplug (r_eapp r_hole) e₁) (rplug (r_eapp r_hole) e₂)).
eapply rel_c_compat_e; [ | exact He ].
apply r_eapp_compat_cl.
Qed.

Lemma eapp_compat {EV V : Set} (Γ : V → typ Eff EV) e₁ e₂ τ ε ε' n :
  n ⊨ T〚Σ; Γ ⊨ e₁ ≾ e₂ ∷ t_forallE τ // ε'〛 →
  n ⊨ T〚Σ; Γ ⊨ e_eapp e₁ ≾ e_eapp e₂ ∷ tsubst τ ε // ε'〛.
Proof.
intro He.
iintro η; iintro γ₁; iintro γ₂; iintro Hγ.
simpl; apply eapp_compat_cl.
iespecialize He; iapply He; assumption.
Qed.

(* ========================================================================= *)
(* lift *)

Lemma r_lift_compat_cl {EV : Set} (η : EV → IRel rel_row_sig)
    l τ ε n :
  n ⊨ C〚Σ ⊢ τ // ε ↝ τ // ef_cons l ε〛 η (r_lift l r_hole) (r_lift l r_hole).
Proof.
apply rel_c_intro with (M := ModShift l (ModNone _)).
+ intros v₁ v₂; iintro Hv.
  eapply rel_e_red_r; [ apply red_lift_val | ].
  eapply rel_e_red_l; [ apply red_lift_val | ].
  iintro; apply rel_v_in_e; assumption.
+ intro l'; simpl.
  destruct (dec_effect_eq l l'); [ subst | ];
    iintro; repeat constructor; assumption.
Qed.

Lemma lift_compat_cl {EV : Set} (η : EV → IRel rel_row_sig)
    l e₁ e₂ τ ε n :
  n ⊨ E〚Σ ⊢ τ // ε〛 η e₁ e₂ →
  n ⊨ E〚Σ ⊢ τ // ef_cons l ε〛 η (e_lift l e₁) (e_lift l e₂).
Proof.
intro He.
change (n ⊨ E〚Σ ⊢ τ // ef_cons l ε〛 η
  (rplug (r_lift l r_hole) e₁) (rplug (r_lift l r_hole) e₂)).
eapply rel_c_compat_e; [ | exact He ].
apply r_lift_compat_cl.
Qed.

Lemma lift_compat {EV V : Set} (Γ : V → typ Eff EV) l e₁ e₂ τ ε n :
  n ⊨ T〚Σ; Γ ⊨ e₁ ≾ e₂ ∷ τ // ε〛 →
  n ⊨ T〚Σ; Γ ⊨ e_lift l e₁ ≾ e_lift l e₂ ∷ τ // ef_cons l ε〛.
Proof.
intro He.
iintro η; iintro γ₁; iintro γ₂; iintro Hγ.
simpl; apply lift_compat_cl.
iespecialize He; iapply He; assumption.
Qed.

(* ========================================================================= *)
(* handler *)

Lemma r_handle_compat_cl {EV : Set} (η : EV → IRel rel_row_sig)
    l h₁ h₂ re₁ re₂ σ τ ε n :
  n ⊨ H〚Σ; Σ l ⊢ τ // ε〛 η h₁ h₂ →
  n ⊨ E〚Σ; σ ⊢ τ // ε〛 η re₁ re₂ →
  n ⊨ C〚Σ ⊢ σ // ef_cons l ε ↝ τ // ε〛 η
    (r_handle l r_hole h₁ re₁) (r_handle l r_hole h₂ re₂).
Proof.
intros Hh Hre.
loeb_induction.
apply rel_c_intro with (M := ModHandle l (ModNone _)).
+ intros v₁ v₂; iintro Hv; simpl.
  eapply rel_e_red_r; [ apply red_handle_val | ].
  eapply rel_e_red_l; [ apply red_handle_val | ].
  iintro.
  unfold rel_e_cl1 in Hre.
  iespecialize Hre; iapply Hre; assumption.
+ intro l'; simpl; isplit.
  { destruct (dec_effect_eq l l'); [ subst | ];
    iintro; repeat constructor; assumption.
  }
  destruct (dec_effect_eq l' l); [ subst | iintro; constructor ].
  iintro op; iintro Θ; iintro v₁; iintro v₂; iintro E₁; iintro E₂.
  iintro Heff.
  idestruct Heff as Hop Heff; idestruct Heff as Hv Heff.
  apply I_Prop_elim in Hop; destruct Hop as [ Hop [ HE₁ HE₂ ] ].
  assert (Hhe := rel_h_handle _ _ _ _ _ _ _ _ _ _ Hop Hh).
  idestruct Hhe as he₁ Hhe; idestruct Hhe as he₂ Hhe.
  idestruct Hhe as Hhe Hhdl.
  apply I_Prop_elim in Hhdl; destruct Hhdl as [ Hhe₁ Hhe₂ ].
  eapply rel_e_red_r; [ apply red_handle_op; eassumption | ].
  eapply rel_e_red_l; [ apply red_handle_op; eassumption | ].
  later_shift.
  unfold rel_e_cl2 in Hhe; iespecialize Hhe; iapply Hhe; clear Hhe.
  isplit; [ exact Hv | ].
  simpl.
  iintro u₁; iintro u₂; iintro Hu.
  eapply rel_e_red_r; [ apply red_beta | ].
  eapply rel_e_red_l; [ apply red_beta | ]; iintro.
  simpl.
  rewrite bind_rplug, rsubst_shift, esubst_shift1; simpl.
  rewrite bind_rplug, rsubst_shift, esubst_shift1; simpl.
  rewrite List_map2_id; [ | intro; apply esubst_shift2 ].
  rewrite List_map2_id; [ | intro; apply esubst_shift2 ].
  change (n ⊨ E〚Σ ⊢ τ // ε〛 η
    (rplug (r_handle l r_hole h₁ re₁) (rplug E₁ u₁))
    (rplug (r_handle l r_hole h₂ re₂) (rplug E₂ u₂))).
  eapply rel_c_compat_e; [ exact IH | ].
  iespecialize Heff; iapply Heff.
  exact Hu.
Qed.

Lemma handle_compat_cl {EV : Set} (η : EV → IRel rel_row_sig)
    l e₁ e₂ h₁ h₂ re₁ re₂ σ τ ε n :
  n ⊨ E〚Σ ⊢ σ // ef_cons l ε〛 η e₁ e₂ →
  n ⊨ H〚Σ; Σ l ⊢ τ // ε〛 η h₁ h₂ →
  n ⊨ E〚Σ; σ ⊢ τ // ε〛 η re₁ re₂ →
  n ⊨ E〚Σ ⊢ τ // ε〛 η (e_handle l e₁ h₁ re₁) (e_handle l e₂ h₂ re₂).
Proof.
intros He Hh Hre.
change (n ⊨ E〚Σ ⊢ τ // ε〛 η
  (rplug (r_handle l r_hole h₁ re₁) e₁) (rplug (r_handle l r_hole h₂ re₂) e₂)).
eapply rel_c_compat_e; [ | exact He ].
apply r_handle_compat_cl; assumption.
Qed.

Lemma handle_compat {EV V : Set} (Γ : V → typ Eff EV)
    l e₁ e₂ h₁ h₂ re₁ re₂ σ τ ε n :
  n ⊨ T〚Σ; Γ ⊨ e₁ ≾ e₂ ∷ σ // ef_cons l ε〛 →
  n ⊨ H〚Σ; Γ; Σ l ⊨ h₁ ≾ h₂ ∷ τ // ε〛 →
  n ⊨ T〚Σ; Γ ,+ σ ⊨ re₁ ≾ re₂ ∷ τ // ε〛 →
  n ⊨ T〚Σ; Γ ⊨ e_handle l e₁ h₁ re₁ ≾ e_handle l e₂ h₂ re₂ ∷ τ // ε〛.
Proof.
intros He Hh Hre.
iintro η; iintro γ₁; iintro γ₂; iintro Hγ.
simpl; eapply handle_compat_cl.
+ iespecialize He; iapply He; assumption.
+ eapply rel_h_close; eassumption.
+ eapply rel_e_close1; eassumption.
Qed.

End Compatibility.