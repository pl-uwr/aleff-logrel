(** * Relation H for handlers (not presented in the paper) *)
Require Import Utf8.
Require Import IxFree.Lib.
Require Import Lang.Syntax Lang.SyntaxProperties Lang.Typing.
Require Import Lang.Reduction.
Require Import BinaryRelation.Core.
Require Import List.

(** We define a relation for (open) handlers as a conjunction of logical
relations for each handler clause.
*)
Reserved Notation "'H〚' Σ ';' Γ ';' FS '⊨' h₁ '≾' h₂ '∷' τ '//' ε '〛'"
  (FS at level 97).

Fixpoint rel_h_open {Eff : Set} (Σ : eff_sig Eff) {V EV : Set}
    (Γ : V → typ Eff EV) (FS : list (op_sig Eff))
    (h₁ h₂ : list (expr Eff (inc (inc V))))
    (τ : typ Eff EV) ε : IProp :=
  match FS, h₁, h₂ with
  | nil,     nil,       nil       => (True)ᵢ
  | Θ :: FS, he₁ :: h₁, he₂ :: h₂ =>
    T〚Σ; Γ ,+ topen (op_input Θ) 
           ,+ t_arrow (topen (op_output Θ)) ε τ
           ⊨ he₁ ≾ he₂ ∷ τ // ε〛 ∧ᵢ
    H〚Σ; Γ; FS ⊨ h₁ ≾ h₂ ∷ τ // ε〛
  | _,       _,         _         => (False)ᵢ
  end
where "'H〚' Σ ';' Γ ';' FS '⊨' h₁ '≾' h₂ '∷' τ '//' ε '〛'" :=
  (rel_h_open Σ Γ FS h₁ h₂ τ ε).

(** Relation H is compatible with nil and cons *)
Section HandlerRelCompat.
Context {Eff : Set} (Σ : eff_sig Eff).

Lemma h_nil_compat {V EV : Set} (Γ : V → typ Eff EV) τ ε n :
  n ⊨ H〚Σ; Γ; nil ⊨ nil ≾ nil ∷ τ // ε〛.
Proof.
simpl; iintro; constructor.
Qed.

Lemma h_cons_compat {V EV : Set} (Γ : V → typ Eff EV)
    Θ FS he₁ he₂ h₁ h₂ τ ε n :
  n ⊨ T〚Σ; Γ ,+ topen (op_input Θ) 
             ,+ t_arrow (topen (op_output Θ)) ε τ
             ⊨ he₁ ≾ he₂ ∷ τ // ε〛 →
  n ⊨ H〚Σ; Γ; FS ⊨ h₁ ≾ h₂ ∷ τ // ε〛 →
  n ⊨ H〚Σ; Γ; Θ :: FS ⊨ he₁ :: h₁ ≾ he₂ :: h₂ ∷ τ // ε〛.
Proof.
intros Hhe Hh; simpl; isplit; assumption.
Qed.

End HandlerRelCompat.

(** Auxiliary relation for expressions with one and two potentially free
variables. *)
Definition rel_e_cl1 {Eff : Set} (Σ : eff_sig Eff) {EV : Set}
    σ τ ε (η : EV → IRel rel_row_sig)
    e₁ e₂ : IProp :=
  ∀ᵢ v₁ v₂, 〚Σ ⊢ σ〛 η v₁ v₂ ⇒ E〚Σ ⊢ τ // ε〛 η (esubst e₁ v₁) (esubst e₂ v₂).
Notation "'E〚' Σ ';' σ '⊢' τ '//' ε '〛' η" :=
  (rel_e_cl1 Σ σ τ ε η) (at level 0).

Definition rel_e_cl2 {Eff : Set} (Σ : eff_sig Eff) {EV : Set}
    σ₁ σ₂ τ ε (η : EV → IRel rel_row_sig)
    e₁ e₂ : IProp :=
  ∀ᵢ v₁ v₂ u₁ u₂, (〚Σ ⊢ σ₁〛 η v₁ v₂ ∧ᵢ 〚Σ ⊢ σ₂〛 η u₁ u₂) ⇒
    E〚Σ ⊢ τ // ε〛 η (ebisubst e₁ v₁ u₁) (ebisubst e₂ v₂ u₂).
Notation "'E〚' Σ ';' σ₁ ';' σ₂ '⊢' τ '//' ε '〛' η" :=
  (rel_e_cl2 Σ σ₁ σ₂ τ ε η) (at level 0).

(** Relation for closed handlers *)
Reserved Notation "'H〚' Σ ';' FS '⊢' τ '//' ε '〛' η" (at level 0).

Fixpoint rel_h {Eff : Set} (Σ : eff_sig Eff) {EV : Set}
   τ ε (η : EV → IRel rel_row_sig)
   (FS : list (op_sig Eff)) (h₁ h₂ : list (expr2 Eff)) : IProp :=
  match FS, h₁, h₂ with
  | nil,     nil,       nil       => (True)ᵢ
  | Θ :: FS, he₁ :: h₁, he₂ :: h₂ =>
    E〚Σ; topen (op_input Θ); t_arrow (topen (op_output Θ)) ε τ ⊢ τ // ε〛 η
      he₁ he₂ ∧ᵢ
    H〚Σ; FS ⊢ τ // ε〛 η h₁ h₂
  | _,       _,         _         => (False)ᵢ
  end
where "'H〚' Σ ';' FS '⊢' τ '//' ε '〛' η" := (rel_h Σ τ ε η FS).

Section HandlerRel.
Context {Eff : Set} (Σ : eff_sig Eff).

(** Related handlers for effect signature FS can handle operations from FS *)
Lemma rel_h_handle {EV : Set} τ ε (η : EV → IRel rel_row_sig)
    FS h₁ h₂ op Θ n :
  Ef[ FS ⊢ op ∷ Θ] →
  n ⊨ H〚Σ; FS ⊢ τ // ε〛 η h₁ h₂ →
  n ⊨ ∃ᵢ he₁ he₂,
    E〚Σ; topen (op_input Θ); t_arrow (topen (op_output Θ)) ε τ ⊢ τ // ε〛 η
      he₁ he₂ ∧ᵢ (handler he₁ h₁ op ∧ handler he₂ h₂ op)ᵢ.
Proof.
intro Hop; generalize h₁ h₂; clear h₁ h₂.
induction Hop; intros h₁ h₂ Hh.
+ destruct h₁ as [ | he₁ h₁ ]; destruct h₂ as [ | he₂ h₂ ]; simpl in Hh;
    try (apply I_Prop_elim in Hh; destruct Hh; fail).
  idestruct Hh as Hhe Hh.
  iexists he₁; iexists he₂; isplit.
  - exact Hhe.
  - iintro; split; constructor.
+ destruct h₁ as [ | hf₁ h₁ ]; destruct h₂ as [ | hf₂ h₂ ]; simpl in Hh;
    try (apply I_Prop_elim in Hh; destruct Hh; fail).
  idestruct Hh as Hhf Hh.
  specialize (IHHop h₁ h₂ Hh).
  idestruct IHHop as he₁ IH; idestruct IH as he₂ IH; idestruct IH as Hhe Hhdl.
  iexists he₁; iexists he₂; isplit.
  - exact Hhe.
  - apply I_Prop_elim in Hhdl; destruct Hhdl as [ Hhe₁ Hhe₂ ].
    iintro; split; constructor; assumption.
Qed.

(** We can get related expression with one (two) potentially free variables
by substituting related values for all except one (two) variables
in related open terms *)
Lemma rel_e_close1 {V EV: Set} (η : EV → IRel rel_row_sig)
    (Γ : V → typ Eff EV)
    e₁ e₂ σ τ ε γ₁ γ₂ n :
  n ⊨ G〚Σ ⊢ Γ〛 η γ₁ γ₂ →
  n ⊨ T〚Σ; Γ ,+ σ ⊨ e₁ ≾ e₂ ∷ τ // ε〛 →
  n ⊨ E〚Σ; σ ⊢ τ // ε〛 η (ebind (lift γ₁) e₁) (ebind (lift γ₂) e₂).
Proof.
intros Hγ He; unfold rel_e_cl1.
iintro v₁; iintro v₂; iintro Hv.
rewrite esubst_bind_lift, esubst_bind_lift.
iespecialize He; iapply He.
iintro x; destruct x as [ | x ]; simpl.
+ assumption.
+ iespecialize Hγ; exact Hγ.
Qed.

Lemma rel_e_close2 {V EV: Set} (η : EV → IRel rel_row_sig)
    (Γ : V → typ Eff EV)
    e₁ e₂ σ₁ σ₂ τ ε γ₁ γ₂ n :
  n ⊨ G〚Σ ⊢ Γ〛 η γ₁ γ₂ →
  n ⊨ T〚Σ; Γ ,+ σ₁ ,+ σ₂ ⊨ e₁ ≾ e₂ ∷ τ // ε〛 →
  n ⊨ E〚Σ; σ₁; σ₂ ⊢ τ // ε〛 η
    (ebind (lift (lift γ₁)) e₁) (ebind (lift (lift γ₂)) e₂).
Proof.
intros Hγ He; unfold rel_e_cl2.
iintro v₁; iintro v₂; iintro u₁; iintro u₂; iintro Hvu.
idestruct Hvu as Hv Hu.
rewrite ebisubst_bind_lift_lift, ebisubst_bind_lift_lift.
iespecialize He; iapply He.
iintro x; destruct x as [ | [ | x ] ]; simpl.
+ assumption.
+ assumption.
+ iespecialize Hγ; exact Hγ.
Qed.

(** When related handlers are closed with related substitutions, we get related
closed handles *)
Lemma rel_h_close {V EV: Set} (η : EV → IRel rel_row_sig)
    (Γ : V → typ Eff EV) (FS : list (op_sig Eff))
    (h₁ h₂ : list (expr Eff (inc (inc V)))) τ ε γ₁ γ₂ n :
  n ⊨ G〚Σ ⊢ Γ〛 η γ₁ γ₂ →
  n ⊨ H〚Σ; Γ; FS ⊨ h₁ ≾ h₂ ∷ τ // ε〛 →
  n ⊨ H〚Σ; FS ⊢ τ // ε〛 η (List.map (ebind (lift (lift γ₁))) h₁)
    (List.map (ebind (lift (lift γ₂))) h₂).
Proof.
intro Hγ.
generalize h₁ h₂; clear h₁ h₂.
induction FS; intros h₁ h₂ Hh.
+ destruct h₁ as [ | he₁ h₁ ]; destruct h₂ as [ | he₂ h₂ ]; simpl in Hh;
    try (apply I_Prop_elim in Hh; destruct Hh; fail).
  simpl; iintro; constructor.
+ destruct h₁ as [ | he₁ h₁ ]; destruct h₂ as [ | he₂ h₂ ]; simpl in Hh;
    try (apply I_Prop_elim in Hh; destruct Hh; fail).
  idestruct Hh as Hhe Hh.
  simpl; isplit.
  - eapply rel_e_close2; [ exact Hγ | ].
    exact Hhe.
  - apply IHFS; assumption.
Qed.

End HandlerRel.