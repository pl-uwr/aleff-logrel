(** In this file, we build up to and proof soundness of the logical
relation. *)
(** At this point, the proof follows the standard route: we show the
fundamental property of the logical relations, we show that the
logical relation is an adequate precongruence, which is easy due to
the structure of the relation. From these two properties soundness
follows in a simple manner.*)
Require Import Utf8.
Require Import IxFree.Lib.
Require Import Lang.Syntax Lang.SyntaxProperties Lang.Typing.
Require Import Lang.Reduction Lang.ReductionProperties.
Require Import Lang.CtxApprox.
Require Import BinaryRelation.Core BinaryRelation.Compatibility.
Require Import BinaryRelation.DefUnroll BinaryRelation.Properties.
Require Import BinaryRelation.HandlerRel BinaryRelation.Subsumption.

Section Soundness.
Context {Eff : Set} {EffDec : DecEffectEq Eff}.
Context {Σ : eff_sig Eff}.

Fixpoint fundamental_property {V EV : Set} (Γ : V → typ Eff EV) e τ ε n
  (Htyping : T[ Σ ; Γ ⊢ e ∷ τ // ε ]) :
  n ⊨ T〚Σ; Γ ⊨ e ≾ e ∷ τ // ε〛
with fundamental_property_h {V EV : Set} (Γ : V → typ Eff EV) FS h τ ε n
  (Hhtyping : H[ Σ ; Γ ; FS ⊢ h ∷ τ // ε ]) :
  n ⊨ H〚Σ; Γ; FS ⊨ h ≾ h ∷ τ // ε〛.
Proof.
+ destruct Htyping.
  - apply var_compat.
  - apply unit_compat.
  - apply lam_compat, fundamental_property; assumption.
  - apply elam_compat, fundamental_property; assumption.
  - apply op_compat; assumption.
  - eapply app_compat.
    * apply fundamental_property; eassumption.
    * apply fundamental_property; assumption.
  - apply eapp_compat, fundamental_property; assumption.
  - apply lift_compat, fundamental_property; assumption.
  - eapply handle_compat.
    * apply fundamental_property; eassumption.
    * apply fundamental_property_h; assumption.
    * apply fundamental_property; assumption.
  - eapply sub_compat.
    * eassumption.
    * eassumption.
    * apply fundamental_property; assumption.
+ destruct Hhtyping.
  - apply h_nil_compat.
  - apply h_cons_compat.
    * apply fundamental_property; assumption.
    * apply fundamental_property_h; assumption.
Qed.

Lemma fundamental_property_cl {EV : Set} n η e (τ : typ Eff EV) ε :
  T[ Σ ; of_empty ⊢ e ∷ τ // ε ] →
  n ⊨ E〚Σ ⊢ τ // ε〛 η e e.
Proof.
intro Htp.
assert (H := fundamental_property _ _ _ _ n Htp).
unfold rel_e_open in H.
ispecialize H η.
ispecialize H (@v_var Eff Empty_set); ispecialize H (@v_var Eff Empty_set).
repeat rewrite emonad_bind_return' in H.
iapply H.
iintro x; destruct x.
Qed.

Fixpoint precongruence {V EV : Set} (Γ : V → typ Eff EV) e₁ e₂ τ ε C n
  (Hctx_typing : C[ Σ ; Γ ⊢ C ∷ τ // ε ]) :
  n ⊨ T〚Σ; Γ ⊨ e₁ ≾ e₂ ∷ τ // ε〛 →
  n ⊨ E〚Σ ⊢ t_unit // ef_nil〛 (λ x : Empty_set, match x with end)
    (cplug C e₁) (cplug C e₂)
with precongruence_h {V EV : Set} (Γ : V → typ Eff EV) FS h₁ h₂ τ ε C n
  (Hhctx_typing : CH[ Σ ; Γ ; FS ⊢ C ∷ τ // ε ]) :
  n ⊨ H〚Σ; Γ; FS ⊨ h₁ ≾ h₂ ∷ τ // ε〛 →
  n ⊨ E〚Σ ⊢ t_unit // ef_nil〛 (λ x : Empty_set, match x with end)
    (hplug C h₁) (hplug C h₂).
Proof.
+ destruct Hctx_typing; simpl.
  - intro He; unfold rel_e_open in He.
    ispecialize He (λ x : Empty_set,
      match x with end : IRel (@rel_row_sig Eff)).
    ispecialize He (of_empty : Empty_set → value0 Eff).
    ispecialize He (of_empty : Empty_set → value0 Eff).
    rewrite emonad_bind_return in He; [ | intros [] ].
    rewrite emonad_bind_return in He; [ | intros [] ].
    iapply He.
    iintro x; destruct x.
  - intro He; eapply precongruence; [ eassumption | ].
    apply lam_compat; assumption.
  - intro He; eapply precongruence; [ eassumption | ].
    apply elam_compat; assumption.
  - intro He; eapply precongruence; [ eassumption | ].
    eapply app_compat; [ eassumption | ].
    apply fundamental_property; assumption.
  - intro He; eapply precongruence; [ eassumption | ].
    eapply app_compat; [ | eassumption ].
    apply fundamental_property; assumption.
  - intro He; eapply precongruence; [ eassumption | ].
    eapply eapp_compat; assumption.
  - intro He; eapply precongruence; [ eassumption | ].
    apply lift_compat; assumption.
  - intro He; eapply precongruence; [ eassumption | ].
    eapply handle_compat.
    * eassumption.
    * apply fundamental_property_h; assumption.
    * apply fundamental_property; assumption.
  - intro He; eapply precongruence; [ eassumption | ].
    eapply handle_compat.
    * apply fundamental_property; eassumption.
    * apply fundamental_property_h; assumption.
    * exact He.
  - intro He; eapply precongruence_h; [ eassumption | ].
    apply h_cons_compat.
    * assumption.
    * apply fundamental_property_h; assumption.
  - intro He; eapply precongruence; [ eassumption | ].
    eapply sub_compat; eassumption.
+ destruct Hhctx_typing; simpl.
  - intro Hh; eapply precongruence_h; [ eassumption | ].
    apply h_cons_compat.
    * apply fundamental_property; assumption.
    * assumption.
  - intro Hh; eapply precongruence; [ eassumption | ].
    eapply handle_compat.
    * apply fundamental_property; eassumption.
    * assumption.
    * apply fundamental_property; assumption.
Qed.

Lemma Obs_adequacy n (e₁ e₂ : expr0 Eff) :
  n ⊨ ProgramObs.Obs e₁ e₂ → red_n n e₁ v_unit → red_rtc e₂ v_unit.
Proof.
generalize e₁; clear e₁; induction n; intros e₁ HObs Hred_n.
+ inversion Hred_n; subst.
  apply ProgramObs.Obs_unroll in HObs; unfold ProgramObs.F_Obs in HObs.
  idestruct HObs as HObs HObs.
  - apply I_Prop_elim in HObs; apply HObs.
  - idestruct HObs as e₁' HObs.
    idestruct HObs as Hred_v HObs.
    apply I_Prop_elim in Hred_v; inversion Hred_v.
+ inversion Hred_n as [ | ? ? e₁' ? Hred' ]; subst.
  apply ProgramObs.Obs_unroll in HObs; unfold ProgramObs.F_Obs in HObs.
  idestruct HObs as HObs HObs.
  - apply I_Prop_elim in HObs; destruct HObs; subst; inversion Hred'.
  - idestruct HObs as e₁'' HObs; idestruct HObs as Hred'' HObs.
    apply I_valid_elim in HObs; simpl in HObs.
    apply I_Prop_elim in Hred''.
    eapply IHn; [ eassumption | ].
    erewrite (red_determ _ e₁''); eassumption.
Qed.

Lemma adequacy_idx (e₁ e₂ : expr0 Eff) n :
  n ⊨ E〚Σ ⊢ t_unit // ef_nil〛
    (λ x : Empty_set, match x with end : IRel (@rel_row_sig Eff)) e₁ e₂ →
  n ⊨ ProgramObs.Obs e₁ e₂.
Proof.
intro Hlog.
change (n ⊨ ProgramObs.Obs (plug x_hole e₁) (plug x_hole e₂)).
eapply rel_k_expr; [ | exact Hlog ].
apply rel_k_roll; simpl.
+ intros v₁ v₂; iintro Hv; apply I_Prop_elim in Hv; destruct Hv; simpl.
  apply ProgramObs.Obs_roll; ileft.
  iintro; split; constructor.
+ intros E₁ E₂ f₁ f₂; iintro Hf.
  unfold rel_simpl_cl in Hf.
  idestruct Hf as ρ₁ Hf; idestruct Hf as ρ₂ Hf.
  idestruct Hf as ν Hf; idestruct Hf as F Hf.
  apply I_Prop_elim in F; destruct F.
Qed.

Theorem adequacy (e₁ e₂ : expr0 Eff) :
  (⊨ E〚Σ ⊢ t_unit // ef_nil〛
    (λ x : Empty_set, match x with end : IRel (@rel_row_sig Eff)) e₁ e₂) →
  base_obs e₁ e₂.
Proof.
intros Hlog Hev₁.
apply red_rtc_to_red_n in Hev₁; destruct Hev₁ as [ n Hev₁ ].
eapply Obs_adequacy; [ | eassumption ]; clear Hev₁.
specialize (Hlog n).
apply adequacy_idx in Hlog; assumption.
Qed.

Theorem soundness {V EV : Set} (Γ : V → typ Eff EV) e₁ e₂ τ ε :
  (⊨ T〚Σ; Γ ⊨ e₁ ≾ e₂ ∷ τ // ε〛) → CTX[Σ; Γ ⊨ e₁ ≾ e₂ ∷ τ // ε].
Proof.
intros Hlog C HC.
apply adequacy.
intro n; eapply precongruence; [ eassumption | ].
apply Hlog.
Qed.

End Soundness.