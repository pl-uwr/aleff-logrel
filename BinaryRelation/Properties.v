(** This module provides some simple properties of the logical
relation, including the propositions of Lemma 1 of the paper. *)
Require Import Utf8.
Require Import IxFree.Lib.
Require Import Lang.Syntax Lang.Typing.
Require Import Lang.Reduction Lang.ReductionProperties.
Require Import BinaryRelation.Core BinaryRelation.ProgramObs.

Section Properties.
Context {Eff : Set} {EffDec : DecEffectEq Eff}.
Context (Σ : eff_sig Eff).

Lemma rel_k_value {EV : Set} (F₁ F₂ : ectx0 Eff) (η : EV → IRel rel_row_sig)
    τ ε {v₁ v₂ : value0 Eff} {n} :
  n ⊨ K〚Σ ⊢ τ // ε〛 η F₁ F₂ →
  n ⊨ 〚Σ ⊢ τ〛 η v₁ v₂ →
  n ⊨ Obs (plug F₁ v₁) (plug F₂ v₂).
Proof.
intros HF Hv; unfold rel_ectx_cl in HF.
idestruct HF as HFv HFs; iespecialize HFv; iapply HFv.
assumption.
Qed.

Lemma rel_k_simpl {EV : Set} (F₁ F₂ : ectx0 Eff) (η : EV → IRel rel_row_sig)
    τ ε (E₁ E₂ : rctx0 Eff) {e₁ e₂ : expr0 Eff} {n} :
  n ⊨ K〚Σ ⊢ τ // ε〛 η F₁ F₂ →
  n ⊨ S〚Σ ⊢ τ // ε〛 η E₁ E₂ e₁ e₂ →
  n ⊨ Obs (plug F₁ (rplug E₁ e₁)) (plug F₂ (rplug E₂ e₂)).
Proof.
intros HF Hs; unfold rel_ectx_cl in HF.
idestruct HF as HFv HFs; iespecialize HFs; iapply HFs.
assumption.
Qed.

Lemma rel_k_expr {EV : Set} (F₁ F₂ : ectx0 Eff) (η : EV → IRel rel_row_sig)
    τ ε {e₁ e₂ : expr0 Eff} {n} :
  n ⊨ K〚Σ ⊢ τ // ε〛 η F₁ F₂ →
  n ⊨ E〚Σ ⊢ τ // ε〛 η e₁ e₂ →
  n ⊨ Obs (plug F₁ e₁) (plug F₂ e₂).
Proof.
intros HF He; unfold rel_ectx_cl in He.
iespecialize He; iapply He; assumption.
Qed.

(* Lemma 1.(1) *)
Lemma rel_v_in_e {EV : Set} (η : EV → IRel rel_row_sig) τ ε
    {v₁ v₂ : value0 Eff} {n} :
  n ⊨ 〚Σ ⊢ τ〛 η v₁ v₂ →
  n ⊨ E〚Σ ⊢ τ // ε〛 η v₁ v₂.
Proof.
intro Hv.
iintro F₁; iintro F₂; iintro HF.
eapply rel_k_value.
+ exact HF.
+ assumption.
Qed.

Lemma rel_v_open_in_e {V EV : Set} (Γ : V → typ Eff EV)
    {v₁ v₂ : value Eff V} τ ε {n} :
  n ⊨ T〚Σ; Γ ⊨ v₁ ≾ v₂ ∷ τ〛 →
  n ⊨ T〚Σ; Γ ⊨ v₁ ≾ v₂ ∷ τ // ε〛.
Proof.
intro Hv.
iintro η; iintro γ₁; iintro γ₂; iintro Hγ; simpl.
apply rel_v_in_e.
iespecialize Hv; iapply Hv; assumption.
Qed.

(** Lemma 1.(2) *)
Lemma rel_s_in_e {EV : Set} (η : EV → IRel rel_row_sig) τ ε
    (E₁ E₂ : rctx0 Eff) {e₁ e₂ : expr0 Eff} {n} :
  n ⊨ S〚Σ ⊢ τ // ε〛 η E₁ E₂ e₁ e₂ →
  n ⊨ E〚Σ ⊢ τ // ε〛 η (rplug E₁ e₁) (rplug E₂ e₂).
Proof.
intro Hs.
iintro F₁; iintro F₂; iintro HF.
eapply rel_k_simpl.
+ exact HF.
+ assumption.
Qed.

(** Lemma 1.(3) *)
Lemma rel_e_red_l {EV : Set} (η : EV → IRel rel_row_sig) τ ε
    (e₁ e₁' e₂ : expr0 Eff) {n} :
  red e₁ e₁' →
  n ⊨ ▷ E〚Σ ⊢ τ // ε〛 η e₁' e₂ →
  n ⊨ E〚Σ ⊢ τ // ε〛 η e₁ e₂.
Proof.
intros Hred He'.
iintro F₁; iintro F₂; iintro HF.
eapply Obs_red_l.
  { apply red_in_ectx; eassumption. }
later_shift.
eapply rel_k_expr; eassumption.
Qed.

(** Lemma 1.(4) *)
Lemma rel_e_red_r {EV : Set} (η : EV → IRel rel_row_sig) τ ε
    (e₁ e₂ e₂' : expr0 Eff) {n} :
  red e₂ e₂' →
  n ⊨ E〚Σ ⊢ τ // ε〛 η e₁ e₂' →
  n ⊨ E〚Σ ⊢ τ // ε〛 η e₁ e₂.
Proof.
intros Hred He'.
iintro F₁; iintro F₂; iintro HF.
eapply Obs_red_r.
  { apply red_in_ectx; eassumption. }
eapply rel_k_expr; eassumption.
Qed.

Lemma rel_e_coerce_pure {EV : Set} (η : EV → IRel rel_row_sig) τ ε
    e₁ e₂ n :
  n ⊨ E〚Σ ⊢ τ // ef_nil〛 η e₁ e₂ →
  n ⊨ E〚Σ ⊢ τ // ε〛 η e₁ e₂.
Proof.
intro He.
iintro F₁; iintro F₂; iintro HF.
eapply rel_k_expr; [ | eassumption ].
isplit.
+ iintro v₁; iintro v₂; iintro Hv.
  eapply rel_k_value; eassumption.
+ iintro E₁; iintro E₂; iintro e₁'; iintro e₂'; iintro Hs.
  idestruct Hs as ρ₁ Hs; idestruct Hs as ρ₂ Hs; idestruct Hs as ν Hs.
  idestruct Hs as Hs Hs'; simpl in Hs.
  apply I_Prop_elim in Hs; destruct Hs.
Qed.

End Properties.