(** * Relation C for partial evaluation contexts *)
(** This module formalizes the definition and characterization of relation C
for partial evaluation contexts (middle of Section 3.3).
The characterization presented here is more general then the one presented
in the paper: Lemma 3 and Lemma 4 are special cases of the lemma rel_c_intro.
*)
Require Import Utf8.
Require Import IxFree.Lib.
Require Import Lang.Syntax Lang.SyntaxProperties Lang.Typing.
Require Import BinaryRelation.Core BinaryRelation.DefUnroll.
Require Import BinaryRelation.Properties BinaryRelation.EffectSubst.

(** The relation for partial evaluation contexts *)
Definition rel_c {Eff : Set} (Σ : eff_sig Eff) {EV : Set}
    τ₁ ε₁ τ₂ ε₂ (η : EV → IRel rel_row_sig)
    (E₁ E₂ : rctx0 Eff) : IProp :=
  (∀ᵢ v₁ v₂, 〚Σ ⊢ τ₁〛 η v₁ v₂ ⇒
      E〚Σ ⊢ τ₂ // ε₂〛 η (rplug E₁ v₁) (rplug E₂ v₂)) ∧ᵢ
  (∀ᵢ E₁' E₂' e₁ e₂, S〚Σ ⊢ τ₁ // ε₁〛 η E₁' E₂' e₁ e₂ ⇒
      E〚Σ ⊢ τ₂ // ε₂〛 η (rplug E₁ (rplug E₁' e₁)) (rplug E₂ (rplug E₂' e₂))).
Notation "'C〚' Σ '⊢' τ₁ '//' ε₁ '↝' τ₂ '//' ε₂ '〛' η" :=
  (rel_c Σ τ₁ ε₁ τ₂ ε₂ η) (at level 0).

Section ContextRel.
Context {Eff : Set} {EffDec : DecEffectEq Eff}.
Context (Σ : eff_sig Eff).

(** Relation rel_c is compatible with relation for expressions (Lemma 2) *)
Lemma rel_c_compat_e {EV : Set}
    τ₁ ε₁ τ₂ ε₂ (η : EV → IRel rel_row_sig)
    E₁ E₂ e₁ e₂ n :
  n ⊨ C〚Σ ⊢ τ₁ // ε₁ ↝ τ₂ // ε₂〛 η E₁ E₂ →
  n ⊨ E〚Σ ⊢ τ₁ // ε₁〛 η e₁ e₂ →
  n ⊨ E〚Σ ⊢ τ₂ // ε₂〛 η (rplug E₁ e₁) (rplug E₂ e₂).
Proof.
intros HE He.
iintro F₁; iintro F₂; iintro HF.
rewrite <- plug_xcompr; rewrite <- plug_xcompr.
eapply rel_k_expr; [ | exact He ].
clear e₁ e₂ He.
apply rel_k_roll.
+ intros v₁ v₂; iintro Hv.
  rewrite plug_xcompr; rewrite plug_xcompr.
  eapply rel_k_expr; [ exact HF | ].
  idestruct HE as HEv HEs; iespecialize HEv; iapply HEv.
  exact Hv.
+ intros E₁' E₂' e₁ e₂; iintro Hs.
  rewrite plug_xcompr; rewrite plug_xcompr.
  eapply rel_k_expr; [ exact HF | ].
  idestruct HE as HEv HEs; iespecialize HEs; iapply HEs.
  exact Hs.
Qed.

(* ========================================================================= *)
(** ** Characterization of rel_c *)

(** *** Paths between effects *)

(** First we define a path between two effects ε₁ and ε₂ as a sequence
of operation that can be performed in order to obtain effect ε₂ from ε₁.
We have the following operations: *)
Inductive EffectMod {EV : Set} : effect Eff EV → effect Eff EV → Set :=
(** End of the sequence (effects are equal) *)
| ModNone   : ∀ ε, EffectMod ε ε
(** Shifting operation l in effect ε₁ *)
| ModShift  : ∀ l ε₁ ε₂, EffectMod ε₁ ε₂ → EffectMod ε₁ (ef_cons l ε₂)
(** Handlong operation l in effect 〈 l | ε₁ 〉 *)
| ModHandle : ∀ l ε₁ ε₂, EffectMod ε₁ ε₂ → EffectMod (ef_cons l ε₁) ε₂
.

Arguments ModShift  [EV] l [ε₁] [ε₂].
Arguments ModHandle [EV] l [ε₁] [ε₂].

(** Note that we do not include subsumption rules in the path. This doesn't
cause the loss of generality: subsumption rules can be applied just before
and just after the lemmas presented here, to obtain two effects that there
exists path between them (see Subsumption module).
*)

(** If path M transforms effect ε₁ to ε₂, then we can compute conditions
on contexts E₁ and E₂ sufficient to have them in relation
C〚 τ₁/ε₁ ↝ τ₂/ε₂ 〛_η by the function rel_cM (defined recurisvely on path M).
This function has some additional parameters:
- ε₁' ε₂' are initial values of ε₁ and ε₂ respectively (values of ε₁ and
  ε₂ may change during recursive calls, see the definition of EffectMod).
- l is an effect for which we compute conditions (to obtain full list of
  conditions we have to quantify over all effects)
- m₁ describes how many times l was already handled in the path (initally 0)
- m₂ describes how many times l was already shifted in the path (initally 0)

Informally generated conditions for effect l and path M can be described
as follows:
- context E₁ and E₂ should be (m₁,m₂)-free for l (rfree_g m₁ l m₂ E_i)
  where m₁ and m₂ describes how many times l was handled and shifted
  (respecively) in M.
- if l was handled m times, then for each i < m, (op_l : σ → τ) ∈ Σ,
  (v₁,v₂) ∈ ▷〚σ〛_η and context E₁' and E₂' if we have the following conditions
  * i-free E₁'
  * i-free E₂'
  * foreach (u₁,u₂) ∈ ▷〚τ〛_η we have (E₁'[u₁],E₂'[u₂]) ∈ ▷E〚τ₁/ε₁〛_η
  then (E₁[E₁'[op_l v₁]],E₂[E₂'[op_l v₂]]) ∈ E〚τ₂/ε₂〛_η
*)

Fixpoint rel_cM {EV : Set} l m₁ m₂ {ε₁ ε₂ : effect Eff EV}
    (M : EffectMod ε₁ ε₂)
    (τ₁ : typ Eff EV) ε₁' τ₂ ε₂' (η : EV → IRel (@rel_row_sig Eff))
    (E₁ E₂ : rctx0 Eff) : IProp :=
  match M in EffectMod ε₁ ε₂ with
  | ModNone _     => (rfree_g m₁ l m₂ E₁ ∧ rfree_g m₁ l m₂ E₂)ᵢ
  | ModShift l' M =>
    rel_cM l m₁ (if dec_effect_eq l' l then S m₂ else m₂)
      M τ₁ ε₁' τ₂ ε₂' η E₁ E₂
  | ModHandle l' M =>
    rel_cM l (if dec_effect_eq l' l then S m₁ else m₁) m₂
      M τ₁ ε₁' τ₂ ε₂' η E₁ E₂ ∧ᵢ
    if dec_effect_eq l l' then
      ∀ᵢ op Θ v₁ v₂ E₁' E₂',
        ((Ef[ Σ l ⊢ op ∷ Θ ] ∧ rfree l m₁ E₁' ∧ rfree l m₁ E₂')ᵢ ∧ᵢ
        ▷ 〚Σ ⊢ topen (op_input Θ)〛 η v₁ v₂ ∧ᵢ
        ▷ ∀ᵢ u₁ u₂, 〚Σ ⊢ topen (op_output Θ)〛 η u₁ u₂ ⇒
           E〚Σ ⊢ τ₁ // ε₁'〛 η (rplug E₁' u₁) (rplug E₂' u₂)) ⇒
       E〚Σ ⊢ τ₂ // ε₂'〛 η
         (rplug E₁ (rplug E₁' (e_app (v_op l op) v₁)))
         (rplug E₂ (rplug E₂' (e_app (v_op l op) v₂)))
    else (True)ᵢ
  end.

(** Some auxiliary properites of finite maps *)
Definition add_func (ξ : Eff → nat) (ρ₁ ρ₂ : Eff → option nat) : Prop :=
  ∀ l, match ρ₁ l with
       | None   => ρ₂ l = None
       | Some n => ρ₂ l = Some (ξ l + n)
       end.

Definition shift_lmap (l : Eff) (ρ : Eff → option nat) : Eff → option nat :=
  λ l', match ρ l' with
        | None   => None
        | Some n => Some (if dec_effect_eq l l' then S n else n)
        end.

Lemma lift_lmap_shift l ρ : lift_lmap l ρ (shift_lmap l ρ).
Proof.
constructor; unfold shift_lmap.
+ destruct (dec_effect_eq l l); [ reflexivity | ].
  exfalso; auto.
+ intros l' Hneq; destruct (ρ l'); [ | reflexivity ].
  destruct (dec_effect_eq l l'); [ | reflexivity ].
  exfalso; auto.
Qed.

(**
The proof, that conditions described by rel_cM are sufficient to characterize
relation for partial evaluation contexts is very technical, and done by
nested induction (first using Loeb induction on step-indices, and then
by induction on paths between effects. The inner part of this induction
is done in the following lemma.
*)
Lemma rel_cM_lemma {EV : Set} {ε₁ ε₂ : effect Eff EV} (M : EffectMod ε₁ ε₂)
  τ₁ ε₁' τ₂ ε₂' ξ₁ ξ₂ (η : EV → IRel (@rel_row_sig Eff))
  E₁ E₂ E₁' E₂' e₁ e₂ ρ₁ ρ₂ ν n :
  (n ⊨ ▷ ∀ᵢ e₁ e₂, E〚Σ ⊢ τ₁ // ε₁'〛 η e₁ e₂ ⇒
    E〚Σ ⊢ τ₂ // ε₂'〛 η (rplug E₁ e₁) (rplug E₂ e₂)) →
  n ⊨ R〚Σ ⊢ ε₁〛 η e₁ e₂ ρ₁ ρ₂ ν →
  (n ⊨ ∀ᵢ l, rel_cM l (ξ₁ l) (ξ₂ l) M τ₁ ε₁' τ₂ ε₂' η E₁ E₂) →
  (∀ l, match ρ₁ l with
        | None   => True
        | Some m => rfree l (ξ₁ l + m) E₁'
        end) →
  (∀ l, match ρ₂ l with
        | None   => True
        | Some m => rfree l (ξ₁ l + m) E₂'
        end) →
  (n ⊨ ∀ᵢ e₁ e₂ ρ₁ ρ₂ ν, R〚Σ ⊢ ε₂〛 η e₁ e₂ ρ₁ ρ₂ ν ⇒
    ∃ᵢ ρ₁' ρ₂', R〚Σ ⊢ ε₂'〛 η e₁ e₂ ρ₁' ρ₂' ν ∧ᵢ
    (add_func ξ₂ ρ₁ ρ₁' ∧ add_func ξ₂ ρ₂ ρ₂')ᵢ) →
  (n ⊨ ∀ᵢ e₁' e₂', ν e₁' e₂' ⇒
      ▷ E〚Σ ⊢ τ₁ // ε₁'〛 η (rplug E₁' e₁') (rplug E₂' e₂')) →
  n ⊨ E〚Σ ⊢ τ₂ // ε₂'〛 η (rplug E₁ (rplug E₁' e₁)) (rplug E₂ (rplug E₂' e₂)).
Proof.
intro Loeb_IH.
generalize ρ₁ ρ₂ ξ₁ ξ₂; clear ρ₁ ρ₂ ξ₁ ξ₂.
induction M as [ ε | l ε₁ ε₂ M IHM | l ε₁ ε₂ M IHM ];
  intros ρ₁ ρ₂ ξ₁ ξ₂ Hs HM Hρ₁ Hρ₂ Hξ₂ HE'; simpl in HM;
  [ | | simpl in Hs; idestruct Hs as Hs Hs ].
+ rewrite <- rplug_rcomp; rewrite <- rplug_rcomp.
  apply rel_s_in_e.
  iespecialize Hξ₂; assert (Hξ₂' := I_arrow_elim Hξ₂ Hs); clear Hξ₂.
  idestruct Hξ₂' as ρ₁' Hξ₂; idestruct Hξ₂ as ρ₂' Hξ₂.
  idestruct Hξ₂ as Hρ' Hξ₂.
  apply I_Prop_elim in Hξ₂; destruct Hξ₂ as [ Hρ₁' Hρ₂' ].
  eapply rel_s_roll.
  - exact Hρ'.
  - intro l; specialize (Hρ₁ l); specialize (Hρ₁' l).
    ispecialize HM l; apply I_Prop_elim in HM; destruct HM as [ HM _ ].
    destruct (ρ₁ l); rewrite Hρ₁'; [ | constructor ].
    eapply rfree_g_rcomp; [ | eassumption ].
    apply rfree_g_shift; assumption.
  - intro l; specialize (Hρ₂ l); specialize (Hρ₂' l).
    ispecialize HM l; apply I_Prop_elim in HM; destruct HM as [ _ HM ].
    destruct (ρ₂ l); rewrite Hρ₂'; [ | constructor ].
    eapply rfree_g_rcomp; [ | eassumption ].
    apply rfree_g_shift; assumption.
  - intros e₁' e₂'; iintro He'.
    rewrite rplug_rcomp; rewrite rplug_rcomp.
    iespecialize HE'.
    assert (HE'' := I_arrow_elim HE' He'); later_shift.
    iespecialize Loeb_IH; iapply Loeb_IH.
    exact HE''.
+ eapply IHM; clear IHM.
  - exact Hs.
  - exact HM.
  - exact Hρ₁.
  - exact Hρ₂.
  - clear e₁ e₂ ρ₁ ρ₂ ν Hs Hρ₁ Hρ₂ HE'.
    iintro e₁; iintro e₂; iintro ρ₁; iintro ρ₂; iintro ν; iintro Hs.
    assert (Hs' : n ⊨ R〚Σ ⊢ ef_cons l ε₂〛 η e₁ e₂
      (shift_lmap l ρ₁) (shift_lmap l ρ₂) ν).
    { simpl; iright.
      iexists ρ₁; iexists ρ₂; isplit; [ | exact Hs ].
      iintro; split.
      + apply lift_lmap_shift.
      + apply lift_lmap_shift.
    }
    iespecialize Hξ₂; assert (Hξ₂' := I_arrow_elim Hξ₂ Hs'); clear Hξ₂.
    idestruct Hξ₂' as ρ₁' Hξ₂; idestruct Hξ₂ as ρ₂' Hξ₂.
    idestruct Hξ₂ as Hρ' Hξ₂.
    iexists ρ₁'; iexists ρ₂'; isplit; [ exact Hρ' | ].
    iintro; apply I_Prop_elim in Hξ₂.
    destruct Hξ₂ as [ Hξ1 Hξ2 ]; split.
    * intro l'; specialize (Hξ1 l'); unfold shift_lmap in Hξ1.
      destruct (ρ₁ l'); [ | assumption ].
      destruct (dec_effect_eq l l'); [ | assumption ].
      rewrite <- plus_n_Sm in Hξ1; assumption.
    * intro l'; specialize (Hξ2 l'); unfold shift_lmap in Hξ2.
      destruct (ρ₂ l'); [ | assumption ].
      destruct (dec_effect_eq l l'); [ | assumption ].
      rewrite <- plus_n_Sm in Hξ2; assumption.
  - exact HE'.
+ clear IHM.
  ispecialize HM l; idestruct HM as HM1 HM; clear HM1.
  destruct (dec_effect_eq l l) as [ _ | Hneq ]; [ | exfalso; auto ].
  unfold Frel_eff in Hs.
  idestruct Hs as op Hs; idestruct Hs as Θ Hs.
  idestruct Hs as v₁ Hs; idestruct Hs as v₂ Hs.
  idestruct Hs as Hs1 Hs; idestruct Hs as Hv Hν.
  apply I_Prop_elim in Hs1; destruct Hs1 as [ HΘ Hρ₁l Hρ₂l ].
  iespecialize HM; iapply HM.
  isplit; [ iintro; split; [ | split ] | isplit ].
  - eassumption.
  - specialize (Hρ₁ l); rewrite Hρ₁l in Hρ₁.
    rewrite <- plus_n_O in Hρ₁; assumption.
  - specialize (Hρ₂ l); rewrite Hρ₂l in Hρ₂.
    rewrite <- plus_n_O in Hρ₂; assumption.
  - later_shift.
    apply rel0_v_fix_unroll in Hv.
    eapply rel_v_weaken_l; [ | exact Hv ].
    iintro μ; destruct μ.
  - later_down; iintro u₁.
    later_down; iintro u₂.
    later_down; iintro Hu.
    iespecialize HE'; iapply HE'.
    iespecialize Hν; idestruct Hν as Hν₁ Hν₂.
    iapply Hν₂; later_shift.
    apply rel0_v_fix_roll; eapply rel_v_weaken_r; [ | exact Hu ].
    iintro μ; destruct μ.
+ idestruct Hs as ρ₁' Hs; idestruct Hs as ρ₂' Hs.
  idestruct Hs as Hlift Hs; apply I_Prop_elim in Hlift.
  destruct Hlift as [ Hlift₁ Hlift₂ ].
  eapply IHM; clear IHM.
  - exact Hs.
  - iintro l'; ispecialize HM l'.
    idestruct HM as HM HM'; exact HM.
  - destruct Hlift₁ as [ Hρ₁l Hρ₁l' ]; intro l'.
    destruct (dec_effect_eq l l').
    * subst; destruct (ρ₁' l'); [ | constructor ].
      specialize (Hρ₁ l'); rewrite Hρ₁l in Hρ₁; simpl in Hρ₁.
      rewrite <- plus_n_Sm in Hρ₁; assumption.
    * rewrite <- Hρ₁l'; [ | assumption ].
      apply Hρ₁.
  - destruct Hlift₂ as [ Hρ₂l Hρ₂l' ]; intro l'.
    destruct (dec_effect_eq l l').
    * subst; destruct (ρ₂' l'); [ | constructor ].
      specialize (Hρ₂ l'); rewrite Hρ₂l in Hρ₂; simpl in Hρ₂.
      rewrite <- plus_n_Sm in Hρ₂; assumption.
    * rewrite <- Hρ₂l'; [ | assumption ].
      apply Hρ₂.
  - exact Hξ₂.
  - exact HE'.
Qed.

(** Characterization of rel_c proved by Loeb induction *)
Lemma rel_c_intro_aux {EV : Set} {ε₁ ε₂} (M : EffectMod ε₁ ε₂)
    τ₁ τ₂ (η : EV → IRel rel_row_sig)
    E₁ E₂ n :
  n ⊨ (∀ᵢ l, rel_cM l 0 0 M τ₁ ε₁ τ₂ ε₂ η E₁ E₂) →
  n ⊨ (∀ᵢ v₁ v₂, 〚Σ ⊢ τ₁〛 η v₁ v₂ ⇒
    E〚Σ ⊢ τ₂ // ε₂〛 η (rplug E₁ v₁) (rplug E₂ v₂)) →
  n ⊨ C〚Σ ⊢ τ₁ // ε₁ ↝ τ₂ // ε₂〛 η E₁ E₂.
Proof.
intros HE_s HE_v.
loeb_induction.
isplit.
+ exact HE_v.
+ iintro E₁'; iintro E₂'; iintro e₁; iintro e₂; iintro Hs.
  idestruct Hs as ρ₁ Hs; idestruct Hs as ρ₂ Hs.
  idestruct Hs as ν Hs.
  idestruct Hs as Hs Hs'; idestruct Hs' as Hfree Hν.
  apply I_Prop_elim in Hfree; destruct Hfree as [ Hfree₁ Hfree₂ ].
  eapply rel_cM_lemma with (ξ₁ := λ _, 0) (ξ₂ := λ _, 0).
  - later_shift; clear e₁ e₂ Hs.
    iintro e₁; iintro e₂; iintro He.
    eapply rel_c_compat_e; [ exact IH | ].
    exact He.
  - exact Hs.
  - exact HE_s.
  - exact Hfree₁.
  - exact Hfree₂.
  - clear e₁ e₂ ρ₁ ρ₂ ν Hs Hfree₁ Hfree₂ Hν.
    iintro e₁; iintro e₂; iintro ρ₁; iintro ρ₂; iintro ν.
    iintro Hr.
    iexists ρ₁; iexists ρ₂; isplit; [ exact Hr | ].
    iintro; split; unfold add_func.
    * intro l; destruct (ρ₁ l); reflexivity.
    * intro l; destruct (ρ₂ l); reflexivity.
  - iintro e₁'; iintro e₂'; iintro He'.
    iespecialize Hν; assert (Hν' := I_arrow_elim Hν He'); later_shift.
    apply rel_expr_cl_fix_unroll in Hν'; assumption.
Qed.

(** The same lemma as before, but step-indexed quantifier was replaced
by usual one for convenience. *)
Lemma rel_c_intro {EV : Set} {ε₁ ε₂} (M : EffectMod ε₁ ε₂)
    τ₁ τ₂ (η : EV → IRel rel_row_sig)
    E₁ E₂ n :
  (∀ v₁ v₂, n ⊨ 〚Σ ⊢ τ₁〛 η v₁ v₂ ⇒
    E〚Σ ⊢ τ₂ // ε₂〛 η (rplug E₁ v₁) (rplug E₂ v₂)) →
  (∀ l, n ⊨ rel_cM l 0 0 M τ₁ ε₁ τ₂ ε₂ η E₁ E₂) →
  n ⊨ C〚Σ ⊢ τ₁ // ε₁ ↝ τ₂ // ε₂〛 η E₁ E₂.
Proof.
intros HE_v HE_s; eapply rel_c_intro_aux.
+ iintro l; apply HE_s.
+ iintro v₁; iintro v₂; apply HE_v.
Qed.

End ContextRel.

Arguments ModShift  {Eff} [EV] l [ε₁] [ε₂].
Arguments ModHandle {Eff} [EV] l [ε₁] [ε₂].