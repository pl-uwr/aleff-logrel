(** This module contains the proofs that the logical relation is
compatible with respect to subtyping and effect subsumption rules *)
(** The end goal of the module is to show that the relation is
compatible with the typing rule for subtyping and subeffecting. To
this end, the relation is shown compatible with all the rules that
define the subtyping and effect subsumption relations, by matching
each rule with a lemma about the logical relation. *)
Require Import Utf8.
Require Import IxFree.Lib.
Require Import Lang.Syntax Lang.Typing.
Require Import BinaryRelation.Core BinaryRelation.Properties.
Require Import BinaryRelation.DefUnroll BinaryRelation.ContextRel.

Section Subsumption.
Context {Eff : Set} {EffDec : DecEffectEq Eff}.
Context (Σ : eff_sig Eff).

Lemma lift_map_diam l₁ l₂ ρ₁ ρ₂ ρ₃ :
  lift_lmap l₁ ρ₂ ρ₁ → lift_lmap l₂ ρ₃ ρ₂ →
  lift_lmap l₂ (shift_lmap l₁ ρ₃) ρ₁.
Proof.
intros [ Hopt₁ H₁₂ ] [ Hopt₂ H₂₃ ]; split.
+ unfold shift_lmap.
  destruct (dec_effect_eq l₁ l₂) as [ ? | Hneq₁ ].
  - subst.
    rewrite Hopt₁, Hopt₂; reflexivity.
  - rewrite H₁₂; [ | assumption ].
    rewrite Hopt₂; destruct (ρ₃ l₂); reflexivity.
+ intros l Hneq; unfold shift_lmap.
  destruct (dec_effect_eq l₁ l) as [ ? | Hneq₁ ].
  - subst.
    rewrite Hopt₁, H₂₃; [ | assumption ].
    reflexivity.
  - rewrite H₁₂; [ | assumption ].
    rewrite H₂₃; [ | assumption ].
    destruct (ρ₃ l); reflexivity.
Qed.

Lemma sub_rel_r (EV : Set) (ε₁ ε₂ : effect Eff EV) :
  SF[ ε₁ ≾ ε₂ ] →
  ∀ (η : EV → IRel rel_row_sig) e₁ e₂ ρ₁ ρ₂ ν n,
    n ⊨ R〚Σ ⊢ ε₁〛 η e₁ e₂ ρ₁ ρ₂ ν → n ⊨ R〚Σ ⊢ ε₂〛 η e₁ e₂ ρ₁ ρ₂ ν.
Proof.
induction 1.
+ auto.
+ auto.
+ intros η e₁ e₂ ρ₁ ρ₂ ν n; simpl.
  destruct (dec_effect_eq l₁ l₂) as [ ? | Hneq ]; [ subst; auto | ].
  intro H₁; idestruct H₁ as H₁ H₁.
  { iright; iexists ρ₁; iexists ρ₂; isplit; [ | ileft; assumption ].
    unfold Frel_eff in H₁.
    idestruct H₁ as op H₁; idestruct H₁ as Θ H₁.
    idestruct H₁ as v₁ H₁; idestruct H₁ as v₂ H₁.
    idestruct H₁ as H₁ Hv; apply I_Prop_elim in H₁.
    destruct H₁ as [ _ Hρ₁l₁ Hρ₂l₂ Hρ₁ Hρ₂ ].
    iintro; split; constructor.
    + rewrite Hρ₁; auto.
    + reflexivity.
    + rewrite Hρ₂; auto.
    + reflexivity.
  }
  idestruct H₁ as ρ₁' H₁; idestruct H₁ as ρ₂' H₁.
  idestruct H₁ as Hρ' H₁; apply I_Prop_elim in Hρ'.
  destruct Hρ' as [ Hρ₁' Hρ₂' ].
  idestruct H₁ as H₁ H₁.
  { ileft.
    unfold Frel_eff in H₁.
    idestruct H₁ as op H₁; idestruct H₁ as Θ H₁.
    idestruct H₁ as v₁ H₁; idestruct H₁ as v₂ H₁.
    idestruct H₁ as H₁ Hv; apply I_Prop_elim in H₁.
    iexists op; iexists Θ; iexists v₁; iexists v₂.
    isplit; [ | assumption ].
    destruct H₁ as [ Hop Hρ₁l₂ Hρ₂l₂ Hρ₁ Hρ₂ ]; iintro; constructor.
    - assumption.
    - destruct Hρ₁' as [ _ Hev ]; rewrite Hev; assumption.
    - destruct Hρ₂' as [ _ Hev ]; rewrite Hev; assumption.
    - intros l Hneq'.
      destruct (dec_effect_eq l₁ l).
      * subst; destruct Hρ₁' as [ Hev _ ].
        rewrite Hev, Hρ₁; auto.
      * subst; destruct Hρ₁' as [ _ Hev ]; rewrite Hev; auto.
    - intros l Hneq'.
      destruct (dec_effect_eq l₁ l).
      * subst; destruct Hρ₂' as [ Hev _ ].
        rewrite Hev, Hρ₂; auto.
      * subst; destruct Hρ₂' as [ _ Hev ]; rewrite Hev; auto.
  }
  idestruct H₁ as ρ₁'' H₁; idestruct H₁ as ρ₂'' H₁.
  idestruct H₁ as Hρ'' H₁.
  apply I_Prop_elim in Hρ''; destruct Hρ'' as [ Hρ₁'' Hρ₂'' ].
  iright; iexists (shift_lmap l₁ ρ₁''); iexists (shift_lmap l₁ ρ₂'').
  isplit.
  { iintro; split; eapply lift_map_diam; eassumption.
  }
  iright; iexists ρ₁''; iexists ρ₂''.
  isplit; [ | assumption ].
  iintro; split; apply lift_lmap_shift.
+ intros η e₁ e₂ ρ₁ ρ₂ ν n H; simpl in H.
  apply I_Prop_elim in H; destruct H.
+ intros η e₁ e₂ ρ₁ ρ₂ ν n; simpl.
  intro Hε; idestruct Hε as Hε Hε.
  - ileft; assumption.
  - idestruct Hε as ρ₁' Hε; idestruct Hε as ρ₂' Hε; idestruct Hε as Hρ Hε.
    iright; iexists ρ₁'; iexists ρ₂'; isplit; auto.
Qed.

Lemma sub_rel_c_aux {EV : Set} (η : EV → IRel rel_row_sig)
    τ₁ τ₂ ε₁ ε₂ n :
  (∀ v₁ v₂ n, n ⊨ 〚Σ ⊢ τ₁〛 η v₁ v₂ → n ⊨ 〚Σ ⊢ τ₂〛 η v₁ v₂) →
  SF[ ε₁ ≾ ε₂ ] →
  n ⊨ C〚Σ ⊢ τ₁ // ε₁ ↝ τ₂ // ε₂〛 η r_hole r_hole.
Proof.
intros Hτ Hε; loeb_induction; isplit.
+ iintro v₁; iintro v₂; iintro Hv; simpl.
  apply rel_v_in_e; auto.
+ iintro E₁; iintro E₂; iintro e₁; iintro e₂; iintro HEe; simpl.
  apply rel_s_in_e.
  idestruct HEe as ρ₁ HEe; idestruct HEe as ρ₂ HEe; idestruct HEe as ν HEe.
  idestruct HEe as He HEe; idestruct HEe as Hfree HE.
  apply I_Prop_elim in Hfree; destruct Hfree as [ Hfree₁ Hfree₂ ].
  eapply rel_s_roll.
  - eapply sub_rel_r; eassumption.
  - assumption.
  - assumption.
  - intros e₁' e₂'; iintro He'.
    assert (Hfix : n ⊨ 
      ▷rel_expr_cl_fix (R〚Σ ⊢ ε₁〛 η) (〚Σ ⊢ τ₁〛 η)
        (rplug E₁ e₁') (rplug E₂ e₂')).
    { iespecialize HE; iapply HE; assumption. }
    later_shift.
    apply rel_expr_cl_fix_unroll in Hfix.
    change (n ⊨ E〚Σ ⊢ τ₂ // ε₂〛 η
      (rplug r_hole (rplug E₁ e₁')) (rplug r_hole (rplug E₂ e₂'))).
    eapply rel_c_compat_e; eassumption.
Qed.

Lemma sub_rel_e_aux {EV : Set} (η : EV → IRel rel_row_sig)
    e₁ e₂ τ₁ τ₂ ε₁ ε₂ n :
  (∀ v₁ v₂ n, n ⊨ 〚Σ ⊢ τ₁〛 η v₁ v₂ → n ⊨ 〚Σ ⊢ τ₂〛 η v₁ v₂) →
  SF[ ε₁ ≾ ε₂ ] →
  n ⊨ E〚Σ ⊢ τ₁ // ε₁〛 η e₁ e₂ →
  n ⊨ E〚Σ ⊢ τ₂ // ε₂〛 η e₁ e₂.
Proof.
intros Hτ Hε He.
change (n ⊨ E〚Σ ⊢ τ₂ // ε₂〛 η (rplug r_hole e₁) (rplug r_hole e₂)).
eapply rel_c_compat_e; [ | eassumption ].
apply sub_rel_c_aux; assumption.
Qed.

Lemma sub_rel_v (EV : Set) (τ₁ τ₂ : typ Eff EV) :
  ST[ τ₁ ≾ τ₂ ] →
  ∀ (η : EV → IRel rel_row_sig) v₁ v₂ n,
    n ⊨ 〚Σ ⊢ τ₁〛 η v₁ v₂ → n ⊨ 〚Σ ⊢ τ₂〛 η v₁ v₂.
Proof.
induction 1.
+ auto.
+ auto.
+ intros η f₁ f₂ n; simpl; intro Hf.
  iintro v₁; iintro v₂; iintro Hv.
  eapply sub_rel_e_aux.
  - apply IHsubtyp2.
  - eassumption.
  - iespecialize Hf; iapply Hf.
    apply IHsubtyp1; assumption.
+ intros η f₁ f₂ n; intro Hf; simpl.
  iintro R.
  change (n ⊨ E〚Σ ⊢ τ₂ // ef_nil〛 (η ,+ R) (e_eapp f₁) (e_eapp f₂)).
  eapply sub_rel_e_aux.
  - apply IHsubtyp.
  - constructor.
  - simpl in Hf; ispecialize Hf R; exact Hf.
Qed.

Lemma sub_compat_cl {EV : Set} (η : EV → IRel rel_row_sig)
    e₁ e₂ τ₁ τ₂ ε₁ ε₂ n :
  ST[ τ₁ ≾ τ₂ ] → SF[ ε₁ ≾ ε₂ ] →
  n ⊨ E〚Σ ⊢ τ₁ // ε₁〛 η e₁ e₂ →
  n ⊨ E〚Σ ⊢ τ₂ // ε₂〛 η e₁ e₂.
Proof.
intros Hτ Hε He.
eapply sub_rel_e_aux; [ | | eassumption ].
+ apply sub_rel_v; assumption.
+ assumption.
Qed.

(** The main lemma of the module: the logical relation is compatible
with the typing rule that enables subtyping and subeffecting. *)
Lemma sub_compat {EV V : Set} (Γ : V → typ Eff EV) e₁ e₂ τ₁ τ₂ ε₁ ε₂ n :
  ST[ τ₁ ≾ τ₂ ] → SF[ ε₁ ≾ ε₂ ] →
  n ⊨ T〚Σ; Γ ⊨ e₁ ≾ e₂ ∷ τ₁ // ε₁〛 →
  n ⊨ T〚Σ; Γ ⊨ e₁ ≾ e₂ ∷ τ₂ // ε₂〛.
Proof.
intros Hτ Hε He.
iintro η; iintro γ₁; iintro γ₂; iintro Hγ.
eapply sub_compat_cl.
+ eassumption.
+ eassumption.
+ iespecialize He; iapply He; assumption.
Qed.

End Subsumption.