(** This module provides lemmas about weakening and substitution for
the semantic effect rows. *)
(** Particularly important is the lemma rel_e_subst which shows that
the interpretation of a type τ[η / α] is equivalent to the
interpretation of τ where the *free* variable α is associated with the
interpretation of row ε. This equivalence is crucial for proving
compatibility: since α can appear both in co- and contravariant
positions, the relation has to be *invariant* under substitution of
rows for row variables. *)
Require Import Utf8.
Require Import IxFree.Lib.
Require Import Lang.Syntax Lang.Typing.
Require Import BinaryRelation.Core.

Section EffectSubst.
Context {Eff : Set} {EffDec : DecEffectEq Eff}.
Context (Σ : eff_sig Eff).

Fixpoint Frel_row_weaken {EA EB : Set} (Φ : EA → EB)
    (R₁ R₂ : IRel rel_tp0_v2_sig)
    (η₁    : EB → IRel rel_row_sig)
    (η₂    : EA → IRel rel_row_sig)
    (ε     : effect Eff EA)
    (e₁ e₂ : expr0 Eff)
    (ρ₁ ρ₂ : Eff → option nat)
    (ν     : IRel rel_e2_sig) n :
  n ⊨ ▷(R₁ ≈ᵢ R₂) →
  n ⊨ (∀ᵢ μ, η₁ (Φ μ) ≈ᵢ η₂ μ) →
  n ⊨ Frel_row Σ R₁ η₁ (fmap Φ ε) e₁ e₂ ρ₁ ρ₂ ν ⇔
      Frel_row Σ R₂ η₂ ε e₁ e₂ ρ₁ ρ₂ ν.
Proof.
intros HR Hη; destruct ε as [ μ | | l ε ]; simpl.
+ iespecialize Hη; exact Hη.
+ auto_contr.
+ auto_contr.
  - apply Frel_eff_contractive; assumption.
  - apply Frel_row_weaken; assumption.
Qed.

Fixpoint Frel_v_weaken {EA EB : Set} (Φ : EA → EB)
    (R₁ R₂ : IRel rel_tp0_v2_sig)
    (η₁    : EB → IRel rel_row_sig)
    (η₂    : EA → IRel rel_row_sig)
    (τ     : typ Eff EA)
    (v₁ v₂ : value0 Eff) n :
  n ⊨ ▷(R₁ ≈ᵢ R₂) →
  n ⊨ (∀ᵢ μ, η₁ (Φ μ) ≈ᵢ η₂ μ) →
  n ⊨ Frel_v Σ R₁ η₁ (tmap Φ τ) v₁ v₂ ⇔ Frel_v Σ R₂ η₂ τ v₁ v₂.
Proof.
intros HR Hη; destruct τ as [ | τ₁ ε τ₂ | τ ]; simpl.
+ auto_contr.
+ auto_contr.
  - apply Frel_v_weaken; assumption.
  - apply rel_expr_cl_nonexpansive.
    * iintro e₁; iintro e₂; iintro ρ₁; iintro ρ₂; iintro ν.
      apply Frel_row_weaken; assumption.
    * iintro u₁; iintro u₂.
      apply Frel_v_weaken; assumption.
+ auto_contr.
  apply rel_expr_cl_nonexpansive.
  - iintro; iintro; iintro; iintro; iintro; auto_contr.
  - iintro u₁; iintro u₂; apply Frel_v_weaken; [ assumption | ].
    iintro μ; iintro e₁; iintro e₂; iintro ρ₁; iintro ρ₂; iintro ν.
    destruct μ as [ | μ ]; simpl.
    * auto_contr.
    * iespecialize Hη; exact Hη.
Qed.

Lemma rel_v_weaken {EA EB : Set} (Φ : EA → EB)
    (η₁    : EB → IRel rel_row_sig)
    (η₂    : EA → IRel rel_row_sig)
    (τ     : typ Eff EA)
    (v₁ v₂ : value0 Eff) n :
  n ⊨ (∀ᵢ μ, η₁ (Φ μ) ≈ᵢ η₂ μ) →
  n ⊨ 〚Σ ⊢ tmap Φ τ〛 η₁ v₁ v₂ ⇔ 〚Σ ⊢ τ〛 η₂ v₁ v₂.
Proof.
intro Hη.
apply Frel_v_weaken.
+ iintro; iintro τ'; iintro u₁; iintro u₂; auto_contr.
+ assumption.
Qed.

Lemma rel_v_weaken_l {EA EB : Set} (Φ : EA → EB)
    (η₁    : EB → IRel rel_row_sig)
    (η₂    : EA → IRel rel_row_sig)
    (τ     : typ Eff EA)
    (v₁ v₂ : value0 Eff) n :
  n ⊨ (∀ᵢ μ, η₁ (Φ μ) ≈ᵢ η₂ μ) →
  n ⊨ 〚Σ ⊢ τ〛 η₂ v₁ v₂ →
  n ⊨ 〚Σ ⊢ tmap Φ τ〛 η₁ v₁ v₂.
Proof.
intros Hη Hv.
assert (H := rel_v_weaken Φ η₁ η₂ τ v₁ v₂ n Hη).
idestruct H as H₁ H₂; iapply H₂; assumption.
Qed.

Lemma rel_v_weaken_r {EA EB : Set} (Φ : EA → EB)
    (η₁    : EB → IRel rel_row_sig)
    (η₂    : EA → IRel rel_row_sig)
    (τ     : typ Eff EA)
    (v₁ v₂ : value0 Eff) n :
  n ⊨ (∀ᵢ μ, η₁ (Φ μ) ≈ᵢ η₂ μ) →
  n ⊨ 〚Σ ⊢ tmap Φ τ〛 η₁ v₁ v₂ →
  n ⊨ 〚Σ ⊢ τ〛 η₂ v₁ v₂.
Proof.
intros Hη Hv.
assert (H := rel_v_weaken Φ η₁ η₂ τ v₁ v₂ n Hη).
idestruct H as H₁ H₂; iapply H₁; assumption.
Qed.

Lemma rel_e_weaken {EA EB : Set} (Φ : EA → EB)
    (η₁    : EB → IRel rel_row_sig)
    (η₂    : EA → IRel rel_row_sig)
    (τ     : typ Eff EA)
    (ε     : effect Eff EA)
    (e₁ e₂ : expr0 Eff) n :
  n ⊨ (∀ᵢ μ, η₁ (Φ μ) ≈ᵢ η₂ μ) →
  n ⊨ E〚Σ ⊢ tmap Φ τ // fmap Φ ε〛 η₁ e₁ e₂ ⇔ E〚Σ ⊢ τ // ε〛 η₂ e₁ e₂.
Proof.
intro Hη; apply rel_expr_cl_nonexpansive.
+ iintro e₁'; iintro e₂'; iintro ρ₁; iintro ρ₂; iintro ν.
  apply Frel_row_weaken.
  - iintro; iintro τ'; iintro v₁; iintro v₂; auto_contr.
  - exact Hη.
+ iintro v₁; iintro v₂; apply Frel_v_weaken.
  - iintro; iintro τ'; iintro u₁; iintro u₂; auto_contr.
  - exact Hη.
Qed.

Lemma rel_e_weaken_l {EA EB : Set} (Φ : EA → EB)
    (η₁    : EB → IRel rel_row_sig)
    (η₂    : EA → IRel rel_row_sig)
    (τ     : typ Eff EA)
    (ε     : effect Eff EA)
    (e₁ e₂ : expr0 Eff) n :
  n ⊨ (∀ᵢ μ, η₁ (Φ μ) ≈ᵢ η₂ μ) →
  n ⊨ E〚Σ ⊢ τ // ε〛 η₂ e₁ e₂ →
  n ⊨ E〚Σ ⊢ tmap Φ τ // fmap Φ ε〛 η₁ e₁ e₂.
Proof.
intros Hη Hv.
assert (H := rel_e_weaken Φ η₁ η₂ τ ε e₁ e₂ n Hη).
idestruct H as H₁ H₂; iapply H₂; assumption.
Qed.

Lemma rel_e_weaken_r {EA EB : Set} (Φ : EA → EB)
    (η₁    : EB → IRel rel_row_sig)
    (η₂    : EA → IRel rel_row_sig)
    (τ     : typ Eff EA)
    (ε     : effect Eff EA)
    (e₁ e₂ : expr0 Eff) n :
  n ⊨ (∀ᵢ μ, η₁ (Φ μ) ≈ᵢ η₂ μ) →
  n ⊨ E〚Σ ⊢ tmap Φ τ // fmap Φ ε〛 η₁ e₁ e₂ →
  n ⊨ E〚Σ ⊢ τ // ε〛 η₂ e₁ e₂.
Proof.
intros Hη Hv.
assert (H := rel_e_weaken Φ η₁ η₂ τ ε e₁ e₂ n Hη).
idestruct H as H₁ H₂; iapply H₁; assumption.
Qed.

(* ========================================================================= *)

Fixpoint Frel_row_subst {EA EB : Set} (Φ : EA → effect Eff EB)
    (R₁ R₂ : IRel rel_tp0_v2_sig)
    (η₁    : EB → IRel rel_row_sig)
    (η₂    : EA → IRel rel_row_sig)
    (ε     : effect Eff EA)
    (e₁ e₂ : expr0 Eff)
    (ρ₁ ρ₂ : Eff → option nat)
    (ν     : IRel rel_e2_sig) n :
  n ⊨ ▷(R₁ ≈ᵢ R₂) →
  n ⊨ (∀ᵢ μ, (Frel_row Σ R₁ η₁ (Φ μ) : IRel rel_row_sig) ≈ᵢ η₂ μ) →
  n ⊨ Frel_row Σ R₁ η₁ (fbind Φ ε) e₁ e₂ ρ₁ ρ₂ ν ⇔
      Frel_row Σ R₂ η₂ ε e₁ e₂ ρ₁ ρ₂ ν.
Proof.
intros HR Hη; destruct ε as [ μ | | l ε ]; simpl.
+ iespecialize Hη; exact Hη.
+ auto_contr.
+ auto_contr.
  - apply Frel_eff_contractive; assumption.
  - apply Frel_row_subst; assumption.
Qed.

Fixpoint Frel_v_subst {EA EB : Set} (Φ : EA → effect Eff EB)
    (R₁ R₂ : IRel rel_tp0_v2_sig)
    (η₁    : EB → IRel rel_row_sig)
    (η₂    : EA → IRel rel_row_sig)
    (τ     : typ Eff EA)
    (v₁ v₂ : value0 Eff) n :
  n ⊨ ▷(R₁ ≈ᵢ R₂) →
  n ⊨ (∀ᵢ μ, (Frel_row Σ R₁ η₁ (Φ μ) : IRel rel_row_sig) ≈ᵢ η₂ μ) →
  n ⊨ Frel_v Σ R₁ η₁ (tbind Φ τ) v₁ v₂ ⇔ Frel_v Σ R₂ η₂ τ v₁ v₂.
Proof.
intros HR Hη; destruct τ as [ | τ₁ ε τ₂ | τ ]; simpl.
+ auto_contr.
+ auto_contr.
  - apply Frel_v_subst; assumption.
  - apply rel_expr_cl_nonexpansive.
    * iintro e₁; iintro e₂; iintro ρ₁; iintro ρ₂; iintro ν.
      apply Frel_row_subst; assumption.
    * iintro u₁; iintro u₂.
      apply Frel_v_subst; assumption.
+ auto_contr.
  apply rel_expr_cl_nonexpansive.
  - iintro; iintro; iintro; iintro; iintro; auto_contr.
  - iintro u₁; iintro u₂; apply Frel_v_subst; [ assumption | ].
    iintro μ; iintro e₁; iintro e₂; iintro ρ₁; iintro ρ₂; iintro ν.
    destruct μ as [ | μ ]; simpl.
    * auto_contr.
    * assert (Hη' : n ⊨
        Frel_row Σ R₁ (η₁,+ x) (fshift (Φ μ)) e₁ e₂ ρ₁ ρ₂ ν ⇔
        Frel_row Σ R₁ η₁ (Φ μ) e₁ e₂ ρ₁ ρ₂ ν).
      { apply Frel_row_weaken.
        - iintro; iintro; iintro; iintro; auto_contr.
        - iintro; iintro; iintro; iintro; iintro; iintro; simpl.
          auto_contr.
      }
      ispecialize Hη μ; iespecialize Hη; idestruct Hη as Hη₁ Hη₂.
      idestruct Hη' as Hη₁' Hη₂'.
      { isplit.
      + iintro; iapply Hη₁; iapply Hη₁'; assumption.
      + iintro; iapply Hη₂'; iapply Hη₂; assumption.
      }
Qed.

Lemma rel_e_subst {EA EB : Set} (Φ : EA → effect Eff EB)
    (η₁    : EB → IRel rel_row_sig)
    (η₂    : EA → IRel rel_row_sig)
    (τ     : typ Eff EA)
    (ε     : effect Eff EA)
    (e₁ e₂ : expr0 Eff) n :
  n ⊨ (∀ᵢ μ, (R〚Σ ⊢ Φ μ〛 η₁ : IRel rel_row_sig) ≈ᵢ η₂ μ) →
  n ⊨ E〚Σ ⊢ tbind Φ τ // fbind Φ ε〛 η₁ e₁ e₂ ⇔ E〚Σ ⊢ τ // ε〛 η₂ e₁ e₂.
Proof.
intro Hη; apply rel_expr_cl_nonexpansive.
+ iintro e₁'; iintro e₂'; iintro ρ₁; iintro ρ₂; iintro ν.
  apply Frel_row_subst.
  - iintro; iintro τ'; iintro v₁; iintro v₂; auto_contr.
  - exact Hη.
+ iintro v₁; iintro v₂; apply Frel_v_subst.
  - iintro; iintro τ'; iintro u₁; iintro u₂; auto_contr.
  - exact Hη.
Qed.

Lemma rel_e_subst_l {EA EB : Set} (Φ : EA → effect Eff EB)
    (η₁    : EB → IRel rel_row_sig)
    (η₂    : EA → IRel rel_row_sig)
    (τ     : typ Eff EA)
    (ε     : effect Eff EA)
    (e₁ e₂ : expr0 Eff) n :
  n ⊨ (∀ᵢ μ, (R〚Σ ⊢ Φ μ〛 η₁ : IRel rel_row_sig) ≈ᵢ η₂ μ) →
  n ⊨ E〚Σ ⊢ τ // ε〛 η₂ e₁ e₂ →
  n ⊨ E〚Σ ⊢ tbind Φ τ // fbind Φ ε〛 η₁ e₁ e₂.
Proof.
intros Hη Hv.
assert (H := rel_e_subst Φ η₁ η₂ τ ε e₁ e₂ n Hη).
idestruct H as H₁ H₂; iapply H₂; assumption.
Qed.

Lemma rel_e_subst_r {EA EB : Set} (Φ : EA → effect Eff EB)
    (η₁    : EB → IRel rel_row_sig)
    (η₂    : EA → IRel rel_row_sig)
    (τ     : typ Eff EA)
    (ε     : effect Eff EA)
    (e₁ e₂ : expr0 Eff) n :
  n ⊨ (∀ᵢ μ, (R〚Σ ⊢ Φ μ〛 η₁ : IRel rel_row_sig) ≈ᵢ η₂ μ) →
  n ⊨ E〚Σ ⊢ tbind Φ τ // fbind Φ ε〛 η₁ e₁ e₂ →
  n ⊨ E〚Σ ⊢ τ // ε〛 η₂ e₁ e₂.
Proof.
intros Hη Hv.
assert (H := rel_e_subst Φ η₁ η₂ τ ε e₁ e₂ n Hη).
idestruct H as H₁ H₂; iapply H₁; assumption.
Qed.

End EffectSubst.