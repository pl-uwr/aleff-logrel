(** This module define the (step-indexed) observation relation *)
(** Observation is defined as a step-indexed cotermination at unit
values, by first defining a functor that matches the rule shown in
Figure 8 of the paper but uses a parameter rather than recursion, and
then taking a fixed-point of that functor as the definition of the
observation. *)
Require Import Utf8.
Require Import IxFree.Lib.
Require Import Lang.Reduction Lang.Syntax.

Definition obs_rsig {Eff : Set} : list Type :=
  ((expr0 Eff:Type) :: (expr0 Eff:Type) :: nil)%list.

Definition F_Obs (Eff : Set) (Obs : IRel obs_rsig) (e₁ e₂ : expr0 Eff) :=
  (e₁ = v_unit ∧ red_rtc e₂ v_unit)ᵢ
  ∨ᵢ ∃ᵢ e₁', (red e₁ e₁')ᵢ ∧ᵢ ▷Obs e₁' e₂.

Section Obs.
Context {Eff : Set}.

Lemma F_Obs_contractive : contractive obs_rsig (F_Obs Eff).
Proof.
unfold contractive.
intros R₁ R₂ n; iintro HR; simpl.
iintro e₁; iintro e₂; unfold F_Obs; auto_contr.
iespecialize HR; eassumption.
Qed.

(** The observation is defined as a fixed-point of the functor
F_Obs. Note the requirement that the functor is contractive, fulfilled
via using the later operator to guard the recursive occurrence. *)
Definition Obs := I_fix obs_rsig (F_Obs Eff) F_Obs_contractive.

Lemma Obs_roll (n : nat) (e₁ e₂ : expr0 Eff) :
  n ⊨ F_Obs Eff Obs e₁ e₂ → n ⊨ Obs e₁ e₂.
Proof.
intro He; unfold Obs; apply (I_fix_roll obs_rsig); assumption.
Qed.

Lemma Obs_unroll (n : nat) (e₁ e₂ : expr0 Eff) :
  n ⊨ Obs e₁ e₂ → n ⊨ F_Obs Eff Obs e₁ e₂.
Proof.
intro He; apply (I_fix_unroll obs_rsig); assumption.
Qed.

Lemma Obs_red_l (e₁ e₁' e₂ : expr0 Eff) (n : nat) :
  red e₁ e₁' →
  n ⊨ ▷Obs e₁' e₂ →
  n ⊨ Obs e₁ e₂.
Proof.
intros Hred He'; apply Obs_roll; unfold F_Obs.
iright; iexists e₁'; isplit.
+ iintro; assumption.
+ assumption.
Qed.

Lemma Obs_red_r (e₁ e₂ e₂' : expr0 Eff) (n : nat) :
  red e₂ e₂' →
  n ⊨ Obs e₁ e₂' →
  n ⊨ Obs e₁ e₂.
Proof.
intro Hred.
assert (HGen : n ⊨ ∀ᵢ e₁, Obs e₁ e₂' ⇒ Obs e₁ e₂).
{ clear e₁; loeb_induction; iintro e₁; iintro He'.
  apply Obs_roll; apply Obs_unroll in He'; idestruct He' as He' He'.
  + apply I_Prop_elim in He'.
    ileft; iintro; split.
    - apply He'.
    - econstructor 2; [ eassumption | ].
      apply He'.
  + idestruct He' as e₁' He₁'.
    idestruct He₁' as Hred₁ He'.
    iright; iexists e₁'; isplit; [ assumption | ].
    later_shift.
    iespecialize IH; iapply IH; assumption.
}
intro; iespecialize HGen; iapply HGen; assumption.
Qed.

End Obs.