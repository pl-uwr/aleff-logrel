(** Core definitions of the logical relation *)
(** This module formalises the core parts of the logical relation
definition (most of Section 3.2 of the paper). Note that recursion is
disentangled here: we define various closures (evaluation contexts,
expressions, simple expressions) that could be applied to any semantic
type and row through an application of the fixed-point theorem for
contractive step-indexed maps, and only then we proceed to the actual
interpretation of types. Consult Appendix B for a development closer
to the one presented here, although still less explicit. *)
Require Import Utf8.
Require Import IxFree.Lib.
Require Import Lang.Syntax Lang.Typing.
Require Import BinaryRelation.ProgramObs.

(** First, define expression, evaluation context and simple expression
closures of any semantic type and row. *)
Section RelationEKS.
Context {Eff : Set}.

Definition rel_v2_sig : list Type :=
  ((value0 Eff : Type) :: (value0 Eff : Type) :: nil)%list.

Definition rel_tp0_v2_sig : list Type :=
  (typ0 Eff : Type) :: rel_v2_sig.

Definition rel_e2_sig : list Type :=
  ((expr0 Eff : Type) :: (expr0 Eff : Type) :: nil)%list.

Definition rel_row_sig : list Type :=
  ((expr0 Eff : Type) :: (expr0 Eff : Type)
  :: ((Eff → option nat) : Type) :: ((Eff → option nat) : Type)
  :: IRel rel_e2_sig :: nil)%list.

(** Relation on simple expressions defined as a functor: the
interpretations of effect rows and of the "recursive" call to the
interpretation of expressions are taken as parameters. *)
Definition Frel_simpl_cl
    (RelR  : IRel rel_row_sig)
    (RelE  : IRel rel_e2_sig)
    (E₁ E₂ : rctx0 Eff)
    (e₁ e₂ : expr0 Eff) : IProp :=
  ∃ᵢ ρ₁ ρ₂ ν,
    RelR e₁ e₂ ρ₁ ρ₂ ν ∧ᵢ
    (rfree_m ρ₁ E₁ ∧ rfree_m ρ₂ E₂)ᵢ ∧ᵢ
    ∀ᵢ e₁' e₂', ν e₁' e₂' ⇒ ▷ RelE (rplug E₁ e₁') (rplug E₂ e₂').

(** Relation on evaluation contexts defined as a functor: the
interpretations of effect rows, types, and of the "recursive" call to
the interpretation of expressions are taken as parameters. *)
Definition Frel_ectx_cl
    (RelR  : IRel rel_row_sig)
    (RelV  : IRel rel_v2_sig)
    (RelE  : IRel rel_e2_sig)
    (F₁ F₂ : ectx0 Eff) : IProp :=
  (∀ᵢ v₁ v₂, RelV v₁ v₂ ⇒ Obs (plug F₁ v₁) (plug F₂ v₂)) ∧ᵢ
  (∀ᵢ E₁ E₂ e₁ e₂,
    Frel_simpl_cl RelR RelE E₁ E₂ e₁ e₂ ⇒
    Obs (plug F₁ (rplug E₁ e₁)) (plug F₂ (rplug E₂ e₂))).

(** Relation on expressions defined as a functor: the interpretations
of effect rows, types, and of the "recursive" call to the
interpretation of expressions are taken as parameters. *)
Definition Frel_expr_cl
    (RelR  : IRel rel_row_sig)
    (RelV  : IRel rel_v2_sig)
    (RelE  : IRel rel_e2_sig)
    (e₁ e₂ : expr0 Eff) : IProp :=
  ∀ᵢ F₁ F₂, Frel_ectx_cl RelR RelV RelE F₁ F₂ ⇒
    Obs (plug F₁ e₁) (plug F₂ e₂).

Lemma Frel_expr_cl_contractive
    (RelR  : IRel rel_row_sig)
    (RelV  : IRel rel_v2_sig) :
  contractive rel_e2_sig (Frel_expr_cl RelR RelV).
Proof.
intros R₁ R₂ n; iintro HR.
iintro e₁; iintro e₂.
unfold Frel_expr_cl, Frel_ectx_cl, Frel_simpl_cl; auto_contr.
iespecialize HR; exact HR.
Qed.

(** With the closurer on expressions defined as a functor, we can tie
the recursion via the fixed-point operator for COFEs. Note that we
have to prove the functor is contractive (which it is, as we used
"later" in the definition). *)
Definition rel_expr_cl_fix
    (RelR  : IRel rel_row_sig)
    (RelV  : IRel rel_v2_sig) : IRel rel_e2_sig :=
  I_fix rel_e2_sig
    (Frel_expr_cl RelR RelV)
    (Frel_expr_cl_contractive RelR RelV).

End RelationEKS.

(** The closure operators of Appendix B can be defined as notations
over the operators defined above applied to the relation obtained as a
fixed point, rel_expr_cl_fix. *)
Notation rel_expr_cl RelR RelV :=
  (Frel_expr_cl RelR RelV (rel_expr_cl_fix RelR RelV)).
Notation rel_ectx_cl RelR RelV :=
  (Frel_ectx_cl RelR RelV (rel_expr_cl_fix RelR RelV)).
Notation rel_simpl_cl RelR RelV :=
  (Frel_simpl_cl RelR (rel_expr_cl_fix RelR RelV)).


(** With the closure operators defined, we can proceed to define the
interpretation of types, effects and rows. *)
Section RelationCore.
Context {Eff : Set} {EffDec : DecEffectEq Eff}.
Context (Σ : eff_sig Eff).

(** The interpretation of effects is first declared inductively, and
folded as a COFE separately, for convenience. *)
Inductive rel_eff_core l op Θ ρ₁ ρ₂ (v₁ v₂ : value0 Eff) :
    expr0 Eff → expr0 Eff → Prop :=
| Rel_eff_core :
  Ef[ Σ l ⊢ op ∷ Θ ] →
  ρ₁ l = Some 0 →
  ρ₂ l = Some 0 →
  (∀ l', l ≠ l' → ρ₁ l' = None) →
  (∀ l', l ≠ l' → ρ₂ l' = None) →
  rel_eff_core l op Θ ρ₁ ρ₂ v₁ v₂ (e_app (v_op l op) v₁) (e_app (v_op l op) v₂)
.

Definition Frel_eff
    (TyRel : IRel rel_tp0_v2_sig)
    (l     : Eff)
    (e₁ e₂ : expr0 Eff)
    (ρ₁ ρ₂ : Eff → option nat)
    (ν     : IRel rel_e2_sig) : IProp :=
  ∃ᵢ op Θ v₁ v₂,
    (rel_eff_core l op Θ ρ₁ ρ₂ v₁ v₂ e₁ e₂)ᵢ ∧ᵢ
    ▷ TyRel (op_input Θ) v₁ v₂ ∧ᵢ
    ∀ᵢ w₁ w₂ : value0 Eff, ν w₁ w₂ ⇔ ▷ TyRel (op_output Θ) w₁ w₂.

Inductive lift_lmap (l : Eff) ρ₁ ρ₂ : Prop :=
| LiftLMap :
    ρ₂ l = option_map S (ρ₁ l) →
    (∀ l', l ≠ l' → ρ₂ l' = ρ₁ l') →
    lift_lmap l ρ₁ ρ₂
.

(** The interpretation of rows is defeined along the lines shown in
the paper, but as a Coq fixpoint. *)
Fixpoint Frel_row {EV : Set}
    (TyRel : IRel rel_tp0_v2_sig)
    (η     : EV → IRel rel_row_sig)
    (ε     : effect Eff EV)
    (e₁ e₂ : expr0 Eff)
    (ρ₁ ρ₂ : Eff → option nat)
    (ν     : IRel rel_e2_sig) : IProp :=
  match ε with
  | ef_var μ    => η μ e₁ e₂ ρ₁ ρ₂ ν
  | ef_nil      => (False)ᵢ
  | ef_cons l ε =>
      Frel_eff TyRel l e₁ e₂ ρ₁ ρ₂ ν ∨ᵢ
      ∃ᵢ ρ₁' ρ₂', (lift_lmap l ρ₁' ρ₁ ∧ lift_lmap l ρ₂' ρ₂)ᵢ ∧ᵢ
        Frel_row TyRel η ε e₁ e₂ ρ₁' ρ₂' ν
  end.

Inductive rel_unit : value0 Eff → value0 Eff → Prop :=
| RelUnit : rel_unit v_unit v_unit
.

(** Like with rows, the interpretation of types as a functor is
defined as a fixpoint, which ensures well-definedness. Note that the
argument that represents recursive unfolding of the relation is also
passed to the interpretation of the rows.*)
Fixpoint Frel_v {EV : Set}
    (TyRel : IRel rel_tp0_v2_sig)
    (η     : EV → IRel rel_row_sig)
    (τ     : typ Eff EV)
    (v₁ v₂ : value0 Eff) : IProp :=
  match τ with
  | t_unit          => (rel_unit v₁ v₂)ᵢ
  | t_arrow τ₁ ε τ₂ => ∀ᵢ u₁ u₂, Frel_v TyRel η τ₁ u₁ u₂ ⇒
      rel_expr_cl (Frel_row TyRel η ε) (Frel_v TyRel η τ₂)
        (e_app v₁ u₁) (e_app v₂ u₂)
  | t_forallE τ     => ∀ᵢ R,
     rel_expr_cl (Frel_row TyRel η ef_nil) (Frel_v TyRel (η ,+ R) τ)
       (e_eapp v₁) (e_eapp v₂)
  end.

Definition Frel0_v (TyRel : IRel rel_tp0_v2_sig) : IRel rel_tp0_v2_sig :=
  @Frel_v Empty_set TyRel (λ x, match x with end).

Lemma rel_expr_cl_nonexpansive_aux
    (RR₁ RR₂ : IRel rel_row_sig)
    (RV₁ RV₂ : IRel rel_v2_sig)
    (k : nat) :
  k ⊨ RR₁ ≈ᵢ RR₂ →
  k ⊨ RV₁ ≈ᵢ RV₂ →
  k ⊨ ∀ᵢ e₁ e₂ : expr0 Eff,
    rel_expr_cl RR₁ RV₁ e₁ e₂ ⇔ rel_expr_cl RR₂ RV₂ e₁ e₂.
Proof.
intros HRR HRV; loeb_induction.
unfold rel_expr_cl, rel_ectx_cl, rel_simpl_cl; iintro e₁; iintro e₂; auto_contr.
+ iespecialize HRV; exact HRV.
+ iespecialize HRR; exact HRR.
+ iespecialize IH; idestruct IH as IH1 IH2.
 isplit; iintro H.
  - apply (I_fix_roll rel_e2_sig); apply (I_fix_unroll rel_e2_sig) in H.
    iapply IH1; exact H.
  - apply (I_fix_roll rel_e2_sig); apply (I_fix_unroll rel_e2_sig) in H.
    iapply IH2; exact H.
Qed.

Lemma rel_expr_cl_nonexpansive
    (RR₁ RR₂ : IRel rel_row_sig)
    (RV₁ RV₂ : IRel rel_v2_sig)
    (e₁ e₂   : expr0 Eff)
    (k : nat) :
  k ⊨ RR₁ ≈ᵢ RR₂ →
  k ⊨ RV₁ ≈ᵢ RV₂ →
  k ⊨ rel_expr_cl RR₁ RV₁ e₁ e₂ ⇔ rel_expr_cl RR₂ RV₂ e₁ e₂.
Proof.
intros HRR HRV.
assert (H := rel_expr_cl_nonexpansive_aux _ _ _ _ _ HRR HRV).
iespecialize H; exact H.
Qed.

Lemma Frel_eff_contractive (R₁ R₂ : IRel rel_tp0_v2_sig)
    (l : Eff) e₁ e₂ ρ₁ ρ₂ ν (k : nat) :
  k ⊨ ▷ (R₁ ≈ᵢ R₂) →
  k ⊨ Frel_eff R₁ l e₁ e₂ ρ₁ ρ₂ ν ⇔ Frel_eff R₂ l e₁ e₂ ρ₁ ρ₂ ν.
Proof.
intro HR; unfold Frel_eff; auto_contr.
+ iespecialize HR; exact HR.
+ iespecialize HR; exact HR.
Qed.

Lemma Frel_row_contractive {EV : Set} (R₁ R₂ : IRel rel_tp0_v2_sig)
    (η : EV → IRel rel_row_sig) (ε : effect Eff EV) e₁ e₂ ν (k : nat) :
  k ⊨ ▷ (R₁ ≈ᵢ R₂) → ∀ ρ₁ ρ₂,
  k ⊨ Frel_row R₁ η ε e₁ e₂ ρ₁ ρ₂ ν ⇔ Frel_row R₂ η ε e₁ e₂ ρ₁ ρ₂ ν.
Proof.
intro HR; induction ε as [ μ | | l ε IHε ]; intros ρ₁ ρ₂; simpl.
+ auto_contr.
+ auto_contr.
+ auto_contr.
  - apply Frel_eff_contractive; assumption.
  - apply IHε.
Qed.

Lemma Frel_v_contractive {EV : Set} (R₁ R₂ : IRel rel_tp0_v2_sig)
   (η : EV → IRel rel_row_sig) (τ : typ Eff EV) (k : nat) :
  k ⊨ ▷ (R₁ ≈ᵢ R₂) → ∀ v₁ v₂,
  k ⊨ Frel_v R₁ η τ v₁ v₂ ⇔ Frel_v R₂ η τ v₁ v₂.
Proof.
intros HR; induction τ as [ | EV τ₁ IHτ₁ ε τ₂ IHτ₂ | EV τ IHτ ];
    intros v₁ v₂; simpl.
+ auto_contr.
+ auto_contr.
  - apply IHτ₁.
  - apply rel_expr_cl_nonexpansive.
    * iintro e₁; iintro e₂; iintro ρ₁; iintro ρ₂; iintro ν.
      apply Frel_row_contractive; assumption.
    * iintro w₁; iintro w₂; apply IHτ₂.
+ auto_contr.
  apply rel_expr_cl_nonexpansive.
  - iintro e₁; iintro e₂; iintro ρ₁; iintro ρ₂; iintro ν.
    auto_contr.
  - iintro w₁; iintro w₂; apply IHτ.
Qed.

Lemma Frel0_v_contractive : contractive rel_tp0_v2_sig Frel0_v.
Proof.
intros R₁ R₂ n; iintro HR.
iintro τ; iintro v₁; iintro v₂.
apply Frel_v_contractive; assumption.
Qed.

(** Here we define the interpretation of types as a fixed point of the
appropriate operator. This allows us to define the usual-looking
judgments as notations that internally use the fixed point.*)
Definition rel0_v_fix :=
  I_fix rel_tp0_v2_sig Frel0_v Frel0_v_contractive.

End RelationCore.

Notation "'〚' Σ '⊢' τ '〛' η" := (Frel_v Σ (rel0_v_fix Σ) η τ) (at level 0).
Notation "'L〚' Σ '⊢' l '〛'" := (Frel_eff Σ (rel0_v_fix Σ) l).
Notation "'R〚' Σ '⊢' ε '〛' η" := (Frel_row Σ (rel0_v_fix Σ) η ε) (at level 0).
Notation "'E〚' Σ '⊢' τ '//' ε '〛' η" :=
  (rel_expr_cl (R〚Σ ⊢ ε〛 η) (〚Σ ⊢ τ〛 η)) (at level 0).
Notation "'K〚' Σ '⊢' τ '//' ε '〛' η" :=
  (rel_ectx_cl (R〚Σ ⊢ ε〛 η) (〚Σ ⊢ τ〛 η)) (at level 0).
Notation "'S〚' Σ '⊢' τ '//' ε '〛' η" :=
  (rel_simpl_cl (R〚Σ ⊢ ε〛 η) (〚Σ ⊢ τ〛 η)) (at level 0).

Definition rel_g {Eff V EV : Set} Σ
    (Γ : V → typ Eff EV) (η : EV → IRel rel_row_sig)
    (γ₁ γ₂ : V → value0 Eff) : IProp :=
  ∀ᵢ x, 〚Σ ⊢ Γ x〛 η (γ₁ x) (γ₂ x).
Notation "'G〚' Σ '⊢' Γ '〛' η" := (rel_g Σ Γ η) (at level 0, Σ at level 97).

Definition rel_v_open {Eff V EV : Set} Σ (Γ : V → typ Eff EV)
    (v₁ v₂ : value Eff V) τ : IProp :=
  ∀ᵢ η γ₁ γ₂, G〚Σ ⊢ Γ〛 η γ₁ γ₂ ⇒ 〚Σ ⊢ τ〛 η (vbind γ₁ v₁) (vbind γ₂ v₂).
Notation "'T〚' Σ ';' Γ '⊨' v₁ '≾' v₂ '∷' τ '〛'" :=
  (rel_v_open Σ Γ v₁ v₂ τ) (Γ at level 97).

Definition rel_e_open {Eff V EV : Set} Σ
    (Γ : V → typ Eff EV) (e₁ e₂ : expr Eff V) τ ε : IProp :=
  ∀ᵢ η γ₁ γ₂, G〚Σ ⊢ Γ〛 η γ₁ γ₂ ⇒ E〚Σ ⊢ τ // ε〛 η (ebind γ₁ e₁) (ebind γ₂ e₂).
Notation "'T〚' Σ ';' Γ '⊨' e₁ '≾' e₂ '∷' τ '//' ε '〛'" :=
  (rel_e_open Σ Γ e₁ e₂ τ ε) (Γ at level 97).

(* ========================================================================= *)
(* Rolling and unrolling *)

Section Rolling.
Context {Eff : Set}.
Context (Σ : eff_sig Eff).

Lemma rel_expr_cl_fix_roll RelR RelV (e₁ e₂ : expr0 Eff) n :
  n ⊨ rel_expr_cl RelR RelV e₁ e₂ →
  n ⊨ rel_expr_cl_fix RelR RelV e₁ e₂.
Proof.
intro He; apply (I_fix_roll rel_e2_sig); exact He.
Qed.

Lemma rel_expr_cl_fix_unroll RelR RelV (e₁ e₂ : expr0 Eff) n :
  n ⊨ rel_expr_cl_fix RelR RelV e₁ e₂ →
  n ⊨ rel_expr_cl RelR RelV e₁ e₂.
Proof.
intro He; apply (I_fix_unroll rel_e2_sig); exact He.
Qed.

Lemma rel0_v_fix_roll (τ : typ0 Eff) v₁ v₂ n :
  n ⊨ 〚Σ ⊢ τ〛 (λ x, match x : Empty_set with end) v₁ v₂ →
  n ⊨ rel0_v_fix Σ τ v₁ v₂.
Proof.
intro Hv; apply (I_fix_roll rel_tp0_v2_sig (Frel0_v Σ)).
exact Hv.
Qed.

Lemma rel0_v_fix_unroll (τ : typ0 Eff) v₁ v₂ n :
  n ⊨ rel0_v_fix Σ τ v₁ v₂ →
  n ⊨ 〚Σ ⊢ τ〛 (λ x, match x : Empty_set with end) v₁ v₂.
Proof.
intro Hv; apply (I_fix_unroll rel_tp0_v2_sig (Frel0_v Σ)) in Hv.
exact Hv.
Qed.

End Rolling.